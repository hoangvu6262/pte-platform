import { SummarizeWritten, WriteEssay } from "../page/practice/Writing/";
import {
  ReadAloud,
  RepeatSentence,
  DescribeImage,
  ReTellLecture,
  AnswerShortQuestion,
} from "../page/practice/Speaking";

import {
  HighlightCorrectSummary,
  SummarizeSpokenText,
  WriteFromDictation,
  MultipleChoiceListening,
  SingleChoiceListening,
  SelectMissingWord,
  HighlightIncorrect,
  FillBlanks,
} from "../page/practice/Listening";

import {
  MultipleChoice as ReadingMultiChoice,
  SingleChoice as ReadingSingleChoice,
  ReadingAndWriting,
  ReOrderParagraphs,
  ReadingFillInTheBlank,
} from "../page/practice/Reading";

import { SignIn, SignUp, ForgotPassword, ResetPassword } from "../page/account";

import { ChangePassword, Profile } from "../page/user";

const speakingRouter = [
  {
    id: "RA",
    path: "speaking/ra/:id/:zorder",
    Component: ReadAloud,
  },
  {
    id: "RS",
    path: "speaking/rs/:id/:zorder",
    Component: RepeatSentence,
  },
  {
    id: "DI",
    path: "speaking/di/:id/:zorder",
    Component: DescribeImage,
  },
  {
    id: "RL",
    path: "speaking/rl/:id/:zorder",
    Component: ReTellLecture,
  },
  {
    id: "AS",
    path: "speaking/asq/:id/:zorder",
    Component: AnswerShortQuestion,
  },
];

const writingRouter = [
  {
    id: "SWT",
    path: "writing/SWT/:id/:zorder",
    Component: SummarizeWritten,
  },
  {
    id: "ESSAY",
    path: "writing/ESSAY/:id/:zorder",
    Component: WriteEssay,
  },
];

const readingRouter = [
  {
    id: "FIB",
    path: "reading/FIB-R/:id/:zorder",
    Component: ReadingAndWriting,
  },
  {
    id: "MCQ-MA",
    path: "reading/MCQ-MA-R/:id/:zorder",
    Component: ReadingMultiChoice,
  },
  {
    id: "ROD",
    path: "reading/ROP/:id/:zorder",
    Component: ReOrderParagraphs,
  },
  {
    id: "DD",
    path: "reading/DD/:id/:zorder",
    Component: ReadingFillInTheBlank,
  },
  {
    id: "MCQ-SA",
    path: "reading/MCQ-SA-R/:id/:zorder",
    Component: ReadingSingleChoice,
  },
];

const listeningRouter = [
  {
    id: "SST",
    path: "listening/SST/:id/:zorder",
    Component: SummarizeSpokenText,
  },
  {
    id: "MCQ-MA",
    path: "listening/MCQ-MA-L/:id/:zorder",
    Component: MultipleChoiceListening,
  },
  {
    id: "FIB",
    path: "listening/FIB-L/:id/:zorder",
    Component: FillBlanks,
  },
  {
    id: "HCS",
    path: "listening/HCS/:id/:zorder",
    Component: HighlightCorrectSummary,
  },
  {
    id: "MCQ-SA",
    path: "listening/MCQ-SA-L/:id/:zorder",
    Component: SingleChoiceListening,
  },
  {
    id: "SMW",
    path: "listening/SMW/:id/:zorder",
    Component: SelectMissingWord,
  },
  {
    id: "HIW",
    path: "listening/HIW/:id/:zorder",
    Component: HighlightIncorrect,
  },
  {
    id: "WFD",
    path: "listening/WFD/:id/:zorder",
    Component: WriteFromDictation,
  },
];

export const practiceRouter = [
  ...speakingRouter,
  ...writingRouter,
  ...readingRouter,
  ...listeningRouter,
];

export const accountRouter = [
  {
    id: "sign-up",
    path: "sign-up",
    Component: SignUp,
  },
  {
    id: "sign-in",
    path: "sign-in",
    Component: SignIn,
  },
  {
    id: "forgot-password",
    path: "forgot-password",
    Component: ForgotPassword,
  },
  {
    id: "reset-password",
    path: "reset-password",
    Component: ResetPassword,
  },
];

export const userRouter = [
  {
    id: "profile",
    path: "profile",
    Component: Profile,
  },
  {
    id: "change-password",
    path: "change-password",
    Component: ChangePassword,
  },
];
