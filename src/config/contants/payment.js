export const CODE_LOCALE_PLAN = {
  VIET_NAM: "V",
  USA: "U",
};

export const STANDARD_ID = -1;
export const STANDARD_AMOUNT = 0;

export const LIST_BENEFITS_STANDARD = [
  {
    id: 1,
    name: "Unlimited lesson",
  },
  {
    id: 2,
    name: "Unlimited scoring with AI & suggestion",
  },
  {
    id: 3,
    name: "Do 10 mock tests",
  },
  {
    id: 4,
    name: "Scoring mock tests with AI",
  },
  {
    id: 5,
    name: "Contribute content",
  },
  {
    id: 6,
    name: "Discusstion and community",
  },
  {
    id: 7,
    name: "Devices using in 3 months",
  },
];
