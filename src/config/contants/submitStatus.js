export const submitStatus = {
  NOT_SUBMITTED: 1,
  ANSWERING: 2,
  SUBMITTED: 3,
};
