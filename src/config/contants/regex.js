export const regex = {
  SPECIAL_CHARACTER: /[^a-zA-Z ]/g,
  PTE_CODE: /PTE-[A-Z]/g,
};
