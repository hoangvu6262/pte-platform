const environmentConstants = {
  PTE_ENDPOINT: process.env.REACT_APP_PRACTICE_API,
  USER_ENDPOINT: process.env.REACT_APP_ACCOUNT_API,
  AI_ENDPOIN: process.env.REACT_APP_AI_API,
};

export default environmentConstants;
