import environmentConstants from "./environmentConstant";

export const API_DOMAIN = {
  PTE: environmentConstants.PTE_ENDPOINT,
  USER: environmentConstants.USER_ENDPOINT,
  AI: environmentConstants.AI_ENDPOINT,
};
