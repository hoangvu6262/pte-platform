import httpHandler from "./axios";
import { API_DOMAIN } from "../contants/http";

function get(domain, url, config = {}) {
  return httpHandler(domain).get(`${url}`, config);
}

function post(domain, url, data, config = {}) {
  return httpHandler(domain).post(`${url}`, data, config);
}

function put(domain, url, data, config = {}) {
  return httpHandler(domain).put(`${url}`, data, config);
}

function del(domain, url, config = {}) {
  return httpHandler(domain).delete(`${url}`, config);
}

export const userTransport = {
  get: (url, config = {}) => {
    return get(API_DOMAIN.USER, url, config);
  },
  post: (url, data, config = {}) => {
    return post(API_DOMAIN.USER, url, data, config);
  },
  put: (url, data, config = {}) => {
    return put(API_DOMAIN.USER, url, data, config);
  },
  del: (url, config = {}) => {
    return del(API_DOMAIN.USER, url, config);
  },
};
