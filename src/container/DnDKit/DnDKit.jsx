import DroppableContainer from "../../components/dnd/DroppableContainer";
import React, { useEffect, useMemo, useState } from "react";
import SortableItem from "../../components/dnd/SortableItem";
import WordBank from "../../components/dnd/WordBank";
import {
  DndContext,
  DragOverlay,
  KeyboardSensor,
  PointerSensor,
  closestCorners,
  useSensor,
  useSensors,
} from "@dnd-kit/core";
import { Item } from "../../components/dnd/Item";
import { arrayMove, sortableKeyboardCoordinates } from "@dnd-kit/sortable";

import shuffle from "lodash/shuffle";
import uniq from "lodash/uniq";
import ParagraphBlank from "../../components/practice/ParagraphBlank/ParagraphBlank";
import { useSelector, useDispatch } from "react-redux";
import answerSlices, {
  changeStatusThunk,
} from "../../redux/slices/answer/answerSlice";

import { SolutionGetter, WORD_BANK } from "../../utils";
import { sortQuestions } from "../../utils/practice";

// need a JSX component that can accept props
export function Blank({ solution }) {
  return <></>;
}

const parseItemsFromChildren = (children, wrongAnswers, isTaskComplete) => {
  const solutionGetter = new SolutionGetter();
  let indexAnswer = 0;
  const childrenWithBlanks = React.Children.toArray(children).map(
    (child, index) => {
      if (child.props?.solution) {
        const { solution } = child.props;
        const solutions = Array.isArray(solution) ? solution : [solution];
        indexAnswer++;
        return {
          id: `#${indexAnswer}`,
          solutions,
          isCorrect: isTaskComplete || null,
          items: isTaskComplete ? solutionGetter.get(solutions) : [],
        };
      }
      return child;
    }
  );

  const solutions = [];
  const blanks = childrenWithBlanks.reduce((acc, currChild) => {
    if (currChild.solutions) {
      solutions.push(...currChild.solutions);
      return {
        ...acc,
        [currChild.id]: currChild,
      };
    }

    return acc;
  }, {});

  blanks[WORD_BANK] = {
    items: isTaskComplete
      ? wrongAnswers
      : shuffle(uniq(solutions).concat(wrongAnswers)),
  };

  return [blanks, childrenWithBlanks];
};

export default function Dnd({
  taskId,
  children,
  isAnswerVisible,
  wrongAnswers = [],
  setIsAnswerVisible,
  questions,
}) {
  const { status } = useSelector((state) => state.answer);

  const dispatch = useDispatch();

  const isTaskComplete = false;
  const [isCorrect, setIsCorrect] = useState(
    isTaskComplete && isTaskComplete.length ? true : false
  );
  const [activeId, setActiveId] = useState(null);
  const [hasSubmitted, setHasSubmitted] = useState(
    isTaskComplete && isTaskComplete.length ? true : false
  );

  const [initialItems] = useMemo(() => {
    return parseItemsFromChildren(children, wrongAnswers);
  }, [children, wrongAnswers]);

  const [defaultItems, childrenWithBlanks] = useMemo(() => {
    return parseItemsFromChildren(children, wrongAnswers, isTaskComplete);
  }, [children, isTaskComplete, wrongAnswers]);

  // keys in `items` are the ids of the blanks/droppableContainers
  const [items, setItems] = useState(defaultItems);

  useEffect(() => {
    setItems(defaultItems);
  }, [defaultItems]);

  const allBlanksEmpty = useMemo(
    () =>
      !Object.entries(items).some(
        ([key, value]) => key !== WORD_BANK && value.items.length
      ),
    [items]
  );

  const sensors = useSensors(
    useSensor(PointerSensor),
    useSensor(KeyboardSensor, {
      coordinateGetter: sortableKeyboardCoordinates,
    })
  );

  // find the blank/droppableContainer that an item is in
  const findContainer = (id) => {
    if (id in items) {
      return id;
    }

    return Object.keys(items).find((key) => items[key].items.includes(id));
  };

  const onDragStart = ({ active }) => {
    setActiveId(active.id);
  };

  const onDragEnd = ({ active, over }) => {
    const activeContainer = findContainer(active.id);

    if (!activeContainer) {
      setActiveId(null);
      return;
    }

    const overId = over?.id;
    const overContainer = findContainer(overId);

    if (activeContainer && overContainer) {
      const activeIndex = items[activeContainer].items.indexOf(active.id);
      const overIndex = items[overContainer].items.indexOf(overId);

      // if it's different than overContainer, swap the items
      if (activeContainer !== overContainer) {
        setItems((prevItems) => {
          let activeItems = [...prevItems[activeContainer].items];
          let overItems = [...prevItems[overContainer].items];

          // activeContainer gets what was in overContainer and vice versa
          // first check if overContainer is word bank or a blank
          // if it's a blank (NOT the word bank), swap contents with activeContainer
          // if it IS word bank, just move activeContainer contents to word bank
          if (overContainer === WORD_BANK) {
            activeItems = [];
            overItems.push(active.id);
          } else {
            activeItems.splice(activeIndex, 1);

            // if there's already something in the blank, push its contents to activeItems
            if (overItems.length) {
              activeItems.push(...overItems);
            }
            overItems = [active.id];
          }

          const updatedItems = {
            ...prevItems,
            [activeContainer]: {
              ...prevItems[activeContainer],
              isCorrect: null,
              items: activeItems,
            },
            [overContainer]: {
              ...prevItems[overContainer],
              isCorrect: null,
              items: overItems,
            },
          };

          // reset isCorrect values if all of the blanks (minus word bank) are empty
          if (allBlanksEmpty) {
            Object.values(updatedItems).forEach((blank) => {
              blank.isCorrect = null;
            });
          }

          return updatedItems;
        });
      } else if (activeIndex !== overIndex) {
        setItems((prevItems) => ({
          ...prevItems,
          [overContainer]: {
            ...prevItems[overContainer],
            isCorrect: null,
            items: arrayMove(
              items[overContainer].items,
              activeIndex,
              overIndex
            ),
          },
        }));
      }
    }

    setActiveId(null);
  };

  const onDragCancel = () => {
    setActiveId(null);
  };

  const showWordBank = !hasSubmitted || !isCorrect;

  const checkAnswers = () => {
    let isCorrect = true;
    const checkedBlanks = Object.entries(items).reduce((acc, [key, value]) => {
      if (key !== WORD_BANK) {
        const isBlankCorrect = value.items.some((item) =>
          value.solutions.includes(item)
        );

        acc[key] = {
          ...value,
          isCorrect: isBlankCorrect,
        };

        // if at least one blank is incorrect, the whole activity is incorrect
        // need to update FillInTheBlanksInner `isCorrect` state
        if (!isBlankCorrect) {
          isCorrect = isBlankCorrect;
        }
      } else {
        acc[key] = { ...value, isCorrect: null };
      }

      return acc;
    }, {});

    setIsCorrect(isCorrect);
    setItems(checkedBlanks);
    setHasSubmitted(true);
  };

  const [listAnswers, setListAnswers] = useState([]);

  useEffect(() => {
    if (hasSubmitted) {
      dispatch(changeStatusThunk(3));
    }
  }, [dispatch, hasSubmitted]);

  useEffect(() => {
    let list = [];

    for (const [key, value] of Object.entries(items)) {
      if (key !== WORD_BANK) {
        if (value.items.length > 0) {
          list.push(value.id[1] + "/" + value.items[0]);
        }
      }
    }

    setListAnswers(list);
  }, [items]);

  useEffect(() => {
    if (status === 3) {
      setIsAnswerVisible(true);
      checkAnswers();
    } else if (status === 1) {
      setIsAnswerVisible(false);
      setItems(initialItems);
      setIsCorrect(false);
      setListAnswers([]);
      setHasSubmitted(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [status]);

  useEffect(() => {
    if (listAnswers !== [] && status === 2) {
      const sortedQuestions = sortQuestions(questions);
      const mappedQuestions = sortedQuestions.map((ques) => {
        return {
          question: {
            id: ques.id,
          },
          questionResponseUsers: [
            {
              valueText: "",
            },
          ],
        };
      });

      listAnswers.forEach((ans) => {
        const splitted = ans.split("/");
        mappedQuestions[splitted[0] - 1].questionResponseUsers[0].valueText =
          splitted[1];
      });

      dispatch(answerSlices.actions.updateAnswering(mappedQuestions));
    }
  }, [dispatch, listAnswers, questions, status]);

  return (
    <div>
      <DndContext
        sensors={sensors}
        collisionDetection={closestCorners}
        onDragStart={onDragStart}
        onDragEnd={onDragEnd}
        onDragCancel={onDragCancel}
      >
        <div style={{ width: "100%" }}>
          <div>
            {childrenWithBlanks.map((child, index) => {
              const { solutions, id } = child;
              // need a blank for children that have a 'solution
              if (solutions) {
                const { items: blankItems, isCorrect: isBlankCorrect } = items[
                  id
                ] || { items: [""], isCorrect: null };
                return (
                  <span key={index}>
                    <DroppableContainer
                      key={id}
                      id={id}
                      items={blankItems}
                      isCorrect={isBlankCorrect}
                      allBlanksEmpty={allBlanksEmpty}
                      isAnswerVisible={isAnswerVisible}
                      style={{
                        height: "40px",
                      }}
                    >
                      {blankItems.map((value) => {
                        return (
                          <SortableItem
                            key={`sortable-item--${value}`}
                            id={value}
                            taskId={taskId}
                            isCorrect={isBlankCorrect}
                          />
                        );
                      })}
                    </DroppableContainer>{" "}
                    {isAnswerVisible && (
                      <span style={{ color: "#60d3c6", fontSize: 17 }}>
                        {"(Answer: "}
                        <ParagraphBlank content={solutions[0]} key={index} />
                        {")"}
                      </span>
                    )}
                  </span>
                );
              }
              return <ParagraphBlank content={child} key={index} />;
            })}
          </div>
          {showWordBank && (
            <WordBank
              taskId={taskId}
              items={items}
              hasSubmitted={hasSubmitted}
            />
          )}
        </div>
        <DragOverlay>
          {activeId && (
            <>
              <Item value={activeId} dragOverlay />
            </>
          )}
        </DragOverlay>
      </DndContext>
    </div>
  );
}
