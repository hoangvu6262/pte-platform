import React, { useState, useEffect } from "react";
import { Switch } from "antd";

import { useLocation, useParams, useNavigate } from "react-router-dom";

import answerSlices, {
  submitAnswerThunk,
} from "../../redux/slices/answer/answerSlice";
import {
  getNextPageLessonThunk,
  getPrevPageLessonThunk,
} from "../../redux/slices/lesson/lessonSlice";

import { useDispatch, useSelector } from "react-redux";

import "./styles.scss";
import Button from "../../components/shared/Button/Button";
import ShadowingModal from "../../components/modal/ShadowingModal/ShadowingModal";

import { checkSubmitDisable } from "../../utils/practice";

const FunctionBar = ({ toggleAnswer, isAnswerVisible, disabled, code }) => {
  const { status } = useSelector((state) => state.answer);
  const contentAns = useSelector((state) => state.answer.content);
  const { id } = useParams();
  const [isDisabled, setIsDisabled] = useState(true);
  const [isShadowingVisible, setIsShadowingVisible] = useState(false);

  const { pathname } = useLocation();
  const navigate = useNavigate();

  const { listLesson, pagination } = useSelector(
    (state) => state.lesson.listLesson
  );

  const { content } = useSelector((state) => state.lesson.lessonDetail);

  // Find Current Index
  const findCurIndex = () => {
    const curIndex = listLesson.findIndex((lesson) => lesson.id === content.id);

    return curIndex;
  };

  const dispatch = useDispatch();
  const handleClick = () => {
    dispatch(
      submitAnswerThunk({ id: id - 0, code, category: pathname.split("/")[3] })
    );
  };

  const navigateLink = (path, id) => {
    const splittedPath = path.split("/");
    splittedPath[splittedPath.length - 1] = id;

    return splittedPath.join("/");
  };

  useEffect(() => {
    if (status === 1) {
      setIsDisabled(true);
    }
    if (status === 2 && checkSubmitDisable(contentAns)) {
      setIsDisabled(true);
    } else if (status === 2) {
      setIsDisabled(false);
    } else if (status !== 2) {
      setIsDisabled(true);
    }
  }, [status, contentAns]);

  const disabledNext = () => {
    let disable = false;

    if (
      findCurIndex() === listLesson.length - 1 &&
      pagination.numberOfCurrentPage === pagination.totalPages - 1
    ) {
      disable = true;
    }

    return disable;
  };

  const disabledPrev = () => {
    let disable = false;

    if (findCurIndex() === 0 && pagination.numberOfCurrentPage === 0) {
      disable = true;
    }

    return disable;
  };

  const handleNext = () => {
    if (
      findCurIndex() === listLesson.length - 1 &&
      pagination.numberOfCurrentPage < pagination.totalPages - 1
    ) {
      dispatch(
        getNextPageLessonThunk({
          pageNumber: pagination.numberOfCurrentPage + 1,
        })
      );
    } else {
      const nextLesson = listLesson[findCurIndex() + 1];
      navigate(navigateLink(pathname, nextLesson.id));
    }
  };

  const handlePrev = () => {
    if (findCurIndex() === 0 && pagination.numberOfCurrentPage > 0) {
      dispatch(
        getPrevPageLessonThunk({
          pageNumber: pagination.numberOfCurrentPage - 1,
        })
      );
    } else {
      const prevLesson = listLesson[findCurIndex() - 1];
      navigate(navigateLink(pathname, prevLesson.id));
    }
  };

  const handleShadowing = () => {
    setIsShadowingVisible(true);
  };

  return (
    <div className="function-bar">
      <div className="function-bar__left">
        <Button
          size="small"
          disabled={isDisabled || disabled}
          onClick={handleClick}
        >
          {status === 3 ? "Submitted" : "Submit"}
        </Button>
        <Button
          size="small"
          onClick={() => {
            dispatch(answerSlices.actions.redoAnswering());
          }}
        >
          Re-do
        </Button>
        {id === "3" && (
          <Button size="small" onClick={handleShadowing}>
            Shadowing
          </Button>
        )}

        <Switch
          className="function-bar__left-switch"
          checkedChildren="Answer"
          unCheckedChildren="Answer"
          onChange={() => {
            toggleAnswer();
          }}
          checked={isAnswerVisible}
        />
      </div>
      <div className="function-bar__right">
        <div
          className={`function-bar__btn ${
            disabledPrev() && "function-bar__btn--disabled"
          }`}
          onClick={handlePrev}
        >
          <i className="fa-solid fa-arrow-left-long"></i>
        </div>
        <div
          className={`function-bar__btn ${
            disabledNext() && "function-bar__btn--disabled"
          }`}
          onClick={handleNext}
        >
          <i className="fa-solid fa-arrow-right-long"></i>
        </div>
      </div>

      <ShadowingModal
        isModalVisible={isShadowingVisible}
        setIsModalVisible={setIsShadowingVisible}
      ></ShadowingModal>
    </div>
  );
};

export default FunctionBar;
