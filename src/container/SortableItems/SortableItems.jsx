import React from "react";
import { useSortable } from "@dnd-kit/sortable";
import { CSS } from "@dnd-kit/utilities";

import "./styles.scss";
import Paragraph from "../../components/practice/Paragraph/Paragraph";

const SortableItem = ({ item }) => {
  const {
    attributes,
    listeners,
    transform,
    transition,
    setDroppableNodeRef,
    setDraggableNodeRef,
  } = useSortable({ id: item.id });

  const style = {
    transform: CSS.Transform.toString(transform),
    transition,
  };

  return (
    <div
      ref={setDroppableNodeRef}
      style={style}
      {...attributes}
      {...listeners}
      distance={1}
      className="sort-items"
    >
      <div
        ref={setDraggableNodeRef}
        {...listeners}
        className="sort-items__paragraph"
      >
        <Paragraph content={item.id + ") " + item.content} />
      </div>
    </div>
  );
};

export default SortableItem;
