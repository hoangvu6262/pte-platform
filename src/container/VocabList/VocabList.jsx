import React, { useState, useContext, useRef } from "react";
import { myVocabContext } from "../../page/main/MyVocab/MyVocab";

import { toast } from "react-toastify";

import "./styles.scss";
import {
  Empty,
  Input,
  Divider,
  Tooltip,
  Button,
  Row,
  Col,
} from "antd";
import { SearchOutlined } from "@ant-design/icons";

import Loading from "../../components/shared/Loading/Loading";
import DictionaryModal from "../../components/modal/DictionaryModal/DictionaryModal";
import EditVocabModal from "../../components/modal/EditVocabModal/EditVocabModal";
import myVocabAPI from "../../redux/api/practice/myVocabAPI";

const { deleteVocab } = myVocabAPI;

const { Search } = Input;

const VocabItem = ({ vocab, handleOpen, handleEdit, id, definition }) => {
  const { handleGetVocabList, page } = useContext(myVocabContext);

  const handleEditClick = (e) => {
    e.stopPropagation();
    handleEdit(vocab.vocab);
  };

  const handleClick = () => {
    handleOpen(vocab.vocab);
  };

  const handleDelete = (e) => {
    e.stopPropagation();
    deleteVocab(id).then((res) => {
      handleGetVocabList(page - 0 + 1);
      toast.success("Deleted successfully");
    });
  };

  return (
    <div className="vocab-list__item" onClick={handleClick}>
      <Row style={{ width: "100%" }} gutter={[12, 12]}>
        <Col sm={4} xs={6}>
          <p className="vocab-list__item--name">{vocab.vocab}</p>
        </Col>
        <Col sm={18} xs={14}>
          <p className="vocab-list__item--definition">{definition}</p>
        </Col>
        <Col sm={2} xs={4}>
          <div className="vocab-list__item--tools">
            <Tooltip title="Edit">
              <Button
                shape="circle"
                icon={<i className="fa-regular fa-pen-to-square"></i>}
                style={{
                  background: "transparent",
                  borderColor: "transparent",
                  color: "#60d3c6",
                }}
                onClick={handleEditClick}
              ></Button>
            </Tooltip>
            <Tooltip title="Delete">
              <Button
                shape="circle"
                icon={<i className="fa-regular fa-trash-can"></i>}
                style={{
                  background: "transparent",
                  borderColor: "transparent",
                  color: "#ff5c58",
                }}
                onClick={handleDelete}
              ></Button>
            </Tooltip>
          </div>
        </Col>
      </Row>
    </div>
  );
};

const VocabList = () => {
  const { data, status, handleGetVocabList, vocabListStatus } =
    useContext(myVocabContext);

  const editModalRef = useRef()

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);
  const [openText, setOpenText] = useState();
  const [editText, setEditText] = useState();

  const onSearch = (value) => {
    handleGetVocabList(0, value);
  };

  const handleOpen = (text) => {
    setOpenText(text);
    setIsModalVisible(true);
  };

  const handleEdit = (item) => {
    editModalRef.current.open(item)
  };

  //render view
  if (!data || status === "error") {
    return <Empty />;
  }

  if (status === "loading") return <Loading />;

  return (
    <div className="vocab-list">
      <div className="vocab-list__header">
        <span className="vocab-list__header--total">
          Total: {data.totalItems} {data.totalItems > 1 ? "words" : "word"}
        </span>
        <Search
          placeholder="Search vocab..."
          allowClear
          enterButton={<SearchOutlined />}
          onSearch={onSearch}
          style={{ width: "200px" }}
        />
      </div>
      <Divider />
      <div className="vocab-list__main">
        {vocabListStatus ? (
          <Loading />
        ) : data.contents?.length > 0 ? (
          data.contents.map((item) => {
            return (
              <VocabItem
                id={item.id}
                key={item.id + item.vocab.vocab}
                vocab={item.vocab}
                definition={item.definition}
                handleOpen={handleOpen}
                handleEdit={() => handleEdit(item)}
              />
            );
          })
        ) : (
          <Empty />
        )}
      </div>
      <DictionaryModal
        isModalVisible={isModalVisible}
        setIsModalVisible={setIsModalVisible}
        text={openText}
      />
      <EditVocabModal
        ref={editModalRef}
        isModalVisible={isEditModalVisible}
        setIsModalVisible={setIsEditModalVisible}
        text={editText}
      />
    </div>
  );
};

export default VocabList;
