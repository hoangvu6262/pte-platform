import React, { useState, useRef } from "react";

import { useSelector } from "react-redux";

import "./styles.scss";

import { DatePicker, Input } from "antd";
import moment from "moment";

import testedAPI from "../../redux/api/practice/testedAPI";
import Button from "../../components/shared/Button/Button";

const { TextArea } = Input;

const TestedForm = ({ lessonId, setIsModalVisible }) => {
  const [date, setDate] = useState(moment());
  const [moreIn4, setMoreIn4] = useState();
  const dateRef = useRef();

  const { saveTested } = testedAPI;
  const handleClose = () => {
    setIsModalVisible(false);
  };

  const userAuthId = useSelector((state) => state.user.detail.id);

  const handleSubmit = () => {
    const data = {
      examDate: moment(date).format("YYYY-MM-DD 00:00:00"),
      description: moreIn4,
      lessonId: lessonId - 0,
      userId: userAuthId,
    };

    saveTested(data).then((res) => {
      handleClose();
    });
  };

  const disabledDate = (current) => {
    return current && current > moment().endOf("day");
  };

  const handleDateChange = (value) => {
    if (value) {
      setDate(value._d);
    } else {
      setDate();
    }
  };

  const handleIn4Change = (e) => {
    setMoreIn4(e.target.value);
  };

  const dateFormat = "YYYY-MM-DD";

  return (
    <div className="tested-form">
      <div className="tested-form__header">
        <h1>Confirm exam information</h1>
      </div>
      <div className="tested-form__main">
        <div className="tested-form__input">
          <h3>
            Exam Date <span>(required)</span>
          </h3>

          <DatePicker
            defaultValue={moment()}
            disabledDate={disabledDate}
            ref={dateRef}
            onChange={handleDateChange}
            format={dateFormat}
          />
        </div>
        <div className="tested-form__input">
          <h3>More information</h3>

          <TextArea
            onChange={handleIn4Change}
            placeholder="Please share more information with us"
            autoSize={{ minRows: 4 }}
          />
        </div>
      </div>
      <div className="tested-form__footer">
        <Button
          size={"small"}
          // disabled={date ? false : true}
          onClick={handleSubmit}
        >
          Confirm
        </Button>
        <Button outline={true} size="small" onClick={handleClose}>
          Cancel
        </Button>
      </div>
    </div>
  );
};

export default TestedForm;
