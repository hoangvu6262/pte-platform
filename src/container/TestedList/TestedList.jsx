import React, { useState, useContext } from "react";

import Button from "../../components/shared/Button/Button";

import moment from "moment";
import testedAPI from "../../redux/api/practice/testedAPI";

import { Popover } from "antd";
import "./styles.scss";

import { TestedModalContext } from "../../components/modal/TestedModal/TestedModal";

const { deleteTested } = testedAPI;

const TestedItem = ({ examDate, description, recordTime, id }) => {
  const [popVisible, setPopVisible] = useState(false);

  const { deleteFromList } = useContext(TestedModalContext);

  const hidePop = () => {
    setPopVisible(false);
  };

  const handlePopVisible = (visible) => {
    setPopVisible(visible);
  };

  const handleDelete = (id) => {
    hidePop();
    deleteFromList(id);
    deleteTested(id);
  };

  return (
    <div className="tested-list-item">
      <div className="tested-list-item__date">
        {moment(examDate).format("DD - MM - YYYY")}
      </div>
      <div className="tested-list-item__des">
        <p>{description}</p>
      </div>
      <div className="tested-list-item__record">
        Record Time: {moment(recordTime).format("DD - MM - YYYY")}
      </div>

      <Popover
        content={
          <div className="tested-popover__btns">
            <button
              className="tested-popover__btns--yes"
              onClick={() => {
                handleDelete(id);
              }}
            >
              Yes
            </button>
            <button
              className="tested-popover__btns--no"
              onClick={() => {
                hidePop();
              }}
            >
              No
            </button>
          </div>
        }
        title="Sure to delete this?"
        trigger="click"
        visible={popVisible}
        onVisibleChange={handlePopVisible}
      >
        <div className="tested-list-item__delete">
          <i className="fa-regular fa-trash-can"></i>
        </div>
      </Popover>
    </div>
  );
};

const TestedList = ({ testedList, count, openForm }) => {
  return (
    <div className="tested-list">
      <div className="tested-list__header">
        <h1>
          Been tested on this{" "}
          <span>{testedList.length > 0 ? testedList.length : count}</span> times
        </h1>
      </div>
      <div className="tested-list__main">
        {testedList.length > 0 &&
          testedList.map((tested) => {
            return (
              <TestedItem
                examDate={tested.examDate}
                description={tested.description}
                recordTime={tested.recordTime}
                id={tested.id}
              />
            );
          })}
      </div>
      <div className="tested-list__footer">
        <Button
          size="small"
          onClick={() => {
            openForm(true);
          }}
        >
          Tested Again
        </Button>
      </div>
    </div>
  );
};

export default TestedList;
