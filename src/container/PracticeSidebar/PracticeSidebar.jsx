import React from "react";
import { Tabs, Input, Tooltip } from "antd";
import { useDispatch } from "react-redux";
import { useLocation, useParams, useNavigate } from "react-router-dom";

import listeningIcon from "../../asset/img/listening-icon.png";
import readingIcon from "../../asset/img/reading-icon.png";
import writingIcon from "../../asset/img/writing-icon.png";
import speakingIcon from "../../asset/img/speaking-icon.png";

import "./styles.scss";
import AllTab from "./AllTab";
import lessonSlices, {
  getListLessonThunk,
} from "../../redux/slices/lesson/lessonSlice";
import moment from "moment";
import queryString from "query-string";

const { TabPane } = Tabs;
const { Search } = Input;

const PracticeSidebar = ({ setVisible }) => {
  const location = useLocation();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { id } = useParams();

  const checkType = () => {
    switch (location.pathname.split("/")[2]) {
      case "reading":
        return readingIcon;
      case "listening":
        return listeningIcon;
      case "writing":
        return writingIcon;
      case "speaking":
        return speakingIcon;
      default:
        return "";
    }
  };

  const onSearch = (value) => {
    const searchQuery = queryString.stringify({ category: id, keyword: value });
    navigate("/search?" + searchQuery);
  };

  const handleTabChange = (valueTab) => {
    const date = new Date();
    switch (valueTab) {
      case "all":
        dispatch(
          lessonSlices.actions.updateFilter({ week: null, month: null })
        );
        dispatch(getListLessonThunk({ week: null, month: null }));
        break;
      case "week":
        dispatch(
          lessonSlices.actions.updateFilter({
            week: true,
            month: null,
          })
        );
        dispatch(getListLessonThunk({ week: true, month: null }));
        break;
      case "month":
        dispatch(
          lessonSlices.actions.updateFilter({
            week: null,
            month: date.getMonth() + 1,
          })
        );
        dispatch(
          getListLessonThunk({ week: null, month: date.getMonth() + 1 })
        );
        break;
      default:
        break;
    }
  };

  return (
    <div className="practice-sidebar">
      <div className="practice-sidebar__container">
        <div className="practice-sidebar__header">
          <img src={checkType()} alt="" />
          <div className="practice-sidebar__search">
            <Search
              onSearch={onSearch}
              placeholder="Search..."
              style={{
                width: 240,
              }}
            />
          </div>
        </div>

        <div>
          <Tabs defaultActiveKey="all" onChange={handleTabChange}>
            <TabPane tab={<Tooltip>All</Tooltip>} key="all" size="large">
              <AllTab setVisible={setVisible} />
            </TabPane>
            <TabPane
              tab={
                <Tooltip
                  title={
                    moment().day(1).format("DD/MM/YYYY") +
                    " - " +
                    moment().day(7).format("DD/MM/YYYY")
                  }
                >
                  This week
                </Tooltip>
              }
              key="week"
            >
              <AllTab filters="week" />
            </TabPane>
            <TabPane
              tab={
                <Tooltip
                  title={
                    moment().format("MMMM") + " " + moment().format("YYYY")
                  }
                >
                  This Month
                </Tooltip>
              }
              key="month"
            >
              <AllTab filters="month" />
            </TabPane>
          </Tabs>
        </div>
      </div>
    </div>
  );
};

export default PracticeSidebar;
