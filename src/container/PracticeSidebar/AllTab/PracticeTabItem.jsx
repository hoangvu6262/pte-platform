import React, { useEffect, useState } from "react";
import { Tag } from "antd";
import { Link, useParams, useLocation } from "react-router-dom";

import { mapPriority } from "../../../utils";
import { useSelector } from "react-redux";

const modifiedPath = (path, id) => {
  const arr = path.split("/");
  arr[arr.length - 1] = id;
  return arr.join("/");
};

const PracticeTabItem = ({ item, setVisible }) => {
  const { code } = useParams();
  const location = useLocation();
  const [mark, setMark] = useState("");
  const marks = useSelector((state) => state.shared.marks);

  const modifiedTitle = (title) => {
    const splitted = title.split(" ");
    if (splitted.length > 12) {
      return splitted.slice(0, 12).join(" ") + "...";
    } else {
      return title;
    }
  };

  useEffect(() => {
    if (marks && item.priorities) {
      setMark(mapPriority(item.priorities[0]?.priority, marks));
    }
  }, [item, marks]);

  return (
    <div className="practice-tab-item">
      <div className="practice-tab-item__left">
        <Link
          onClick={() => setVisible(false)}
          to={modifiedPath(location.pathname, item.zorder)}
          disabled={item.id === code - 0}
          title={modifiedTitle(item.content)}
        >
          #{item.zorder} {item.title}
        </Link>
        <Tag className="practice-tab-item__tag">#{item.zorder}</Tag>
      </div>
      <div className="practice-tab-item__right">
        <i
          className="fa-solid fa-bookmark"
          style={{ color: mark || "rgb(204, 204, 204)" }}
        ></i>
      </div>
    </div>
  );
};

export default PracticeTabItem;
