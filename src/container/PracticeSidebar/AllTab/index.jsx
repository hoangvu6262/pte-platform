import React from "react";
import { useSelector, useDispatch } from "react-redux";

import { Empty } from "antd";
import "./styles.scss";
import DropdownAction from "../../../components/practice/SidebarDropdown/SidebarDropdown";
import CustomPagination from "../../../components/shared/Pagination/CustomPagination";
import Loading from "../../../components/shared/Loading/Loading";
import PracticeTabItem from "./PracticeTabItem";

import { getListLessonThunk } from "../../../redux/slices/lesson/lessonSlice";

export const allTabContext = React.createContext();

const AllTab = ({ setVisible }) => {
  const { listLesson, pagination, status } = useSelector(
    (state) => state.lesson.listLesson
  );

  const dispatch = useDispatch();

  const onPageChange = (page) => {
    dispatch(getListLessonThunk({ pageNumber: page - 1 }));
  };

  return (
    <div className="all-tab">
      <div className="all-tab-dropdown-n-reset">
        <DropdownAction />
      </div>
      <div className="all-tab__title">
        <span className="all-tab__pagination--amount">
          Found {pagination.totalItems} lessons
        </span>
        {/* <ResetModal /> */}
      </div>

      <div className="all-tab__practice-tab">
        {status === "loading" && <Loading />}
        {status === "success" && (
          <>
            {listLesson.map((item) => (
              <PracticeTabItem
                key={item.id}
                item={item}
                setVisible={setVisible}
              />
            ))}
          </>
        )}
        {status === "error" && <Empty />}
      </div>
      <div className="all-tab__pagination">
        <CustomPagination
          totalPages={pagination.totalItems}
          curPage={pagination.numberOfCurrentPage}
          pageSize={pagination.sizeCurrentItems}
          onChange={onPageChange}
        />
      </div>
    </div>
  );
};

export default React.memo(AllTab);
