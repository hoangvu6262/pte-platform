import React from "react";

import "./styles.scss";

import Logo from "../../components/shared/Logo/Logo";

const AccountForm = ({ children }) => {
  return (
    <div className="account-form">
      <Logo />
      {children}
    </div>
  );
};

export default AccountForm;
