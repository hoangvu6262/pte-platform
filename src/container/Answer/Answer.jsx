import React from "react";

import Paragraph from "../../components/practice/Paragraph/Paragraph";

import "./styles.scss";

const Answer = ({ answer = "", transcript, children, explanation }) => {
  return (
    <div className="answer">
      <div className="answer__content">
        {transcript ? (
          <div>
            <h5>Transcript:</h5>
            <Paragraph content={transcript} />
          </div>
        ) : (
          ""
        )}
        {explanation ? (
          <div>
            <h5>Explanation:</h5>
            <div
              dangerouslySetInnerHTML={{
                __html: explanation,
              }}
              className="answer__content--explanation"
            ></div>
          </div>
        ) : (
          ""
        )}
        {answer && (
          <div>
            <h5>Answer:</h5>
            <p>{answer.charAt(0) === "#" ? "" : answer}</p>
          </div>
        )}
        {children}
      </div>
    </div>
  );
};

export default Answer;
