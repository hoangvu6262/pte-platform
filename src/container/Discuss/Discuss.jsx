import React, { useEffect } from "react";
import { Tabs } from "antd";
import { useSelector, useDispatch } from "react-redux";
import { useParams } from "react-router-dom";

import "./styles.scss";
import CustomTab from "../../components/shared/Tab/CustomTab";
import DiscussionTab from "../../components/discussion/DiscussionTab/DiscussionTab";
import BoardTab from "../../components/discussion/BoardTab/BoardTab";
import MeTab from "../../components/discussion/MeTab/MeTab";
import {
  MessageOutlined,
  UnorderedListOutlined,
  FileDoneOutlined,
} from "@ant-design/icons";
import {
  getListDiscussionTabThunk,
  getListBoardTabThunk,
  getListMeTabThunk,
} from "../../redux/slices/discuss/discussSlice";

import { changeTabKeyThunk } from "../../redux/slices/discuss/discussSlice";

const { TabPane } = Tabs;

const Discuss = () => {
  const { listAnswer, listBoard, listDiscussion, tabKey } = useSelector(
    (state) => state.discuss
  );
  const submitStatus = useSelector((state) => state.answer.status);
  const userAuthId = useSelector((state) => state.user.detail.id);
  const lessonDetail = useSelector((state) => state.lesson.lessonDetail.content)

  const lessonId = lessonDetail.id

  const dispatch = useDispatch();


  useEffect(() => {
    if (lessonId) {
      dispatch(changeTabKeyThunk("1"));
      dispatch(getListDiscussionTabThunk({ lessonId, page: 0 }));
    }
  }, [lessonId, dispatch]);

  useEffect(() => {
    if (submitStatus === 3) {
      dispatch(
        getListMeTabThunk({
          lessonId,
          page: 0,
          userId: userAuthId
        })
      );
      dispatch(changeTabKeyThunk("3"));
    }
  }, [lessonId, dispatch, userAuthId, submitStatus]);

  useEffect(() => {
    if (lessonId) {
      switch (tabKey) {
        case 1:
          dispatch(getListDiscussionTabThunk({ lessonId, page: 0 }));
  
          break;
        case 2:
          dispatch(
            getListBoardTabThunk({
              lessonId,
              pageNumber: 0,
              userId: userAuthId
            })
          );
          break;
        case 3:
          dispatch(
            getListMeTabThunk({
              lessonId,
              pageNumber: 0,
              userId: userAuthId
            })
          );
          break;
        default:
          break;
    }
    
    }
  }, [tabKey, dispatch, lessonId, userAuthId]);

  return (
    <div className="discuss">
      <CustomTab>
        <TabPane
          tab={
            <span>
              <MessageOutlined />
              Discussion
            </span>
          }
          key="1"
        >
          <DiscussionTab listDiscussion={listDiscussion} />
          {/* <DiscussionTab /> */}
        </TabPane>
        <TabPane
          tab={
            <span>
              <UnorderedListOutlined />
              Board
            </span>
          }
          key="2"
        >
          <BoardTab listBoard={listBoard} />
        </TabPane>
        <TabPane
          tab={
            <span>
              <FileDoneOutlined />
              Me
            </span>
          }
          key="3"
        >
          <MeTab listAnswer={listAnswer} />
        </TabPane>
      </CustomTab>
    </div>
  );
};

export default Discuss;
