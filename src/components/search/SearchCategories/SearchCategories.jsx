import React from "react";

import "./styles.scss";

import { Divider } from "antd";

import lodash from "lodash";
import { useSelector } from "react-redux";

const SearchCategories = ({ searchCategory, handleSearch }) => {
  const listCategory = useSelector((state) => state.shared.categories);

  const renderCategories = () => {
    return listCategory
      .filter((cate) => cate.code !== "COMP")
      .map((parent) => (
        <div className="search-page__categories--group" key={parent.code}>
          <h3>{lodash.capitalize(parent.name)}</h3>
          <div className="search-page__categories--group-list">
            <span
              className={`search-page__categories--cate ${
                searchCategory === parent.id
                  ? "search-page__categories--cate-active"
                  : ""
              }`}
              onClick={() => {
                handleSearch({ category: parent.id });
              }}
            >
              <Divider type="vertical" />
              All
            </span>
            {parent.listChildCate.map((child) => (
              <span
                key={child.code + child.name}
                className={`search-page__categories--cate ${
                  searchCategory === child.id
                    ? "search-page__categories--cate-active"
                    : ""
                }`}
                onClick={() => {
                  handleSearch({ category: child.id });
                }}
                title={child.name}
              >
                <Divider type="vertical" />
                {child.code}
              </span>
            ))}
          </div>
        </div>
      ));
  };

  return <div className="search-page__categories">{renderCategories()}</div>;
};

export default SearchCategories;
