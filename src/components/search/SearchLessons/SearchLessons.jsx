import React from "react";

import "./styles.scss";

import { Divider } from "antd";
import { useSelector } from "react-redux";

import Button from "../../shared/Button/Button";
import { Link } from "react-router-dom";

import { regex } from "../../../config/contants/regex";

import { mapPriority } from "../../../utils/index";
import { get } from "lodash";

const Lesson = ({ detail, marks, parent, categoryId }) => {
  const practiceLink = () => {
    if (detail.categories[0].parentCode.match(regex.PTE_CODE)) {
      return `/practice/${parent}/${detail.categories[0].code}/${categoryId}/${detail.id}`;
    } else {
      return `/practice/${parent}/${detail.categories[0].parentCode}/${categoryId}/${detail.id}`;
    }
  };

  return (
    <div className="search-lesson__item">
      <div className="search-lesson__item--head">
        <h3>
          #{detail.zorder} {detail.title}
        </h3>
        <div className="search-lesson__item--mark">
          <i
            className="fa-solid fa-bookmark"
            style={{
              fontSize: 23,
              color: mapPriority(get(detail, "priorities[0].priority"), marks),
              cursor: "pointer",
            }}
          ></i>
        </div>
      </div>
      <p
        className="search-lesson__item--content"
        dangerouslySetInnerHTML={{
          __html: detail.content,
        }}
      ></p>
      <div className="search-lesson__item--footer">
        <Link to={practiceLink()}>
          <Button size="small">Practice</Button>
        </Link>
      </div>
      <Divider />
    </div>
  );
};

const SearchLessons = ({ lessonList, parent, categoryId }) => {
  const marks = useSelector((state) => state.shared.marks);
  const renderLessons = () => {
    return lessonList.map((lesson, index) => {
      return (
        <Lesson
          key={index}
          detail={lesson}
          parent={parent}
          categoryId={categoryId}
          marks={marks}
        />
      );
    });
  };

  return <div className="search-lesson">{renderLessons()}</div>;
};

export default SearchLessons;
