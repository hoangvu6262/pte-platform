import React from "react";
import { CSS } from "@dnd-kit/utilities";

export const Item = React.memo(
  React.forwardRef(
    (
      {
        hasSubmitted,
        taskId,
        dragOverlay,
        dragging,
        listeners,
        style,
        transition,
        transform,
        value,
        isCorrect,
        ...props
      },
      ref
    ) => {
      const changeBackgroundColor = () => {
        if (isCorrect === true) {
          return "#a0d8d24d";
        } else if (isCorrect === false) {
          return "#efdddd4d";
        } else return "rgb(245, 246, 247)";
      };

      return (
        <div
          ref={ref}
          style={{
            transform: CSS.Transform.toString(transform),
            transition,
            backgroundColor: changeBackgroundColor(),
            minWidth: "150px",
            color: "#868585",
            borderRadius: "5px",
            textAlign: "center",
            padding: 1,
            cursor: "grab",
            touchAction: "manipulation",
            fontSize: 16,
            boxShadow: "rgba(0, 0, 0, 0.24) 0px 3px 8px",
          }}
        >
          <div {...listeners} {...props} tabIndex={0}>
            {typeof isCorrect === "boolean" && (
              <span
                style={{
                  color: `${isCorrect ? "green" : "red"}`,
                  marginRight: 20,
                }}
              >
                <i
                  className={`fa-solid fa-${isCorrect ? "check" : "xmark"}`}
                ></i>
              </span>
            )}
            {value}
          </div>
        </div>
      );
    }
  )
);

Item.displayName = "Item";
