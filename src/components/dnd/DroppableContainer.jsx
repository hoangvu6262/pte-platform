import React from "react";
import { useDroppable } from "@dnd-kit/core";

export default function DroppableContainer({
  children,
  id,
  items,
  isCorrect,
  allBlanksEmpty,
  isAnswerVisible,
}) {
  const { over, isOver, setNodeRef } = useDroppable({
    id,
  });

  const isOverContainer = isOver || (over ? items.includes(over.id) : false);

  const backgroundColorShow = () => {
    if (isOverContainer) {
      return "rgb(204, 204, 204)";
    }

    if (!allBlanksEmpty && typeof isCorrect === "boolean") {
      if (isCorrect) {
        return "rgb(18, 211, 191)";
      } else {
        return "rgb(255, 102, 102)";
      }
    }
    return "rgb(204, 204, 204)";
  };

  return (
    <div
      ref={setNodeRef}
      style={{
        display: "inline-block",
        minWidth: "150px",
        borderRadius: 5,
        height: "35px",
        marginRight: "0px !important",
        border: `2px solid ${backgroundColorShow()}`,
        transition: "background-color .35s ease",
      }}
    >
      {children.length ? (
        children
      ) : (
        <>
          {isAnswerVisible && !isCorrect ? (
            <span
              style={{
                color: `red`,
                marginLeft: 15,
                fontSize: 16,
              }}
            >
              <i className={`fa-solid fa-xmark`}></i>
            </span>
          ) : (
            <div>&nbsp;</div>
          )}
        </>
      )}
    </div>
  );
}
