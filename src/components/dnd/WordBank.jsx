import React from "react";
import SortableItem from "./SortableItem";
import { SortableContext, rectSortingStrategy } from "@dnd-kit/sortable";
import { WORD_BANK } from "../../utils/index";
import { useDroppable } from "@dnd-kit/core";

export default function WordBank({ taskId, items, hasSubmitted }) {
  const { setNodeRef } = useDroppable({
    taskId,
  });
  return (
    <div>
      <SortableContext
        items={items[WORD_BANK].items}
        strategy={rectSortingStrategy}
      >
        <div
          ref={setNodeRef}
          style={{
            display: "flex",
            flexWrap: "wrap",
            gap: 15,
            marginTop: "30px",
            marginBottom: "20px",
            width: "100%",
          }}
          items={items[WORD_BANK].items}
        >
          {items[WORD_BANK].items.map((value) => {
            return (
              <SortableItem
                key={value}
                id={value}
                taskId={taskId}
                hasSubmitted={hasSubmitted}
              />
            );
          })}
        </div>
      </SortableContext>
    </div>
  );
}
