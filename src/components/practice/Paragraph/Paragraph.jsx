import React, { useState } from "react";
import "./styles.scss";

import Word from "../Word/Word";
import DictionaryModal from "../../modal/DictionaryModal/DictionaryModal";

import { handleWord } from "../../../utils/index";

export const ParagraphContext = React.createContext();

const Paragraph = ({ content, isAnswerVisible }) => {
  const [text, setText] = useState();
  const [isModalVisible, setIsModalVisible] = useState(false);

  //open Modal Dictionary
  const openModal = (word) => {
    setText(word);
    setIsModalVisible(true);
  };

  // render paragraph
  const renderParagraph = (sentence) => {
    return sentence
      ?.split(" ")
      .map((word, index) => {
        if (word) {
          const wordSplitted = handleWord(word);

          return (
            <span key={index}>
              <span>{wordSplitted[0]}</span>
              {wordSplitted[1] && (
                <Word
                  text={wordSplitted[1]}
                  setOpen={(word) => openModal(word)}
                />
              )}
              <span>{wordSplitted[2]}</span>
            </span>
          );
        }
        return "";
      })
      .reduce((prev, curr) => [prev, " ", curr]);
  };

  return (
    <ParagraphContext.Provider value={{ isAnswerVisible }}>
      <div className="custom-para">
        {content?.split("\n").map((sentence, index) => (
          <p key={index}>{renderParagraph(sentence)}</p>
        ))}
      </div>
      <DictionaryModal
        isModalVisible={isModalVisible}
        setIsModalVisible={setIsModalVisible}
        text={text}
      />
    </ParagraphContext.Provider>
  );
};

export default Paragraph;
