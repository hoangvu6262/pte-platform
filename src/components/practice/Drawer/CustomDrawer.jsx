import React from "react";
import { Drawer } from "antd";
import { RightOutlined } from "@ant-design/icons";

import "./styles.scss";

const CustomDrawer = ({
  visible,
  setVisible,
  width = "90%",
  placement = "right",
  children,
}) => {
  const onClose = () => {
    setVisible(false);
  };

  const showDefaultDrawer = () => {
    setVisible(!visible);
  };
  return (
    <Drawer
      placement={placement}
      width={width}
      onClose={onClose}
      visible={visible}
      closable={false}
      className="custom-drawer"
    >
      <div className="custom-drawer__container">{children}</div>
      <div className="custom-drawer__close" onClick={showDefaultDrawer}>
        <div className="custom-drawer__button">
          <div
            className={`custom-drawer__wrapper ${!visible ? " close" : null}`}
          >
            <RightOutlined />
          </div>
        </div>
      </div>
    </Drawer>
  );
};

export default CustomDrawer;
