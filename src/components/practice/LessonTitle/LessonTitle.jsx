import React from "react";

import { Tooltip } from "antd";

import "./styles.scss";

import { useSelector, useDispatch } from "react-redux";

import { useNavigate, useLocation } from "react-router-dom";

import {
  getNextPageLessonThunk,
  getPrevPageLessonThunk,
} from "../../../redux/slices/lesson/lessonSlice";

const LessonTitle = ({ number }) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { pathname } = useLocation();

  const { listLesson, pagination } = useSelector(
    (state) => state.lesson.listLesson
  );

  const { status } = useSelector((state) => state.lesson.lessonDetail);

  const contentDetail = useSelector(
    (state) => state.lesson.lessonDetail.content
  );

  const navigateLink = (path, id) => {
    const splittedPath = path.split("/");
    splittedPath[splittedPath.length - 1] = id;

    return splittedPath.join("/");
  };

  const findCurIndex = () => {
    const curIndex = listLesson.findIndex(
      (lesson) => lesson.id === contentDetail.id
    );
    return curIndex;
  };

  const disabledNext = () => {
    let disable = false;

    if (
      findCurIndex() === listLesson.length - 1 &&
      pagination.numberOfCurrentPage === pagination.totalPages - 1
    ) {
      disable = true;
    }

    return disable;
  };

  const disabledPrev = () => {
    let disable = false;

    if (findCurIndex() === 0 && pagination.numberOfCurrentPage === 0) {
      disable = true;
    }

    return disable;
  };

  const handleNext = () => {
    if (
      findCurIndex() === listLesson.length - 1 &&
      pagination.numberOfCurrentPage < pagination.totalPages - 1
    ) {
      dispatch(
        getNextPageLessonThunk({
          pageNumber: pagination.numberOfCurrentPage + 1,
        })
      );
    } else {
      const nextLesson = listLesson[findCurIndex() + 1];
      navigate(navigateLink(pathname, nextLesson.id));
    }
  };

  const handlePrev = () => {
    if (findCurIndex() === 0 && pagination.numberOfCurrentPage > 0) {
      dispatch(
        getPrevPageLessonThunk({
          pageNumber: pagination.numberOfCurrentPage - 1,
        })
      );
    } else {
      const prevLesson = listLesson[findCurIndex() - 1];
      navigate(navigateLink(pathname, prevLesson.id));
    }
  };
  return (
    <div className="lesson-title-container">
      <div className="lesson-title-container__title">
        {status === "error" ? (
          ""
        ) : (
          <>
            <Tooltip title="Previous Lesson">
              <div
                className={`lesson-title-arrow lesson-title-previous ${
                  disabledPrev() && "lesson-title-arrow-disabled"
                }`}
                onClick={handlePrev}
              >
                <i className="fa-solid fa-arrow-left-long"></i>
              </div>
            </Tooltip>
            <h3>{`${number}`}</h3>
            <Tooltip title="Next Lesson">
              <div
                className={`lesson-title-arrow lesson-title-next ${
                  disabledNext() && "lesson-title-arrow-disabled"
                }`}
                onClick={handleNext}
              >
                <i className="fa-solid fa-arrow-right-long"></i>
              </div>
            </Tooltip>
          </>
        )}
      </div>
    </div>
  );
};

export default LessonTitle;
