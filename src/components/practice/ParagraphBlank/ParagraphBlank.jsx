import React, { useState, useEffect } from "react";
import "./styles.scss";

import Word from "../Word/Word";
import DictionaryModal from "../../modal/DictionaryModal/DictionaryModal";
import BlankInput from "../BlankInput/BlankInput";
import BlankSelect from "../BlankSelect/BlankSelect";
import { Blank } from "../../../container/DnDKit/DnDKit";

import answerSlices from "../../../redux/slices/answer/answerSlice";
import { useDispatch, useSelector } from "react-redux";

import { handleWord } from "../../../utils/index";
import {
  filteredEmbeddedAnswer,
  sortQuestions,
  breakBlankSpace,
} from "../../../utils/practice";

export const ParagraphBlankContext = React.createContext();

const checkBlankAnswer = (answerList) => {
  let blank = true;
  answerList.forEach((answer) => {
    if (answer.questionResponseUsers.length > 0) {
      blank = false;
    }
  });

  return blank;
};

const ParagraphBlank = ({ content, questions, isAnswerVisible }) => {
  const [text, setText] = useState();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [answers, setAnswers] = useState([]);
  const submitStatus = useSelector((state) => state.answer.status);

  const dispatch = useDispatch();

  useEffect(() => {
    if (checkBlankAnswer(answers)) {
      dispatch(answerSlices.actions.updateAnswering(""));
    } else {
      dispatch(answerSlices.actions.updateAnswering(answers));
    }
  }, [answers, dispatch]);

  useEffect(() => {
    if (submitStatus === 1 && questions) {
      setAnswers(filteredEmbeddedAnswer(sortQuestions(questions)));
    } else if (submitStatus === 1) {
      setAnswers([]);
    }
  }, [submitStatus, questions]);

  //open Modal Dictionary
  const openModal = (word) => {
    setText(word);
    setIsModalVisible(true);
  };
  // render paragraph
  const renderParagraph = (sentence) => {
    return sentence
      ?.split(" ")
      .map((word, index) => {
        if (word) {
          // Check
          if (word.includes("**")) {
            // **(1)
            if (word.split("//")[0].length > 5) {
              let listAns = word.split("//")[0];
              listAns = listAns.slice(5, word.length).split(",");

              return (
                <BlankSelect
                  id={word[3]}
                  listAns={listAns.map((ans) => breakBlankSpace(ans))}
                  answer={
                    breakBlankSpace(word.split("//")[1])
                      ? breakBlankSpace(word.split("//")[1])
                      : ""
                  }
                />
              );
            }

            return (
              <BlankInput
                id={word[3]}
                answer={word.split("//")[1] ? word.split("//")[1] : ""}
              />
            );
          }

          if (word.includes("$$")) {
            return (
              <Blank
                solution={word.split("//")[1] ? word.split("//")[1] : ""}
              />
            );
          }

          const wordSplitted = handleWord(word);

          return (
            <span key={index}>
              <span>{wordSplitted[0]}</span>
              {wordSplitted[1] && (
                <Word
                  text={wordSplitted[1]}
                  setOpen={(word) => openModal(word)}
                />
              )}
              <span>{wordSplitted[2]}</span>
            </span>
          );
        }
        return "";
      })
      .reduce((prev, curr) => [prev, " ", curr]);
  };

  return (
    <ParagraphBlankContext.Provider
      value={{ isAnswerVisible, answers, setAnswers, openModal }}
    >
      <div className="custom-para">
        {content?.split("\n").map((sentence, index) => (
          <div key={index}>{renderParagraph(sentence)}</div>
        ))}
      </div>
      <DictionaryModal
        isModalVisible={isModalVisible}
        setIsModalVisible={setIsModalVisible}
        text={text}
      />
    </ParagraphBlankContext.Provider>
  );
};

export default ParagraphBlank;
