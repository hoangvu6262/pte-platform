import React from "react";
import { Divider } from "antd";
import LessonTitle from "../LessonTitle/LessonTitle";
import CustomTitle from "../../shared/CustomTitle/CustomTitle";
import { useSelector } from "react-redux";

import "./styles.scss";

const PageHeader = ({ title, type, content, isDrawer = false, idExercise }) => {
  const { status } = useSelector((state) => state.lesson.lessonDetail);

  return (
    <>
      <CustomTitle title={title} name={type} />
      {isDrawer || (
        <div className="page-header__content">
          <p>{content ? content : ""}</p>
        </div>
      )}
      <Divider className="page-header__hr" />
      {status === "error" || status === "loading" ? (
        ""
      ) : (
        <LessonTitle number={idExercise} content={title} />
      )}
    </>
  );
};

export default React.memo(PageHeader);
