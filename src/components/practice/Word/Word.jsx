import React from "react";
import "./styles.scss";

const Word = ({ text, disable, setOpen }) => {
  //open Modal Dictionary

  const handleClick = () => {
    if (!disable) {
      setOpen(text);
    }
  };

  return (
    <span
      className={`word ${disable ? "disable" : ""}`}
      onClick={() => {
        handleClick();
      }}
    >
      {text}
    </span>
  );
};

export default Word;
