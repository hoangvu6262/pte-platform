import React, { useState, useEffect, useContext } from "react";
import { Input } from "antd";

import { ParagraphBlankContext } from "../ParagraphBlank/ParagraphBlank";
import { useSelector, useDispatch } from "react-redux";
import "./styles.scss";

import Word from "../Word/Word";

const BlankInput = ({ id, answer }) => {
  const dispatch = useDispatch();
  const [text, setText] = useState("");

  const { isAnswerVisible, setAnswers, answers } = useContext(
    ParagraphBlankContext
  );

  const submitStatus = useSelector((state) => state.answer.status);

  useEffect(() => {
    if (submitStatus === 1) {
      setText("");
    }
  }, [submitStatus, dispatch]);

  const handleChange = (e) => {
    setText(e.target.value);
    let answerList = [...answers];

    answerList[id - 1] = {
      ...answerList[id - 1],
      questionResponseUsers: [{ valueText: e.target.value }],
    };

    setAnswers([...answerList]);
  };

  return (
    <div
      className={`blank-input ${
        isAnswerVisible && (text === answer ? "right" : "wrong")
      }`}
    >
      {isAnswerVisible &&
        (text === answer ? <i className="fa-solid fa-check"></i> : "")}
      {isAnswerVisible &&
        (text !== answer ? <i className="fa-solid fa-xmark"></i> : "")}
      <Input
        value={text}
        disabled={submitStatus === 3}
        onChange={(e) => handleChange(e)}
      />
      {answer !== "" && isAnswerVisible ? (
        <span> (Answer: {<Word text={answer} />}) </span>
      ) : (
        ""
      )}
    </div>
  );
};

export default BlankInput;
