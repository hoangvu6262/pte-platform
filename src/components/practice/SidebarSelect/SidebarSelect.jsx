import React, { useState, useEffect } from "react";

import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { Select } from "antd";

import lessonSlices, {
  getListLessonThunk,
} from "../../../redux/slices/lesson/lessonSlice";

const { Option } = Select;

const SidebarSelect = ({ listFilter, label, idFil }) => {
  const { id } = useParams();
  const [filter, setFilter] = useState(listFilter[0].name);
  const dispatch = useDispatch();
  const { filters } = useSelector((state) => state.lesson.listLesson);

  const handleFilterChange = (value) => {
    const filtered = listFilter.filter((filter) => filter.name === value)[0];
    dispatch(
      lessonSlices.actions.updateFilter({
        categoryIds: id - 0,
        ...filtered.value,
      })
    );
    dispatch(getListLessonThunk());
    setFilter(value);
  };

  useEffect(() => {
    const filteredValue = listFilter.filter(
      (fil) => fil.value[idFil] === filters[idFil]
    )[0];
    if (filteredValue) {
      setFilter(filteredValue.name);
    }
  }, [filters, idFil, listFilter]);

  return (
    <div className="">
      <Select
        value={filter}
        label={label}
        onChange={handleFilterChange}
        size="small"
      >
        {listFilter.map((filterItem) => {
          return (
            <Option value={filterItem.name} key={filterItem.name}>
              {filterItem.name}
            </Option>
          );
        })}
      </Select>
    </div>
  );
};

export default SidebarSelect;
