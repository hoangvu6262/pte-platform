import React, { useState, useEffect } from "react";
import { Dropdown, Menu, Space, Tooltip } from "antd";
import { useSelector, useDispatch } from "react-redux";

import "./styles.scss";

import Button from "../../shared/Button/Button";
import TestedModal from "../../modal/TestedModal/TestedModal";
import { mapPriority } from "../../../utils/index";
import { getListLessonThunk } from "../../../redux/slices/lesson/lessonSlice";

import testedAPI from "../../../redux/api/practice/testedAPI";
import globalAPI from "../../../redux/api/practice/globalAPI";
import markAPI from "../../../redux/api/practice/markAPI";

const LessonTools = ({ openModal, lessonId, priority }) => {
  const [count, setCount] = useState(0);
  const [testedVisible, setTestedVisible] = useState(false);
  const [mark, setMark] = useState("rgb(204, 204, 204)");
  const [listMark, setListMark] = useState([]);
  const [listPriority, setListPriority] = useState([]);

  const { getCount } = testedAPI;
  const { getCategories } = globalAPI;
  const { setPriority, deletePriority } = markAPI;

  const { listLesson } = useSelector((state) => state.lesson);
  const marks = useSelector((state) => state.shared.marks);

  const dispatch = useDispatch();

  const onChangeMark = async ({ key }) => {
    setMark(mapPriority(key, listPriority));
    if (parseInt(key) !== 0) {
      await setPriority(parseInt(key), lessonId).then(() => {
        dispatch(getListLessonThunk(listLesson.filters));
      });
    } else {
      deletePriority(lessonId).then(() => {
        dispatch(getListLessonThunk(listLesson.filters));
      });
    }
  };

  const menu = (
    <Menu
      className="number-menu"
      onClick={onChangeMark}
      items={[
        ...listMark,
        {
          key: "0",
          label: "Unmark",
          icon: (
            <i
              className="fa-solid fa-bookmark"
              style={{
                fontSize: 23,
                color: "rgb(204, 204, 204)",
                marginRight: "15px",
              }}
            ></i>
          ),
        },
      ]}
    />
  );

  const openTested = () => {
    setTestedVisible(true);
  };

  useEffect(() => {
    getCount(lessonId).then((res) => {
      setCount(res);
    });
  }, [getCount, lessonId, testedVisible]);

  useEffect(() => {
    const listPriority = marks.filter((item) => item.parentCode !== undefined);
    setListPriority(listPriority);
    setMark(mapPriority(priority, listPriority));
    const mapListMask = listPriority.map((mark) => {
      return {
        key: mark.code,
        label: mark.name,
        color: mark.categoryValues || "rgb(204, 204, 204)",
        icon: (
          <i
            className="fa-solid fa-bookmark"
            style={{
              fontSize: 23,
              color: mark.categoryValues || "rgb(204, 204, 204)",
              marginRight: "15px",
            }}
          ></i>
        ),
      };
    });

    setListMark(mapListMask);
  }, [lessonId, priority, getCategories, marks]);

  return (
    <div className="lesson-tools">
      <Dropdown
        overlay={menu}
        className="lesson-tools-dropdown"
        placement="bottom"
      >
        <Space>
          <i
            className="fa-solid fa-bookmark"
            style={{
              fontSize: 23,
              color: mark,
              cursor: "pointer",
            }}
          ></i>
        </Space>
      </Dropdown>

      <div className="lesson-tools-hint" onClick={() => openModal()}>
        <Tooltip title="Dict-mode" placement="bottom">
          <img
            alt="Dict-mode"
            src="https://img.icons8.com/fluency/48/000000/light-on.png"
          />
        </Tooltip>
      </div>
      <div className="lesson-tools-btn" onClick={openTested}>
        <Button size={"small"}>Tested {count > 0 ? `(${count})` : ""}</Button>
      </div>
      <TestedModal
        setIsModalVisible={setTestedVisible}
        isModalVisible={testedVisible}
        lessonId={lessonId}
        count={count}
      />
    </div>
  );
};

export default LessonTools;
