import React, { useState, useEffect } from "react";
import "./styles.scss";

import WordHighlight from "../WordHighlight/WordHighlight";
import DictionaryModal from "../../modal/DictionaryModal/DictionaryModal";

import { useSelector, useDispatch } from "react-redux";
import answerSlices from "../../../redux/slices/answer/answerSlice";

import { handleWord } from "../../../utils/index";

export const HighLightCorrectContext = React.createContext();

const mappedAnswer = (answers, otherId) => {
  return answers.map((ans) => {
    const splittedAns = ans.split("/");

    return {
      question: { id: splittedAns[1] ? splittedAns[1] - 0 : otherId },
      questionResponseUsers: [{ valueText: splittedAns[0] }],
    };
  });
};

const ParagraphHighlight = ({ content, isAnswerVisible, questions }) => {
  const [text, setText] = useState();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [answers, setAnswers] = useState([]);
  const submitStatus = useSelector((state) => state.answer.status);

  const dispatch = useDispatch();

  useEffect(() => {
    const filterOtherId = questions.filter((ques) => ques.name === "orther");
    dispatch(
      answerSlices.actions.updateAnswering(
        mappedAnswer(
          answers,
          filterOtherId.length > 0 ? filterOtherId[0].id : 0
        )
      )
    );
  }, [answers, dispatch, questions]);

  useEffect(() => {
    if (submitStatus === 1) {
      setAnswers([]);
    }
  }, [submitStatus]);

  //open Modal Dictionary
  const openModal = (word) => {
    setText(word);
    setIsModalVisible(true);
  };

  const handleWordHighlight = (word, index) => {
    if (word[word.length - 1].match(/[^a-zA-Z0-9 ]/g)) {
      let splittedWord = word.slice(0, word.length - 1).split("//");

      return (
        <span key={index}>
          <WordHighlight
            text={splittedWord[0]}
            answer={splittedWord[2] ? splittedWord[2] : ""}
          />
          <span>{word[word.length - 1]}</span>
        </span>
      );
    } else {
      let splittedWord = word.split("//");
      return (
        <WordHighlight
          text={splittedWord[0]}
          key={index + word}
          answer={splittedWord[2] ? splittedWord[2] : ""}
          id={splittedWord[1] ? splittedWord[1] : ""}
        />
      );
    }
  };

  const renderParagraph = (sentence) => {
    return sentence
      ?.split(" ")
      .map((word, index) => {
        if (word) {
          const wordSplitted = handleWord(word);
          return (
            <span key={index}>
              <span>{wordSplitted[0]}</span>
              {wordSplitted[1] && handleWordHighlight(wordSplitted[1])}
              <span>{wordSplitted[2]}</span>
            </span>
          );
        }
        return "";
      })
      .reduce((prev, curr) => [prev, " ", curr]);
  };

  return (
    <HighLightCorrectContext.Provider
      value={{ answers, setAnswers, isAnswerVisible, openModal }}
    >
      <div className="custom-para-hl">
        {content?.split("\n").map((sentence, index) => (
          <p key={index}>{renderParagraph(sentence)}</p>
        ))}
      </div>
      <DictionaryModal
        isModalVisible={isModalVisible}
        setIsModalVisible={setIsModalVisible}
        text={text}
      />
    </HighLightCorrectContext.Provider>
  );
};

export default ParagraphHighlight;
