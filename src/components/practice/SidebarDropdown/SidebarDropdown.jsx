import React, { useState, useEffect } from "react";

import { listSort, listPracStatus, listShadowing } from "./data";
import SidebarSelect from "../SidebarSelect/SidebarSelect";

import { useParams } from "react-router-dom";

import lessonAPI from "../../../redux/api/practice/lessonAPI";
import { useSelector } from "react-redux";

import "./styles.scss";

const { getTypeList } = lessonAPI;

const SidebarDropdown = () => {
  const [typeList, setTypeList] = useState([]);
  const [priorityList, setPriorityList] = useState([]);
  const marks = useSelector((state) => state.shared.marks);

  const { id } = useParams();

  useEffect(() => {
    getTypeList(id)
      .then((res) => {
        const mappedTypeList = res.map((item) => {
          return {
            value: {
              categoryId: item.id,
            },
            name: item.name,
          };
        });

        setTypeList([
          {
            value: {
              categoryId: id - 0,
            },
            name: "[Type]",
          },
          ...mappedTypeList,
        ]);
      })
      .catch(() => {
        setTypeList([]);
      });
    const mappedPriorityList = marks.map((mark) => {
      return {
        value: {
          priority: parseInt(mark.code),
        },
        name: mark.name,
      };
    });
    setPriorityList([
      {
        value: {
          priority: null,
        },
        name: "Mark",
      },
      {
        value: {
          priority: 0,
        },
        name: "All Marked",
      },
      ...mappedPriorityList,
      {
        value: {
          priority: -1,
        },
        name: "Not Marked",
      },
    ]);
  }, [id, marks]);

  return (
    <div className="sidebar-dropdown">
      <SidebarSelect listFilter={listSort} idFil="sort" label="Sort" />
      {priorityList.length > 0 && (
        <SidebarSelect
          listFilter={priorityList}
          idFil="priority"
          label="priority"
        />
      )}

      <SidebarSelect
        listFilter={listPracStatus}
        idFil="sort"
        label="Statuses"
      />
      {id === "3" && (
        <SidebarSelect
          listFilter={listShadowing}
          idFil="shadowing"
          label="Shadowing"
        />
      )}

      {typeList.length > 1 && (
        <SidebarSelect listFilter={typeList} idFil="categoryId" label="Type" />
      )}
    </div>
  );
};

export default SidebarDropdown;
