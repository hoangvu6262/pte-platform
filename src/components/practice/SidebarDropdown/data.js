export const listPracStatus = [
  {
    value: {
      practiceStatus: null,
    },
    name: "[Practice Statuses]",
  },
  {
    value: {
      practiceStatus: true,
    },
    name: "Practiced",
  },
  {
    value: {
      practiceStatus: false,
    },
    name: "Unpracticed",
  },
];

export const listShadowing = [
  {
    value: {
      shadowing: null,
    },
    name: "[Shadowing]",
  },
  {
    value: {
      shadowing: true,
    },
    name: "Shadowing",
  },
];

export const listExplanation = [
  {
    value: {
      explanation: null,
    },
    name: "[Explanation]",
  },
  {
    value: {
      explanation: false,
    },
    name: "Explained",
  },
];

export const listSort = [
  {
    value: {
      sort: 1,
    },
    name: "New",
  },
  {
    value: {
      sort: 2,
    },
    name: "No.",
  },
  {
    value: {
      sort: 3,
    },
    name: "Title",
  },
];
