import React, { useState, useEffect, useContext } from "react";
import "./styles.scss";

import { useSelector } from "react-redux";

import { HighLightCorrectContext } from "../ParagraphHighlight/ParagraphHighlight";
import Word from "../Word/Word";

const WordHighlight = ({ text, answer, id }) => {
  const [checked, setChecked] = useState(false);
  const [result, setResult] = useState("");
  const submitStatus = useSelector((state) => state.answer.status);

  const { answers, setAnswers, isAnswerVisible, openModal } = useContext(
    HighLightCorrectContext
  );

  useEffect(() => {
    if (submitStatus === 3) {
      if (checked && answer !== "") {
        setResult("right");
      } else if (checked && answer === "") {
        setResult("wrong");
      } else if (answer !== "") {
        setResult("default");
      } else {
        setResult("");
      }
    } else if (submitStatus === 1) {
      setResult("");
      setChecked(false);
    }
    if (isAnswerVisible) {
      if (checked && answer !== "") {
        setResult("right");
      } else if (checked && answer === "") {
        setResult("wrong");
      } else if (answer !== "") {
        setResult("default");
      } else {
        setResult("");
      }
    } else {
      setResult("");
    }
  }, [submitStatus, isAnswerVisible, answer, checked]);

  //open Modal Dictionary
  const handleClick = () => {
    if (submitStatus === 1 || submitStatus === 2) {
      if (!checked) {
        setChecked(true);
        if (id) {
          setAnswers([...answers, text + "/" + id]);
        } else {
          setAnswers([...answers, text]);
        }
      } else {
        let newList = [...answers];
        if (id) {
          newList.splice(newList.indexOf(text + "/" + id), 1);
        } else {
          newList.splice(newList.indexOf(text), 1);
        }
        setChecked(false);
        setAnswers(newList);
      }
    }
  };

  return (
    <>
      <span
        className={`
      word-highlight ${checked ? "checked" : ""} 
      ${result}
      `}
        onClick={() => {
          handleClick();
        }}
      >
        {result === "right" ? <i className="fa-solid fa-check"></i> : ""}
        {result === "wrong" ? <i className="fa-solid fa-xmark"></i> : ""}

        {submitStatus === 3 && isAnswerVisible ? (
          <Word
            text={text}
            disable={text[0]?.match(/^[0-9]/)}
            setOpen={(text) => {
              openModal(text);
            }}
          />
        ) : (
          text
        )}
        {/* {text} */}
      </span>
      {submitStatus === 3 && isAnswerVisible && answer !== "" ? (
        <span className="word-highlight-answer"> (Answer: {answer}) </span>
      ) : (
        ""
      )}
    </>
  );
};

export default WordHighlight;
