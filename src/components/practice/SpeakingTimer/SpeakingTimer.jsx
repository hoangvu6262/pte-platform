import React, { useEffect, useRef } from "react";
import Countdown from "react-countdown";

import "./styles.scss";

const SpeakingTimer = ({
  time,
  overtime = false,
  title,
  isPrepare,
  setIsPrepare,
  isRecording,
  startRecording,
  stopRecording,
  keyTimer,
  havePermissions,
}) => {
  const timerRef = useRef();
  useEffect(() => {
    if (!isRecording && isPrepare === false) {
      timerRef.current.pause();
    }
  }, [isRecording, isPrepare]);

  useEffect(() => {
    if (isRecording) {
      timerRef.current.start();
    }
  }, [isRecording]);

  useEffect(() => {
    if (havePermissions === false) {
      timerRef.current.stop();
    }
  }, [havePermissions]);

  const onComplete = () => {
    if (isPrepare) {
      setIsPrepare(false);
      // startRecording();
    } else {
      stopRecording();
    }
  };

  return (
    <div className="countdown">
      <Countdown
        ref={timerRef}
        onComplete={onComplete}
        key={keyTimer}
        date={Date.now() + time * 1000}
        overtime={overtime}
        renderer={(props) => {
          const { formatted, total } = props;
          return (
            <>
              <span>
                {title} {total >= 0 ? null : "-"}
                {formatted.minutes}:{formatted.seconds}
              </span>
            </>
          );
        }}
      />
    </div>
  );
};

export default React.memo(SpeakingTimer);
