import React, { useState, useEffect, useContext } from "react";
import { Select } from "antd";

import { ParagraphBlankContext } from "../ParagraphBlank/ParagraphBlank";
import { useSelector, useDispatch } from "react-redux";
import "./styles.scss";

import Word from "../Word/Word";

const { Option } = Select;

const BlankSelect = ({ id, listAns, answer }) => {
  const dispatch = useDispatch();
  const [choice, setChoice] = useState("");

  const { isAnswerVisible, setAnswers, answers } = useContext(
    ParagraphBlankContext
  );

  const submitStatus = useSelector((state) => state.answer.status);

  useEffect(() => {
    if (submitStatus === 1) {
      if (choice) {
        setChoice("");
      }
    }
  }, [submitStatus, dispatch, choice]);

  const handleChange = (value) => {
    setChoice(value);
    let answerList = [...answers];

    answerList[id - 1] = {
      ...answerList[id - 1],
      questionResponseUsers: [{ valueText: value }],
    };

    setAnswers([...answerList]);
  };

  return (
    <div
      className={`blank-select ${
        isAnswerVisible && (choice === answer ? "right" : "wrong")
      }`}
    >
      {isAnswerVisible &&
        (choice === answer ? <i className="fa-solid fa-check"></i> : "")}
      {isAnswerVisible &&
        (choice !== answer ? <i className="fa-solid fa-xmark"></i> : "")}
      <Select onChange={handleChange} value={choice}>
        {listAns.map((ans, index) => (
          <Option key={index} value={ans} disabled={submitStatus === 3}>
            {ans}
          </Option>
        ))}
      </Select>
      {answer !== "" && isAnswerVisible ? (
        <span> (Answer: {<Word text={answer} />}) </span>
      ) : (
        ""
      )}
    </div>
  );
};

export default BlankSelect;
