import React, { useState, useEffect, useRef, useCallback } from "react";
import { Button, Select } from "antd";
import moment from "moment";
import { useSelector } from "react-redux";

import "./styles.scss";

const { Option } = Select;

const CustomAudio = ({ audioList, isPlaying, setIsPlaying, isRecording }) => {
  const [trackProgress, setTrackProgress] = useState(0);
  const [audioSrc, setAudioSrc] = useState(audioList[0]?.cdnLink);

  const [volumeAudio, setVolumeAudio] = useState(1);
  const [speechAudio, setSpeechAudio] = useState(1);
  const [speakerVocal, setSpeakerVocal] = useState(audioList[0]?.voiceName);

  // Refs
  const audioRef = useRef(new Audio(audioSrc));
  const intervalRef = useRef();
  const isReady = useRef(false);

  // Destructure for conciseness
  const { duration } = audioRef.current;

  const { status } = useSelector((state) => state.answer);

  const startTimer = useCallback(() => {
    // Clear any timers already running
    clearInterval(intervalRef.current);

    intervalRef.current = setInterval(() => {
      if (!audioRef.current.ended) {
        setTrackProgress(audioRef.current.currentTime);
      } else {
        setIsPlaying(false);
      }
    }, [100]);
  }, [setIsPlaying]);

  useEffect(() => {
    // Pause and clean up on unmount
    audioRef.muted = true;

    return () => {
      audioRef.current.pause();
      clearInterval(intervalRef.current);
    };
  }, []);

  useEffect(() => {
    audioRef.current.pause();

    audioRef.current = new Audio(audioSrc);
    setTrackProgress(audioRef.current.currentTime);

    if (isReady.current) {
      audioRef.current.play();
      setIsPlaying(true);
      startTimer();
    } else {
      // Set the isReady ref as true for the next pass
      isReady.current = true;
    }
  }, [audioSrc, setIsPlaying, startTimer]);

  useEffect(() => {
    if (status === 1) {
      audioRef.current.currentTime = 0;
      audioRef.current.play();
      setIsPlaying(true);
    } else if (status === 3) {
      audioRef.current.pause();
      setIsPlaying(false);
      audioRef.current.currentTime = 0;
    }
  }, [setIsPlaying, status]);

  const onPlayPauseClick = () => {
    setIsPlaying(!isPlaying);
  };

  useEffect(() => {
    if (isPlaying) {
      audioRef.current.play();
      startTimer();
    } else {
      audioRef.current.pause();
    }
  }, [isPlaying, startTimer]);

  useEffect(() => {
    if (isRecording) {
      audioRef.current.pause();
      setIsPlaying(false);
    }
  }, [isRecording, setIsPlaying]);

  const onScrub = (value) => {
    // Clear any timers already running
    audioRef.current.currentTime = value;
    setTrackProgress(audioRef.current.currentTime);
    startTimer();
  };

  /**
   * set volume
   * onChangeVolume(value)
   * turnOnVolume()
   * turnOffVolume()
   */
  const onChangeVolume = (value) => {
    audioRef.current.volume = value;
    setVolumeAudio(audioRef.current.volume);
  };

  const turnOnVolume = () => {
    setVolumeAudio(audioRef.current.volume);
  };

  const turnOffVolume = () => {
    audioRef.current.volume = 0;
    setVolumeAudio(audioRef.current.volume);
  };

  /**
   * speech audio
   * onChangeSpeechAudio
   */
  const onChangeSpeechAudio = (value) => {
    audioRef.current.playbackRate = value;
    setSpeechAudio(audioRef.current.playbackRate);
  };

  const onChangeSpeaker = (value) => {
    setAudioSrc(
      audioList[audioList.findIndex((audio) => audio.voiceName === value)]
        .lessonMediaLink
    );
    setSpeakerVocal(value);
  };

  const currentPercentage = duration
    ? `${(trackProgress / duration) * 100}%`
    : "0%";
  const trackStyling = `
  -webkit-gradient(linear, 0% 0%, 100% 0%, color-stop(${currentPercentage}, #187498), color-stop(${currentPercentage}, rgb(204, 204, 204)))
`;

  const volumeStyling = `
  -webkit-gradient(linear, 0% 0%, 100% 0%, color-stop(${volumeAudio}, #187498), color-stop(${volumeAudio}, rgb(204, 204, 204)))
`;

  return (
    <div className="custom-audio">
      <div className="custom-audio__left">
        <div className="custom-audio__left-play-stop">
          {isPlaying ? (
            <Button
              shape="circle"
              onClick={onPlayPauseClick}
              icon={<i className="fa-solid fa-pause"></i>}
            />
          ) : (
            <Button
              shape="circle"
              onClick={onPlayPauseClick}
              icon={<i className="fa-solid fa-play"></i>}
            />
          )}
        </div>
        <div className="custom-audio__left-progress">
          <input
            type="range"
            defaultValue={0}
            min={0}
            value={trackProgress}
            step={0.1}
            onChange={(e) => onScrub(e.target.value)}
            max={duration ? duration : `${duration}`}
            style={{ background: trackStyling, width: "240px" }}
          />
        </div>
        <div className="custom-audio__left-time">
          <span>
            {moment.utc(trackProgress * 1000).format("mm:ss")}
            {"/"}
            {moment.utc(Math.floor(duration || 0) * 1000).format("mm:ss")}
          </span>
        </div>
      </div>
      <div className="custom-audio__speech">
        <Select
          className="custom-audio__speech-select"
          defaultValue={audioRef.current.playbackRate || 1}
          onChange={onChangeSpeechAudio}
          value={speechAudio}
        >
          <Option value={1.5}>X1.5</Option>
          <Option value={1.2}>X1.2</Option>
          <Option value={1}>X1.0</Option>
          <Option value={0.8}>X0.8</Option>
          <Option value={0.5}>X0.5</Option>
        </Select>

        {audioList.length > 1 && (
          <Select
            className="custom-audio__speech-select"
            defaultValue={speakerVocal}
            onChange={onChangeSpeaker}
            value={speakerVocal}
          >
            {audioList.map((item) => {
              return (
                <Option value={item.voiceName} key={item.id}>
                  {item.voiceName}
                </Option>
              );
            })}
          </Select>
        )}

        <div className="custom-audio__volume">
          <span>
            {volumeAudio === 0 ? (
              <i
                onClick={turnOnVolume}
                className="fa-solid fa-volume-xmark"
              ></i>
            ) : (
              <i
                onClick={turnOffVolume}
                className="fa-solid fa-volume-high"
              ></i>
            )}
          </span>

          <div>
            <input
              type="range"
              defaultValue={audioRef.current.volume || 1}
              min={0}
              value={volumeAudio}
              step={0.1}
              onChange={(e) => onChangeVolume(e.target.value)}
              max={1}
              style={{ background: volumeStyling, width: "130px" }}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default CustomAudio;
