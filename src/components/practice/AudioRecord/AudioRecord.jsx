import * as React from "react";

import "./styles.scss";

const AudioRecord = ({
  setIsPrepare,
  havePermissions,
  isRecording,
  audioURL,
  startRecording,
  stopRecording,
}) => {
  const renderText = () => {
    let text = "";
    if (isRecording) {
      text = "Click to STOP";
    } else if (!audioURL) {
      text = "Click to START";
    } else {
      text = "Done";
    }

    return text;
  };

  const start = () => {
    setIsPrepare(false);
    startRecording();
  };

  return (
    <>
      {havePermissions ? (
        <div className="audio-record">
          <div className="audio-record__progress"></div>
          <div className="audio-record__body">
            <div className="audio-record__body-text">{renderText()}</div>
            <div className="audio-record__body-button">
              {isRecording ? (
                <div
                  className="audio-record__microphone-button-stop"
                  onClick={stopRecording}
                >
                  <i className="fa-solid fa-microphone"></i>
                </div>
              ) : (
                <div
                  className="audio-record__microphone-button-start"
                  onClick={start}
                >
                  <i className="fa-solid fa-microphone"></i>
                </div>
              )}
            </div>
          </div>
        </div>
      ) : (
        <div className="audio-record__body-warning">
          Microphone permission is not granted.
        </div>
      )}
      {audioURL && <audio src={audioURL} title={"record.mp3"} controls></audio>}
    </>
  );
};

export default AudioRecord;
