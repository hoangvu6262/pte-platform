import React, { useState, useEffect } from "react";

import CustomModal from "../CustomModal";
import Button from "../../shared/Button/Button";
import Loading from "../../shared/Loading/Loading";

import { Table, Progress } from "antd";

import { useLocation } from "react-router-dom";

import { useSelector, useDispatch } from "react-redux";

import { toast } from "react-toastify";

import {
  getListMeTabThunk,
  getListBoardTabThunk,
} from "../../../redux/slices/discuss/discussSlice";

import AIScoreAPI from "../../../redux/api/AI/AIScoreAPI";
import responseAPI from "../../../redux/api/practice/responseAPI";

import { capitalizeLetter } from "../../../utils";

import "./styles.scss";

const { doAIScore } = responseAPI;

const ScoreModal = ({
  setIsModalVisible,
  isModalVisible,
  score,
  response,
  isAI,
  createDate,
  componentScores,
}) => {
  const [AIScoreStatus, setAIScoreStatus] = useState();
  const [message, setMessage] = useState("");

  const { content } = useSelector((state) => state.lesson.lessonDetail);
  const userAuthId = useSelector((state) => state.user.detail.id);
  const lessonDetail = useSelector((state) => state.lesson.lessonDetail.content)

  const location = useLocation();
  const dispatch = useDispatch();

  const AIScoreCases = () => {
    switch (location.pathname.split("/")[3]) {
      case "ra":
        return {
          rest: "read-aloud",
          body: {
            transcription: content.content,
            speech: response,
          },
        };
      case "rs":
        return {
          rest: "repeat-sentence",
          body: {
            transcription: content.content,
            speech: response,
          },
        };
      case "di":
        return {
          rest: "describe-image",
          body: {
            question_category: "",
            main_keywords: [],
            speech: response,
          },
        };
      case "rl":
        return {
          rest: "retell-lecture",
          body: {
            main_keywords: [],
            speech: response,
          },
        };
      case "as":
        return {
          rest: "answer-short-question",
          body: {
            solution: [],
            speech: response,
          },
        };
      case "swt":
        return {
          rest: "summary-writing-text",
          body: {
            question: content?.questionGroup?.questions[0].name,
            question_respond: response,
          },
        };
      case "essay":
        return {
          rest: "essay",
          body: {
            question: content?.questionGroup?.questions[0].name,
            question_respond: response,
          },
        };
      case "sst":
        return {
          rest: "summary-spoken-text",
          body: {
            question: content.content,
            question_respond: response,
          },
        };
      default:
        return;
    }
  };

  const handleDoAIScore = (scoreContent) => {
    const scoreKeys = Object.keys(scoreContent);

    return scoreKeys.map((scoreKey) => {
      return {
        component: scoreKey,
        score: scoreContent[scoreKey],
        question: {
          id: content?.questionGroup?.questions[0].id,
        },
        createDate: createDate,
      };
    });
  };

  const handleClose = () => {
    setIsModalVisible(false);
  };

  const generateCol = (component) => {
    if (component[0]) {
      return Object.keys(component[0])
        .filter((col) => col !== "maxScore")
        .map((col) => {
          return {
            title: capitalizeLetter(col),
            dataIndex: col,
            width: col === "suggestion" ? 200 : 1,
          };
        });
    }
  };

  const generateColData = (component) => {
    return component
      .filter((compo) => compo.component !== "overall")
      .map((data) => {
        return {
          ...data,
          component: capitalizeLetter(
            data.component
              .replace("score", "")
              .split(/[^A-Za-z0-9]/)
              .join(" ")
              .trim()
          ),
          score: data.maxScore ? `${data.score}/${data.maxScore}` : data.score,
          key: data.component,
        };
      });
  };

  useEffect(() => {
    if (isAI && isModalVisible) {
      setAIScoreStatus("loading");
      AIScoreAPI(AIScoreCases())
        .then((res) => {
          if (res?.message && !res.scores) {
            setMessage(res.message);
          } else {
            return res.scores;
          }
        })
        .then((res) => {
          return doAIScore(handleDoAIScore(res));
        })
        .then((res) => {
          setIsModalVisible(false);
          dispatch(
            getListMeTabThunk({
              lessonId: lessonDetail.id, pageNumber: 0, userId: userAuthId 
            })
          );
          dispatch(getListBoardTabThunk({ lessonId: lessonDetail.id, pageNumber: 0, userId: userAuthId }));
          toast.success("Success");
        })
        .catch((err) => {
          toast.error("AI Score failed.");
          setAIScoreStatus("error");
        });
    } else {
      setAIScoreStatus("idle");
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isModalVisible]);

  return (
    <CustomModal
      setIsModalVisible={setIsModalVisible}
      isModalVisible={isModalVisible}
      title="Score Info"
    >
      <div className="score-modal">
        {AIScoreStatus === "loading" && (
          <div className="score-modal-loading">
            <Loading />
            <span>Scoring...</span>
            <p>takes around 30 - 60s you can check back later</p>
          </div>
        )}

        {AIScoreStatus === "error" && (
          <>
            {message ? (
              <span>{message}</span>
            ) : (
              <span>AI Score is under maintenance.</span>
            )}
          </>
        )}

        {score.score !== null && score.score !== undefined && (
          <>
            <div className="score-modal__progress">
              <h3>Score</h3>
              <Progress
                type="circle"
                percent={(score.score / score.maxScore) * 100}
                format={() => `${score.score} / ${score.maxScore}`}
                size="small"
              />
            </div>
            {componentScores && (
              <Table
                size="middle"
                className="score-modal-table"
                columns={generateCol(componentScores)}
                dataSource={generateColData(componentScores)}
                bordered
                pagination={false}
                footer={() => (
                  <span>
                    Max Score: {score.maxScore}, Your Score:{" "}
                    <b style={{ color: "#ff6666" }}>{score.score}</b>
                  </span>
                )}
              />
            )}
          </>
        )}

        {/* {isAI && <h3>AI Score is under maintenance.</h3>} */}
      </div>
      <div className="score-modal-close">
        <Button size="small" onClick={handleClose}>
          Close
        </Button>
      </div>
    </CustomModal>
  );
};

export default ScoreModal;
