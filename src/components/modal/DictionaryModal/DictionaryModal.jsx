import React, { useEffect, useState } from "react";

import axios from "axios";
import CustomModal from "../CustomModal";
import Button from "../../shared/Button/Button";
import { Row, Col } from "antd";
import "./styles.scss";

import { useParams, Link, useLocation } from "react-router-dom";

import myVocabAPI from "../../../redux/api/practice/myVocabAPI";
import { useSelector } from "react-redux";

const { getDetail, addVocab } = myVocabAPI;

const DictionaryModal = ({ setIsModalVisible, isModalVisible, text }) => {
  const userDetail = useSelector((state) => state.user.detail);
  const [data, setData] = useState();
  const [error, setError] = useState();
  const [isAdded, setIsAdded] = useState();
  const [userDef, setUserDef] = useState();

  const { id, categoryId } = useParams();
  const { pathname } = useLocation();

  useEffect(() => {
    if (text) {
      const url = `https://api.dictionaryapi.dev/api/v2/entries/en/${text}`;
      try {
        axios
          .get(url)
          .then((res) => {
            setData(res.data[0]);
            setError();
          })
          .catch((err) => {
            setError(err.response.data.message);
          });

      } catch (error) {
        // console.clear();
      }
    }
  }, [text, categoryId, id]);

  useEffect(() => {
    getDetail({ vocab: text })
      .then((res) => {
        setUserDef(res);
      })
      .catch(() => {
        setIsAdded(false);
      });
    }, [text])

  const handleAddVocab = () => {
    const content = userDef.contents[0]
    addVocab({
      categoryId: parseInt(id),
      definitionUser: "",
      exampleUser: "",
      priority: content.priority,
      userId: userDetail.id,
      vocabId: content.id

    },).then((res) => {
      setIsAdded(true);
    });
  };

  const handleVolume = (link) => {
    const audio = new Audio(link);
    audio.play();
  };

  const handleClose = () => {
    setIsModalVisible(false);
  };

  return (
    <CustomModal
      setIsModalVisible={setIsModalVisible}
      isModalVisible={isModalVisible}
    >
      <div className="dictionary-modal">
        {error ? (
          <>
            <div className="dictionary-modal__head">
              <h1>{text}</h1>
            </div>
            <div className="dictionary-modal__main">
              <p>{error}</p>
            </div>
          </>
        ) : (
          <>
            <div className="dictionary-modal__head">
              <h1>{text}</h1>
              <div className="dictionary-modal__head--phonetic">
                {data?.phonetics.map((text, index) => (
                  <div key={index}>
                    {text ? (
                      <div className="dictionary-modal__head--audio">
                        <span>{text.text}</span>
                        <i
                          className="fa-solid fa-volume-high"
                          onClick={() => handleVolume(text.audio)}
                        ></i>
                      </div>
                    ) : (
                      ""
                    )}
                  </div>
                ))}
              </div>
            </div>

            <div className="dictionary-modal__main">
              <Row gutter={32}>
                <Col md={12} sm={24}>
                  <h2 className="dictionary-modal__main--title">Definitions</h2>

                  <div className="dictionary-modal__main--content">
                    {data?.meanings.map((list, index) => (
                      <ul className="dictionary-modal__main--list" key={index}>
                        <h3>{list.partOfSpeech}</h3>
                        {list.definitions.map((def, index) => (
                          <li key={index}>{def.definition}</li>
                        ))}
                      </ul>
                    ))}
                  </div>
                </Col>

                <Col md={12} sm={24}>
                  <h2 className="dictionary-modal__main--title">Examples</h2>

                  <div className="dictionary-modal__main--content">
                    {data?.meanings.map((list, index) => (
                      <ul key={index} className="dictionary-modal__main--list">
                        {list.definitions.map((def, index) => (
                          <li key={index}>{def?.example}</li>
                        ))}
                      </ul>
                    ))}
                  </div>
                </Col>
              </Row>
              <Row gutter={32}>
                <Col md={12} sm={24}>
                  {userDef?.definition && (
                    <div className="user-def">
                      <h3>My definitions</h3>
                      <p>{userDef.definition}</p>
                    </div>
                  )}
                </Col>
                <Col md={12} sm={24}>
                  {userDef?.example && (
                    <div className="user-def">
                      <h3>My examples</h3>
                      <p>{userDef.example}</p>
                    </div>
                  )}
                </Col>
              </Row>
            </div>
          </>
        )}

        <div className="dictionary-modal__footer">
          <div
            className="dictionary-modal__close"
            onClick={() => {
              handleClose();
            }}
          >
            <Button size="small" outline={true}>
              Close
            </Button>
          </div>

          {!pathname.includes("myvocab") &&
            (isAdded ? (
              <Link to={`/myvocab/${pathname.split("/")[2]}/${id}`}>
                Added. Revise in your Vocab List
              </Link>
            ) : (
              <div className="dictionary-modal__add" onClick={handleAddVocab}>
                <Button size="small">Add to vocab list</Button>
              </div>
            ))}
        </div>
      </div>
    </CustomModal>
  );
};

export default DictionaryModal;
