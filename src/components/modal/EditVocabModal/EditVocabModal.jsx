import React, { useEffect, useContext, forwardRef, useImperativeHandle } from "react";
import { Input, Button, Form, Row, Col, Divider } from "antd";

import CustomModal from "../CustomModal";

import "./styles.scss";

import myVocabAPI from "../../../redux/api/practice/myVocabAPI";

import { useParams } from "react-router-dom";
import { myVocabContext } from "../../../page/main/MyVocab/MyVocab";
import { toast } from "react-toastify";
import { useSelector } from "react-redux";
import useModal from "../../../hooks/useModal";

const { TextArea } = Input;
const { updateVocab, deleteVocab } = myVocabAPI;

const EditVocabModal = ({ setIsModalVisible, isModalVisible, text }, ref) => {
  const {_handleToggle, open, data} = useModal()
  const [form] = Form.useForm();

  const vocab = data?.vocab || null

  const { categoryId } = useParams();

  const { page, handleGetVocabList } = useContext(myVocabContext);
  const userDetail = useSelector((state) => state.user.detail);

  useImperativeHandle(ref , () => ({
    open: handleOpen
  }))

  const handleOpen = (data)=>{
    _handleToggle(data)
  }
 
  const handleDelete = async () => {
    if (vocab.id) {
      await deleteVocab(vocab.id);
      await handleGetVocabList(page + 1);
      setIsModalVisible(false);
    }
  };

  useEffect(() => {
    if(data) {
      form.setFieldsValue({
        definition: data.definitionUser,
        example: data.exampleUser,
      });
    }
  }, [data, form]);

  const onFinish = (values) => {
    updateVocab(
      {
        definitionUser: values.definition,
        exampleUser: values.example,
        priority: vocab.priority,
        categoryId,
        vocabId: vocab.id,
        userId: userDetail.id
      },
      userDetail.id
    ).then((res) => {
      toast.success("Update definition successfully");
      handleGetVocabList(page + 1);
      setIsModalVisible(false);
    });
  };

  const handleBack = () => {
    _handleToggle()
  };

  return (
    <CustomModal
      setIsModalVisible={setIsModalVisible}
      isModalVisible={open}
    >
      <div className="edit-vocab-modal">
        <div className="edit-vocab-modal__title">{vocab?.vocab}</div>
        <Divider />
        <Form
          form={form}
          initialValues={{
            definition: vocab?.definition ?? "",
            example: vocab?.example ?? "",
          }}
          onFinish={onFinish}
          autoComplete="off"
          style={{ width: "600px" }}
        >
          <Row gutter={[20, 20]}>
            <Col span={12}>
              <Form.Item
                name="definition"
                label="Definitions"
                style={{
                  display: "flex",
                  flexDirection: "column",
                  textAlign: "left",
                }}
              >
                <TextArea
                  autoSize={{ minRows: 4 }}
                  placeholder={vocab?.definition}
                />
              </Form.Item>
            </Col>

            <Col span={12}>
              <Form.Item
                name="example"
                label="Examples"
                style={{
                  display: "flex",
                  flexDirection: "column",
                  textAlign: "left",
                }}
              >
                <TextArea
                  autoSize={{ minRows: 4 }}
                  placeholder={vocab?.example}
                />
              </Form.Item>
            </Col>
          </Row>

          <div className="edit-vocab-modal__btns">
            <Button
              type="primary"
              htmlType="button"
              className="back-btn"
              onClick={handleBack}
            >
              Back
            </Button>
            <div className="action-btn">
              <Button type="primary" htmlType="submit">
                Save
              </Button>
              <Button type="primary" danger onClick={handleDelete}>
                Delete
              </Button>
            </div>
          </div>
        </Form>
      </div>
      
    </CustomModal>
  );
};

export default forwardRef(EditVocabModal);
