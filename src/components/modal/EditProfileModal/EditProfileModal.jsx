import React, { useState } from "react";

import "./styles.scss";

import AVATAR from "../../../asset/img/avatar.png";

import CustomInput from "../../shared/CustomInput/CustomInput";
import Button from "../../shared/Button/Button";
import CustomModal from "../CustomModal";

import { useCookies } from "react-cookie";

import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { toast } from "react-toastify";

import { useSelector, useDispatch } from "react-redux";

import { getUserIn4Thunk } from "../../../redux/slices/user/userSlice";

import globalAPI from "../../../redux/api/account/globalAPI";
import accountAPI from "../../../redux/api/account/accountAPI";

const { cdnSaveFile } = globalAPI;
const { updateUserInfo } = accountAPI;

const schema = yup.object({
  username: yup.string(),
  nickName: yup.string(),
  firstName: yup.string(),
  lastName: yup.string(),
  emailAddress: yup.string(),
  phoneNumber1: yup.string(),
});

const EditProfileModal = ({ setIsModalVisible, isModalVisible }) => {
  const [cookies] = useCookies(["authKey"]);
  const [avatarPreview, setAvatarPreview] = useState();
  const [avatarFile, setAvatarFile] = useState();

  const dispatch = useDispatch();

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    resolver: yupResolver(schema),
  });

  const userDetail = useSelector((state) => state.user.detail);

  const onSubmit = async (data) => {
    const body = {};

    if (avatarFile) {
      await cdnSaveFile(avatarFile).then((res) => {
        body["avatar"] = res.data.item.cdnFile.url;
      });
    }

    Object.keys(data).forEach((key) => {
      body[key] = data[key];
    });
    await updateUserInfo({
      username: userDetail.username,
      authKey: cookies.authKey,
      body: body,
    }).then((res) => {
      reset();
      handleClose();
      toast.success("User update successfully");
      dispatch(
        getUserIn4Thunk({
          authKey: cookies.authKey,
          username: userDetail.username,
        })
      );
    });
  };

  const handleClose = () => {
    setIsModalVisible(false);
  };

  const handleAvatar = (e) => {
    if (!e.target.files || e.target.files.length === 0) {
      setAvatarFile();
      setAvatarPreview();
    } else {
      setAvatarFile(e.target.files[0]);
      setAvatarPreview(URL.createObjectURL(e.target.files[0]));
    }
  };

  return (
    <CustomModal
      setIsModalVisible={setIsModalVisible}
      isModalVisible={isModalVisible}
      title="Edit Profile"
    >
      <div className="edit-profile">
        <div className="edit-profile__header">
          <label htmlFor="edit-profile-avatar" className="edit-profile__avatar">
            {avatarPreview ? (
              <img alt="avatar" src={avatarPreview} />
            ) : (
              <img
                src={userDetail.avatar ? userDetail.avatar : AVATAR}
                alt="avatar"
              />
            )}
            <div className="edit-profile__avatar--icon">
              <i className="fa-solid fa-camera"></i>
            </div>
            <div className="overlay"></div>
          </label>
          <input
            type="file"
            name=""
            id="edit-profile-avatar"
            style={{ display: "none" }}
            onChange={handleAvatar}
          />
        </div>

        <div className="edit-profile__main">
          <form
            className="edit-profile__form"
            autoComplete="off"
            onSubmit={(e) => e.preventDefault()}
          >
            <div className="edit-profile__input">
              <CustomInput
                title="First Name"
                name="firstName"
                type="text"
                register={register}
                errors={errors.firstName?.message}
                defaultValue={userDetail.firstName}
              />
            </div>
            <div className="edit-profile__input">
              <CustomInput
                title="Last Name"
                name="lastName"
                type="text"
                register={register}
                errors={errors.lastName?.message}
                defaultValue={userDetail.lastName}
              />
            </div>
            <div className="edit-profile__input">
              <CustomInput
                title="Email"
                name="emailAddress"
                type="email"
                register={register}
                errors={errors.emailAddress?.message}
                defaultValue={userDetail.emailAddress}
              />
            </div>
            <div className="edit-profile__input">
              <CustomInput
                title="Phone"
                name="phoneNumber1"
                type="phone"
                register={register}
                errors={errors.phoneNumber1?.message}
                defaultValue={userDetail.phoneNumber1}
              />
            </div>
            <div className="edit-profile__input">
              <CustomInput
                title="Nickname"
                name="nickName"
                type="text"
                register={register}
                errors={errors.nickName?.message}
                defaultValue={userDetail.nickName}
              />
            </div>
            <div className="edit-profile__input">
              <CustomInput
                title="Website"
                name="website"
                type="text"
                register={register}
                errors={errors.website?.message}
                defaultValue={userDetail.website}
              />
            </div>
            {/* <div className="edit-profile__input">
              <CustomInput
                title="Theme Style"
                name="themeStyle"
                type="phone"
                register={register}
                errors={errors.themeStyle?.message}
                defaultValue={userDetail.themeStyle}
              />
            </div>
            <div className="edit-profile__input">
              <CustomInput
                title="Passphrase"
                name="passphrase"
                type="text"
                register={register}
                errors={errors.passphrase?.message}
                defaultValue={userDetail.passphrase}
              />
            </div> */}
          </form>
          <div className="edit-profile__form--btns">
            <div
              className="edit-profile__form--submit"
              onClick={handleSubmit(onSubmit)}
            >
              <Button size="small">Save</Button>
            </div>

            <button className="edit-profile__form--close" onClick={handleClose}>
              Cancel
            </button>
          </div>
        </div>
      </div>
    </CustomModal>
  );
};

export default EditProfileModal;
