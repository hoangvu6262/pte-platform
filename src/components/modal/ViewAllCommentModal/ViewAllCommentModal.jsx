import React from "react";
import CustomModal from "../CustomModal";

const ViewAllCommentModal = ({
  isModalVisible,
  setIsModalVisible,
  children,
}) => {
  return (
    <CustomModal
      setIsModalVisible={setIsModalVisible}
      isModalVisible={isModalVisible}
      title="All Comments"
    >
      <div style={{ width: 800 }}>{children}</div>
    </CustomModal>
  );
};

export default ViewAllCommentModal;
