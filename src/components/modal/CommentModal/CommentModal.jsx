import React from "react";
import { Input, Button, Form } from "antd";
import { toast } from "react-toastify";
import { useDispatch } from "react-redux";

import { useParams } from "react-router-dom";

import CustomModal from "../CustomModal";
import discussionAPI from "../../../redux/api/practice/discussionAPI";
import { getListDiscussionTabThunk } from "../../../redux/slices/discuss/discussSlice";

import "./styles.scss";

const { TextArea } = Input;
const { addDiscussion } = discussionAPI;

const CommentModal = ({ setIsModalVisible, isModalVisible }) => {
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const { code } = useParams();

  const handleClose = () => {
    setIsModalVisible(false);
  };

  const onFinish = (values) => {
    const commentData = {
      discussionContent: values.comment,
      lesson: { id: code },
      parentId: null,
    };

    addDiscussion(commentData)
      .then((res) => {
        form.resetFields();
        handleClose();
        if (code !== "*") {
          dispatch(getListDiscussionTabThunk({ lessonId: code, page: 0 }));
        }
      })
      .catch((err) => {
        toast.error("Add comment failed");
      });
  };

  return (
    <CustomModal
      setIsModalVisible={setIsModalVisible}
      isModalVisible={isModalVisible}
      title="Comment"
    >
      <Form
        form={form}
        initialValues={{
          comment: "",
        }}
        onFinish={onFinish}
        autoComplete="off"
        style={{ width: "600px" }}
      >
        <Form.Item
          name="comment"
          rules={[
            {
              required: true,
              message: "Please input your comment!",
            },
          ]}
        >
          <TextArea autoSize={{ minRows: 4 }} />
        </Form.Item>
        <div className="comment-button">
          <Button type="primary" htmlType="submit">
            Send
          </Button>
        </div>
      </Form>
    </CustomModal>
  );
};

export default CommentModal;
