import React, { useState, useEffect } from "react";
import { Empty } from "antd";
import CustomModal from "../CustomModal";
import CustomAudio from "../../practice/CustomAudio/CustomAudio";

import "./styles.scss";

import { useSelector } from "react-redux";

const ShadowingModal = ({ isModalVisible, setIsModalVisible }) => {
  const [isPlaying, setIsPlaying] = useState(true);

  const { content } = useSelector((state) => state.lesson.lessonDetail);

  const renderShadowing = () => {
    const shadowingMedia = content.lessonMediaDTOs?.filter(
      (media) => media.type === "shadowing"
    );

    if (shadowingMedia && shadowingMedia.length > 0) {
      return (
        <>
          <div className="shadowing-modal__audio">
            <CustomAudio
              audioList={content.lessonMediaDTOs?.filter(
                (media) => media.type === "shadowing"
              )}
              isPlaying={isPlaying}
              setIsPlaying={setIsPlaying}
            />
          </div>

          <div className="shadowing-modal__main">
            <p
              dangerouslySetInnerHTML={{
                __html: content.explanation,
              }}
            ></p>
          </div>
          <div className="shadowing-modal__tags">
            <div className="shadowing-modal__tag shadowing-modal__tag--pause">
              Pause
            </div>
            <div className="shadowing-modal__tag shadowing-modal__tag--loss">
              Loss
            </div>
            <div className="shadowing-modal__tag shadowing-modal__tag--linking">
              Linking
            </div>
            <div className="shadowing-modal__tag shadowing-modal__tag--weak">
              Weak
            </div>
          </div>
        </>
      );
    } else {
      return <Empty />;
    }
  };

  useEffect(() => {
    if (isModalVisible) {
      setIsPlaying(true);
    } else {
      setIsPlaying(false);
    }

    return () => {
      setIsPlaying(false);
    };
  }, [isModalVisible]);
  return (
    <CustomModal
      setIsModalVisible={setIsModalVisible}
      isModalVisible={isModalVisible}
      title="Shadowing"
    >
      <div className="shadowing-modal">{renderShadowing()}</div>
    </CustomModal>
  );
};

export default ShadowingModal;
