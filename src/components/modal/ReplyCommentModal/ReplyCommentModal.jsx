import React from "react";
import { Input, Button, Form } from "antd";
import { toast } from "react-toastify";

import "./styles.scss";
import CustomModal from "../CustomModal";
import { useDispatch, useSelector } from "react-redux";
import {
  getListDiscussionTabThunk,
  getListBoardTabThunk,
  getListMeTabThunk,
} from "../../../redux/slices/discuss/discussSlice";
import discussionAPI from "../../../redux/api/practice/discussionAPI";

const { TextArea } = Input;
const { addDiscussion } = discussionAPI;

const ReplyCommentModal = ({
  setIsModalVisible,
  isModalVisible,
  parentId,
  parentName,
  parentUser,
}) => {
  const [form] = Form.useForm();

  const tabKey = useSelector((state) => state.discuss.tabKey);
  const userAuthId = useSelector((state) => state.user.detail.id);
  const lessonDetail = useSelector((state) => state.lesson.lessonDetail.content)



  const dispatch = useDispatch();

  const handleClose = () => {
    setIsModalVisible(false);
  };

  const handleReplyComment = (values) => {
    // e.preventDefault();
    const commentData = {
      // userId: userAuthId,
      discussionContent: `${parentUser}@@` + values.comment,
      lesson: { id: lessonDetail.id },
      parentId: parentId,
    };

    addDiscussion(commentData)
      .then((res) => {
        form.resetFields();
        handleClose();
        switch (tabKey) {
          case 1:
            dispatch(getListDiscussionTabThunk({ lessonId: lessonDetail.id, page: 0 }));
            break;
          case 2:
            dispatch(getListBoardTabThunk({ lessonId: lessonDetail.id, pageNumber: 0, userId: userAuthId }));
            break;
          case 3:
            dispatch(
              getListMeTabThunk({ lessonId: lessonDetail.id, pageNumber: 0, userId: userAuthId })
            );
            break;
          default:
            break;
        }
      })
      .catch((err) => {
        toast.error("Add comment failed");
      });
    form.resetFields();
  };

  return (
    <CustomModal
      setIsModalVisible={setIsModalVisible}
      isModalVisible={isModalVisible}
      title="Reply Comment"
    >
      <Form
        form={form}
        initialValues={{
          comment: "",
        }}
        onFinish={handleReplyComment}
        autoComplete="off"
        style={{ width: "600px" }}
      >
        <Form.Item
          name="comment"
          rules={[
            {
              required: true,
              message: "Please input your comment!",
            },
          ]}
        >
          <TextArea placeholder={`@${parentName}`} autoSize={{ minRows: 4 }} />
        </Form.Item>
        <div className="comment-button">
          <Button type="primary" htmlType="submit">
            Send
          </Button>
        </div>
      </Form>
    </CustomModal>
  );
};

export default ReplyCommentModal;
