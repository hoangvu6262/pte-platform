import React, { useState, useEffect, useCallback } from "react";

import CustomModal from "../CustomModal";
import TestedForm from "../../../container/TestedForm/TestedForm";
import TestedList from "../../../container/TestedList/TestedList";

import { useSelector } from "react-redux";

import "./styles.scss";

import testedAPI from "../../../redux/api/practice/testedAPI";

const { getByUserId } = testedAPI;

export const TestedModalContext = React.createContext();

const TestedModal = ({
  setIsModalVisible,
  isModalVisible,
  lessonId,
  count,
}) => {
  const [openForm, setOpenForm] = useState(false);
  const [testedList, setTestedList] = useState([]);

  const userAuthId = useSelector((state) => state.user.detail.id);

  const getTestedList = useCallback(() => {
    getByUserId(lessonId, userAuthId)
      .then((res) => {
        setOpenForm(false);
        setTestedList(res);
      })
      .catch((err) => {
        setTestedList([]);
        setOpenForm(true);
      });
  }, [lessonId, userAuthId]);

  const deleteFromList = (id) => {
    setTestedList(testedList.filter((test) => test.id !== id));
  };

  useEffect(() => {
    getTestedList();
  }, [getTestedList, isModalVisible]);

  useEffect(() => {
    if (testedList.length === 0) {
      setOpenForm(true);
    }
  }, [testedList]);

  return (
    <CustomModal
      setIsModalVisible={setIsModalVisible}
      isModalVisible={isModalVisible}
    >
      <TestedModalContext.Provider value={{ deleteFromList }}>
        <div className="tested-modal">
          {count > 0 && !openForm ? (
            <TestedList
              testedList={testedList}
              count={count}
              openForm={setOpenForm}
            />
          ) : (
            <TestedForm
              lessonId={lessonId}
              setIsModalVisible={setIsModalVisible}
            />
          )}
        </div>
      </TestedModalContext.Provider>
    </CustomModal>
  );
};

export default TestedModal;
