import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import "./styles.scss";
import logo from "../../../asset/img/logo.png";
import accountAPI from "../../../redux/api/account/accountAPI";
import { useCookies } from "react-cookie";

const { getBrandInfo } = accountAPI;

const Logo = (props) => {
  const [headerLogo, setHeaderLogo] = useState(logo);
  const [cookies] = useCookies(["authKey"]);

  useEffect(() => {
    if (cookies.authKey) {
      getBrandInfo(cookies.authKey).then((brand) => {
        setHeaderLogo(brand.data?.item?.logoImagePath || logo);
      });
    }
  }, [cookies.authKey]);

  return (
    <Link to="/" className={`logo ${props.color ? props.color : ""}`}>
      <img src={headerLogo} alt="" />
    </Link>
  );
};

export default Logo;
