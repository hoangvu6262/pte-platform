import React, { useRef, useEffect, useState } from "react";
import "./styles.scss";
import { Popover } from "antd";
import { CaretDownOutlined, BarsOutlined } from "@ant-design/icons";

import Logo from "../../shared/Logo/Logo";
import MegaMenu from "../MegaMenu/MegaMenu";
import Sidebar from "../MegaMenu/Sidebar/Sidebar";
import Button from "../Button/Button";
import AvatarPopover from "../AvatarPopover/AvatarPopover";
import { Link } from "react-router-dom";

import { useSelector, useDispatch } from "react-redux";
import userSlice from "../../../redux/slices/user/userSlice";

import { useCookies } from "react-cookie";

const Header = () => {
  const [openSidebar, setOpenSidebar] = useState(false);
  const [isAuth, setIsAuth] = useState(false);
  const headerRef = useRef(null);

  const [, , removeCookie] = useCookies(["authKey", "username", "token"]);
  const { detail } = useSelector((state) => state.user);
  const dispatch = useDispatch();

  const handleSignOut = () => {
    removeCookie("authKey", { path: "/" });
    removeCookie("username", { path: "/" });
    removeCookie("token", { path: "/" });
    dispatch(userSlice.actions.resetUser());
  };

  useEffect(() => {
    const shrinkHeader = () => {
      if (
        document.body.scrollTop > 0 ||
        document.documentElement.scrollTop > 0
      ) {
        headerRef.current.classList.add("shrink");
      } else {
        headerRef.current.classList.remove("shrink");
      }
    };

    window.addEventListener("scroll", shrinkHeader);
    return () => {
      window.removeEventListener("scroll", shrinkHeader);
    };
  }, []);

  useEffect(() => {
    if (detail.username) {
      setIsAuth(true);
    } else {
      setIsAuth(false);
    }
  }, [detail]);

  return (
    <>
      <header ref={headerRef}>
        <div className="header">
          <Logo />
          <div className="header__menu">
            <Link to="/" className="header__menu--link">
              Home
            </Link>
            <Popover
              placement="bottomLeft"
              content={
                <div className="header__menu--submenu">
                  <MegaMenu />
                </div>
              }
            >
              <div className="header__menu--link">
                Boost PTE Skills <CaretDownOutlined />
              </div>
            </Popover>
            <Link to="/class" className="header__menu--link">
              Class
            </Link>
            <Link to="/community" className="header__menu--link">
              Community
            </Link>
          </div>

          <div className="header__auth">
            {isAuth ? (
              <AvatarPopover handleSignOut={handleSignOut} />
            ) : (
              <>
                <Link to="/account/sign-in" className="header__auth--login">
                  Sign In
                </Link>
                <Link to="/account/sign-up" className="header__auth--register">
                  <Button size="small">Sign Up</Button>
                </Link>
              </>
            )}
          </div>
          <div
            className="header__sidebar"
            onClick={() => {
              setOpenSidebar(true);
            }}
          >
            <BarsOutlined className="header__sidebar--icon" />
          </div>
        </div>
      </header>
      <Sidebar
        open={openSidebar}
        setOpen={(open) => {
          setOpenSidebar(open);
        }}
      />
    </>
  );
};

export default Header;
