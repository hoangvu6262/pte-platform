import React from "react";

import "./styles.scss";

const Footer = () => {
  return (
    <footer>
      <div className="booostpte_copyright--blue">
        <span>
          <strong>#1</strong>{" "}
          <a href="https://boostpte.com/">
            <strong>Online PTE Practice</strong>
          </a>
          <strong>, Tests &amp; AI Scoring Platform |</strong>{" "}
          <a href="https://boostpte.com/">
            <strong>BoostPTE</strong>
          </a>
          <br />
          <strong>#1 Nền Tảng </strong>
          <a href="https://boostpte.com/vi/">
            <strong>Luyện Thi PTE Online</strong>
          </a>
          <strong> Bằng AI |</strong>{" "}
          <a href="https://boostpte.com/vi/">
            <strong>BoostPTE Việt Nam</strong>
          </a>
          <br />
          Copyright @ 2021. All rights reserved | Version: 1.12.2.1
        </span>
      </div>
    </footer>
  );
};

export default Footer;
