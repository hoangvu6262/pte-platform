import React from "react";

import "./styles.scss";

const Loading = () => {
  return (
    <div className="loading">
      <div className="spinner-box">
        <div className="three-quarter-spinner"></div>
      </div>
    </div>
  );
};

export default Loading;
