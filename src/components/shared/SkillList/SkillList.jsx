import React from "react";
import { Row, Col, Empty } from "antd";
import { RightOutlined } from "@ant-design/icons";
import "./styles.scss";

import { Link } from "react-router-dom";
import CustomTitle from "../CustomTitle/CustomTitle";
import { useSelector } from "react-redux";

const SkillList = () => {
  const listCategory = useSelector((state) => state.shared.categories);

  const renderListGuide = () => {
    return listCategory
      .filter((cate) => cate.code !== "COMP")
      .map((parent) => (
        <Col lg={12} sm={24} key={parent.code}>
          <div className="skill-list--card">
            <h2>{parent.name}</h2>
            {parent.listChildCate.map((cate, index) => (
              <Link
                className="skill-list--card-item"
                key={index + cate.code}
                to={
                  "/practice/" +
                  parent.name.toLowerCase() +
                  "/" +
                  cate.code.toLowerCase() +
                  "/" +
                  cate.id +
                  "/1"
                }
              >
                <span>
                  {cate.name}{" "}
                  {cate.tags === "AI" && (
                    <sup style={{ color: "#df1f1f" }}>(AI)</sup>
                  )}
                </span>

                <RightOutlined />
              </Link>
            ))}
          </div>
        </Col>
      ));
  };

  return (
    <section className="home__section skill-list">
      <CustomTitle name="Skills" title="PTE Skills" />
      {listCategory.length > 0 ? (
        <Row gutter={32} wrap={true} className="skill-list--wrapper">
          {renderListGuide()}
        </Row>
      ) : (
        <Empty />
      )}
    </section>
  );
};

export default SkillList;
