import React from "react";
import { Pagination } from "antd";

import "./styles.scss";

const CustomPagination = ({ totalPages, curPage, pageSize, onChange }) => {
  return (
    <Pagination
      showQuickJumper
      current={curPage - 0 + 1}
      total={totalPages}
      pageSize={pageSize}
      onChange={onChange}
    />
  );
};

export default CustomPagination;
