import React from "react";
import "./styles.scss";
import clsx from "clsx";

const Button = ({
  outline,
  size,
  disabled = false,
  children,
  onClick,
  type,
}) => {
  return (
    <button
      onClick={onClick}
      className={clsx(
        "button",
        outline && "outline",
        size,
        disabled && "disabled"
      )}
      disabled={disabled}
    >
      {children}
    </button>
  );
};

export default Button;
