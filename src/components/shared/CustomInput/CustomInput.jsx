import React, { useState } from "react";

import "./styles.scss";

const CustomInput = ({
  title,
  name,
  register,
  errors,
  type,
  errorMessage,
  defaultValue,
  submit,
}) => {
  const [eye, setEye] = useState(false);

  const handleShow = () => {
    setEye(!eye);
  };

  return (
    <div className="cus-input">
      <h6 className="cus-input__title">{title}</h6>

      <div className="cus-input__wrap">
        <input
          className={`cus-input__input ${
            errors ? "cus-input__input--error" : ""
          }`}
          type={type === "password" ? (eye ? "text" : "password") : type}
          autoComplete="off"
          defaultValue={defaultValue}
          onKeyDown={(e) => {
            if (e.key === "Enter" && submit) {
              submit();
            }
          }}
          {...register(name)}
        />

        {type === "password" && (
          <div className="cus-input__input--eye" onClick={handleShow}>
            {eye ? (
              <i className="fa-solid fa-eye-slash"></i>
            ) : (
              <i className="fa-solid fa-eye"></i>
            )}
          </div>
        )}
      </div>
      {errors && <p className="cus-input__error">{errors}</p>}
      {errorMessage && <p className="cus-input__error">{errorMessage}</p>}
    </div>
  );
};

export default CustomInput;
