import React from "react";
import { Tabs } from "antd";
import { useDispatch, useSelector } from "react-redux";

import { changeTabKeyThunk } from "../../../redux/slices/discuss/discussSlice";

const CustomTab = ({ children }) => {
  const dispatch = useDispatch();

  const tabKey = useSelector((state) => state.discuss.tabKey);

  const onChange = (key) => {
    dispatch(changeTabKeyThunk(key));
  };
  return (
    <div>
      <Tabs onChange={onChange} activeKey={JSON.stringify(tabKey)} type="card">
        {children}
      </Tabs>
    </div>
  );
};

export default CustomTab;
