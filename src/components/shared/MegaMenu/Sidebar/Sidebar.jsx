import React from "react";
import { Menu, Drawer } from "antd";
import "./styles.scss";

import { Link } from "react-router-dom";

import { menuHelper } from "../../../../helpers/menuHelper";

import { useSelector } from "react-redux";
const { getItem, generateMenuItem } = menuHelper;

const Sidebar = ({ open, setOpen }) => {
  const listCategory = useSelector((state) => state.shared.categories);

  const onClose = () => {
    setOpen(false);
  };

  const renderMenu = () => {
    const menuList = [
      getItem(<Link to="/">Home</Link>, "home"),
      getItem(<Link to="/class">Class</Link>, "class"),
      getItem(<Link to="/community">Community</Link>, "community"),
    ];
    listCategory.forEach((menu, index) => {
      menuList.push(
        getItem(
          menu.code !== "COMP" ? menu.name : "MORE",
          menu.name,
          null,
          generateMenuItem(menu, menu.listChildCate)
        )
      );
    });

    return menuList;
  };

  return (
    <Drawer
      visible={open}
      onClose={onClose}
      closeIcon={false}
      width="fit-content"
    >
      <div className="sidebar">
        <div
          className="sidebar-close"
          onClick={() => {
            onClose();
          }}
        >
          <i className="fa-solid fa-xmark"></i>
        </div>
        <Menu
          style={{
            width: 256,
          }}
          defaultSelectedKeys={["1"]}
          defaultOpenKeys={["sub1"]}
          mode="inline"
          items={renderMenu()}
        />
      </div>
    </Drawer>
  );
};

export default Sidebar;
