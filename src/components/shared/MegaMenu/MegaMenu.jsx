import React from "react";
import { Menu, Row, Col } from "antd";

import { Link } from "react-router-dom";

import "./styles.scss";

import { useSelector } from "react-redux";
import { menuHelper } from "../../../helpers/menuHelper";

const { getItem } = menuHelper;

const MegaMenu = () => {
  const listCategory = useSelector((state) => state.shared.categories);

  const generateMenuItem = (group, list) => {
    return list?.map((item) =>
      getItem(
        <Link
          to={
            group.code !== "COMP"
              ? "/practice/" +
                group.name.toLowerCase() +
                "/" +
                item.code.toLowerCase() +
                "/" +
                item.id +
                "/1"
              : item.code !== "VOCABULARY"
              ? `/${item.code.toLowerCase()}`
              : "/myvocab/speaking/3"
          }
          className="mega-menu__link--item"
        >
          <span>
            {item.name}{" "}
            {item.tags === "AI" && <sup style={{ color: "#df1f1f" }}>(AI)</sup>}
          </span>
        </Link>,
        item.id + item.code
      )
    );
  };

  const renderMenu = () => {
    return listCategory?.map((group, index) => (
      <Col key={index}>
        <p className="mega-menu__item-title">
          {group.code !== "COMP" ? group.name : "MORE"}
        </p>
        <Menu
          selectable={false}
          items={generateMenuItem(group, group.listChildCate)}
        />
      </Col>
    ));
  };

  return (
    <div className="mega-menu">
      <div className="mega-menu__container">
        <Row>{renderMenu()}</Row>
      </div>
    </div>
  );
};

export default MegaMenu;
