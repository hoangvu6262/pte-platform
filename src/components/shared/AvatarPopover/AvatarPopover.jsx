import React from "react";

import "./styles.scss";
import { Popover } from "antd";

import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

import AVATAR from "../../../asset/img/avatar.png";

const PopoverContent = ({ detail, handleSignOut }) => {
  return (
    <div className="avatar-popover-content">
      <Link className="avatar-popover-content__header" to="/user/profile">
        <div className="avatar-popover-content__header--img">
          <img src={detail.avatar ? detail.avatar : AVATAR} alt="" />
        </div>
        <div className="avatar-popover-content__header--in4">
          <h6>{detail.username}</h6>
          <h6>{detail.emailAddress}</h6>
        </div>
      </Link>

      <div className="avatar-popover-content__main">
        <div className="avatar-popover-content__link avatar-popover-content__link--payment">
          <Link to="/payment">
            <i className="fa-solid fa-crown"></i>
            <span>Upgraded to Premium</span>
          </Link>
        </div>
        <div className="avatar-popover-content__link">
          <Link to="/user/profile">
            <i className="fa-solid fa-gear"></i>
            <span>Setting</span>
          </Link>
        </div>
      </div>

      <div className="avatar-popover-content__sign-out" onClick={handleSignOut}>
        <i className="fa-solid fa-arrow-right-from-bracket"></i>
        <span>Sign out</span>
      </div>
    </div>
  );
};

const AvatarPopover = ({ handleSignOut }) => {
  const { detail } = useSelector((state) => state.user);

  return (
    <div className="avatar-popover">
      <Popover
        placement="bottomRight"
        content={
          <PopoverContent detail={detail} handleSignOut={handleSignOut} />
        }
        className="avatar-popover__popover"
        trigger="click"
      >
        <div className="avatar-popover__avatar">
          <img src={detail.avatar ? detail.avatar : AVATAR} alt="" />
        </div>
      </Popover>
    </div>
  );
};

export default AvatarPopover;
