import React from "react";
import { Empty } from "antd";
import { useSelector, useDispatch } from "react-redux";

import AudioComment from "../AnswerComment/AnswerComment";
import CustomPagination from "../../shared/Pagination/CustomPagination";

import { getListBoardTabThunk } from "../../../redux/slices/discuss/discussSlice";

const BoardTab = ({ listBoard }) => {
  const { boardPagination } = useSelector((state) => state.discuss);
  const userAuthId = useSelector((state) => state.user.detail.id);
  const lessonDetail = useSelector((state) => state.lesson.lessonDetail.content)

  const dispatch = useDispatch();

  // render audio comment
  const renderAudioComment = () => {
    if (listBoard?.length > 0) {
      return listBoard.map((board, index) => {
        return <AudioComment {...board} isBoard={true} key={index} />;
      });
    } else {
      return <Empty />;
    }
  };

  const onPageChange = (page) => {
    dispatch(getListBoardTabThunk({ lessonId: lessonDetail.id, pageNumber: page, userId: userAuthId }));
  };

  return (
    <div className="board">
      {renderAudioComment()}
      <div className="discussion__pagination">
        <CustomPagination
          totalPages={boardPagination.totalItems}
          curPage={boardPagination.numberOfCurrentPage}
          pageSize={boardPagination.sizeCurrentItems}
          onChange={onPageChange}
        />
      </div>
    </div>
  );
};

export default BoardTab;
