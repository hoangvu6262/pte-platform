import React, { useState, useEffect, useCallback } from "react";
import { Button, Popconfirm, Tooltip, Popover } from "antd";
import { MessageOutlined, LikeOutlined, LikeFilled } from "@ant-design/icons";
import "./styles.scss";

import discussionAPI from "../../../redux/api/practice/discussionAPI";

const { addDiscussionReaction, deleteDiscussionReaction } = discussionAPI;

const ActionComment = ({
  showModal,
  handleDeleteComment,
  reactionList,
  userId,
  discussionId,
  canDelete,
}) => {
  const [isReaction, setIsReaction] = useState(false);
  const [count, setCount] = useState(reactionList.length);

  const checkReaction = useCallback(() => {
    const reactionFilter = reactionList.filter(
      (react) => react.createdBy === userId
    );

    if (reactionFilter.length > 0) {
      setIsReaction(true);
    } else {
      setIsReaction(false);
    }
  }, [reactionList, userId]);

  const handleCheck = async () => {
    if (isReaction) {
      await deleteDiscussionReaction(discussionId, userId);

      setCount(count - 1);
      setIsReaction(false);
    } else {
      await addDiscussionReaction({
        userId: userId,
        reactions: 1,
        discussion: { id: discussionId },
      });
      setCount(count + 1);
      setIsReaction(true);
    }
  };

  useEffect(() => {
    checkReaction();
  }, [userId, checkReaction]);

  return (
    <div className="action-comment">
      <div className="action-comment__button">
        {canDelete && (
          <Tooltip placement="bottom" title="Delete" color="red">
            <Popconfirm
              title="Are you sure？"
              okText="Yes"
              cancelText="No"
              onConfirm={handleDeleteComment}
            >
              <Button
                shape="circle"
                className="action-comment__button-report"
                icon={
                  <i
                    className="fa-regular fa-trash-can"
                    style={{ color: "#ffa0a0", fontSize: "18px" }}
                  ></i>
                }
              />
            </Popconfirm>
          </Tooltip>
        )}

        <Button
          shape="circle"
          className="action-comment__button-report"
          icon={<MessageOutlined style={{ color: "#79d9c7" }} />}
          onClick={showModal}
        />

        <Popover>
          {count > 0 ? (
            <Button
              shape="circle"
              className="action-comment__button-reactions"
              onClick={handleCheck}
            >
              <p className="action-comment__button-reactions-amount">{count}</p>

              {isReaction ? (
                <LikeFilled style={{ color: "#84cdf0" }} />
              ) : (
                <LikeOutlined />
              )}
            </Button>
          ) : (
            <Button
              shape="circle"
              className="action-comment__button-reactions"
              onClick={handleCheck}
            >
              <LikeOutlined />
            </Button>
          )}
        </Popover>
      </div>
    </div>
  );
};

export default ActionComment;
