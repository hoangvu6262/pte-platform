import React, { useState, useEffect } from "react";
import { Avatar, Comment, Skeleton } from "antd";

import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import "./styles.scss";
import ActionComment from "../ActionComment/ActionComment";
import SubComment from "../SubComment/SubComment";
import ReplyCommentModal from "../../modal/ReplyCommentModal/ReplyCommentModal";

import discussionAPI from "../../../redux/api/practice/discussionAPI";
import accountAPI from "../../../redux/api/account/accountAPI";
import {
  getListDiscussionTabThunk,
  getListMeTabThunk,
  getListBoardTabThunk,
} from "../../../redux/slices/discuss/discussSlice";

import { useCookies } from "react-cookie";

import avatarImg from "../../../asset/img/avatar.png";

import moment from "moment";

import { renderUsername } from "../../../utils/user";

const { getUserInfoById } = accountAPI;
const { deleteDiscussion } = discussionAPI;

const CustomComment = ({
  id,
  discussionContent,
  createdDate,
  createdBy,
  discussionReactions,
  discussionSubs,
  parentId,
}) => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [userIn4, setUserIn4] = useState();
  const [status, setStatus] = useState("idle");
  const [replyName, setReplyName] = useState();
  const dispatch = useDispatch();
  const { code } = useParams();
  const [cookies] = useCookies(["authKey"]);

  const userAuthId = useSelector((state) => state.user.detail.id);
  const lessonDetail = useSelector((state) => state.lesson.lessonDetail.content)

  const { tabKey, discussPagination, boardPagination, mePagination } =
    useSelector((state) => state.discuss);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleDeleteComment = async () => {
    if (createdBy === userAuthId) {
      await deleteDiscussion(id, createdBy);
      switch (tabKey) {
        case 1:
          dispatch(
            getListDiscussionTabThunk({
              lessonId: lessonDetail.id,
              page: discussPagination.numberOfCurrentPage,
            })
          );
          break;
        case 2:
          dispatch(
            getListBoardTabThunk({
              lessonId: lessonDetail.id,
              pageNumber: boardPagination.numberOfCurrentPage,
              userId: userAuthId
            })
          );
          break;
        case 3:
          dispatch(
            getListMeTabThunk({
              lessonId: code,
              pageNumber: mePagination.numberOfCurrentPage,
              userId: userAuthId
            })
          );
          break;
        default:
          break;
      }
    }
  };

  const handleDiscussionContent = (content) => {
    if (content && content.includes("@@")) {
      const splittedContent = content.split("@@");

      return (
        <>
          <span className="custom-comment__content--rep">
            {replyName && "@" + replyName + " "}
          </span>
          <span>{splittedContent[1]}</span>
        </>
      );
    } else {
      return <span>{content}</span>;
    }
  };

  useEffect(() => {
    if (createdBy) {
      setStatus("loading");
      getUserInfoById({
        id: createdBy,
        authKey: cookies.authKey,
      })
        .then((res) => {
          setUserIn4(res.data.item);
          setStatus("idle");
        })
        .catch((err) => {
          setStatus("idle");
          setUserIn4();
        });
    }
    if (discussionContent && discussionContent.includes("@@")) {
      const splittedContent = discussionContent.split("@@");
      getUserInfoById({
        id: splittedContent[0],
        authKey: cookies.authKey,
      }).then((res) => {
        setReplyName(renderUsername(res.data.item));
      });
    }
  }, [createdBy, cookies.authKey, discussionContent]);

  return (
    <>
      {status === "loading" ? (
        <Skeleton
          avatar
          paragraph={{
            rows: 2,
          }}
        />
      ) : (
        <Comment
          className="custom-comment"
          actions={[
            <span
              key="comment-nested-reply-to"
              className="custom-comment__date"
            >
              {moment(createdDate).format("DD/MM/YYYY")}
            </span>,
            <ActionComment
              showModal={showModal}
              handleDeleteComment={handleDeleteComment}
              reactionList={discussionReactions}
              userId={userAuthId}
              discussionId={id}
              canDelete={userAuthId === createdBy}
            />,
          ]}
          author={
            <h4 className="custom-comment__author">
              {userIn4?.username
                ? renderUsername(userIn4)
                : "User " + createdBy}
            </h4>
          }
          avatar={
            <Avatar
              className="custom-comment__avatar"
              src={userIn4?.avatar || avatarImg}
              alt={"User " + createdBy}
            />
          }
          content={
            <p className="custom-comment__content">
              {handleDiscussionContent(discussionContent)}
            </p>
          }
        >
          {discussionSubs?.length > 0 && (
            <SubComment parentId={id} subCommentList={discussionSubs} />
          )}
        </Comment>
      )}
      <ReplyCommentModal
        parentId={parentId ? parentId : id}
        parentName={
          userIn4?.username ? renderUsername(userIn4) : "User " + createdBy
        }
        parentUser={createdBy}
        where="discussion"
        isModalVisible={isModalVisible}
        setIsModalVisible={setIsModalVisible}
      />
    </>
  );
};

export default CustomComment;
