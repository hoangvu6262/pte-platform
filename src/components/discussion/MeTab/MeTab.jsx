import React from "react";
import { Empty } from "antd";
import { useSelector, useDispatch } from "react-redux";

import AudioComment from "../../../components/discussion/AnswerComment/AnswerComment";
import CustomPagination from "../../../components/shared/Pagination/CustomPagination";

import { getListMeTabThunk } from "../../../redux/slices/discuss/discussSlice";
import { useParams } from "react-router-dom";

const MeTab = ({ listAnswer }) => {
  const { mePagination } = useSelector((state) => state.discuss);

  const { code } = useParams();
  const dispatch = useDispatch();

  const onPageChange = (page) => {
    dispatch(
      getListMeTabThunk({
        lessonId: code,
        page: page - 1,
      })
    );
  };

  // render audio comment
  const renderAudioComment = () => {
    if (listAnswer?.length > 0) {
      return listAnswer.map((answer, index) => {
        return <AudioComment {...answer} key={index} />;
      });
    } else {
      return <Empty />;
    }
  };
  return (
    <div>
      {renderAudioComment()}
      <div className="discussion__pagination">
        <CustomPagination
          totalPages={mePagination.totalItems}
          curPage={mePagination.numberOfCurrentPage}
          pageSize={mePagination.sizeCurrentItems}
          onChange={onPageChange}
        />
      </div>
    </div>
  );
};

export default MeTab;
