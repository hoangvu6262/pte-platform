import React, { useState } from "react";

import "./styles.scss";

import CustomComment from "../Comment/Comment";

const SubComment = ({ subCommentList, parentId }) => {
  const [show, setShow] = useState(false);

  const handleViewAll = () => {
    setShow(true);
  };

  const renderSubComment = (subCommentList) => {
    return subCommentList.map((comment) => {
      return (
        <CustomComment {...comment} parentId={parentId} key={comment.id} />
      );
    });
  };

  return (
    <>
      {renderSubComment(subCommentList.slice(0, 3))}

      {show && renderSubComment(subCommentList.slice(3, subCommentList.length))}

      {!show && subCommentList.length > 3 && (
        <button className="view-all-btn" onClick={handleViewAll}>
          <span>View All</span>
          <i className="fa-solid fa-chevron-down"></i>
        </button>
      )}
    </>
  );
};

export default SubComment;
