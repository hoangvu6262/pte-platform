import React, { useState } from "react";
import { Button, Empty } from "antd";
import { MessageOutlined } from "@ant-design/icons";

import { useSelector, useDispatch } from "react-redux";

import CustomComment from "../../../components/discussion/Comment/Comment";
import CommentModal from "../../../components/modal/CommentModal/CommentModal";
import CustomPagination from "../../../components/shared/Pagination/CustomPagination";

import { getListDiscussionTabThunk } from "../../../redux/slices/discuss/discussSlice";

const DiscussionTab = ({ listDiscussion }) => {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const { discussPagination } = useSelector((state) => state.discuss);
  const lessonDetail = useSelector((state) => state.lesson.lessonDetail.content)

  const dispatch = useDispatch();

  const onPageChange = (page) => {
    dispatch(getListDiscussionTabThunk({ lessonId: lessonDetail.id, page: page - 1 }));
  };

  //open Modal comment
  const showModal = () => {
    setIsModalVisible(true);
  };

  // render comment list
  const renderCommentList = () => {
    return listDiscussion.map((comment) => {
      return <CustomComment {...comment} key={comment.id} />;
    });
  };

  return (
    <>
      <div className="discussion__comment">
        <div className="discussion__comment-add">
          <Button
            type="primary"
            shape="round"
            icon={<MessageOutlined />}
            size="medium"
            onClick={showModal}
          >
            Comment
          </Button>
        </div>
      </div>
      <div className="discussion-tab">
        {listDiscussion.length > 0 ? renderCommentList() : <Empty />}
      </div>

      <div className="discussion__pagination">
        <CustomPagination
          totalPages={discussPagination.totalItems}
          curPage={discussPagination.numberOfCurrentPage}
          pageSize={discussPagination.sizeCurrentItems}
          onChange={onPageChange}
        />
      </div>
      <CommentModal
        isModalVisible={isModalVisible}
        setIsModalVisible={setIsModalVisible}
      />
    </>
  );
};

export default DiscussionTab;
