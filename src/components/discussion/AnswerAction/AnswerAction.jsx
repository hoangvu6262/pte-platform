import React from "react";
import { Button, Popconfirm, Tooltip } from "antd";
import { MessageOutlined } from "@ant-design/icons";

import "./styles.scss";

const AnswerAction = ({ handleDelete, isBoard, showModal }) => {
  return (
    <div className="action-comment">
      {!isBoard && (
        <div className="action-comment__button">
          <Tooltip placement="bottom" title="Delete" color="red">
            <Popconfirm
              title="Are you sure？"
              okText="Yes"
              cancelText="No"
              onConfirm={handleDelete}
            >
              <Button
                shape="circle"
                className="action-comment__button-report"
                // icon={<DeleteOutlined style={{ color: "#ffa0a0" }} />}
                icon={
                  <i
                    className="fa-regular fa-trash-can"
                    style={{ color: "#ffa0a0", fontSize: "18px" }}
                  ></i>
                }
              />
            </Popconfirm>
          </Tooltip>
        </div>
      )}

      <Button
        shape="circle"
        className="action-comment__button-report"
        icon={<MessageOutlined style={{ color: "#79d9c7" }} />}
        onClick={showModal}
      />
    </div>
  );
};

export default AnswerAction;
