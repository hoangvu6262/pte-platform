import React, { useState, useEffect } from "react";
import { Avatar, Comment, Tag, Divider, Skeleton } from "antd";
import { useSelector, useDispatch } from "react-redux";
import { useParams } from "react-router-dom";

import "./styles.scss";
import AnswerAction from "../AnswerAction/AnswerAction";
import SubComment from "../SubComment/SubComment";
import ScoreModal from "../../modal/ScoreModal/ScoreModal";
import ReplyCommentModal from "../../modal/ReplyCommentModal/ReplyCommentModal";

import {
  getListMeTabThunk,
  getListBoardTabThunk,
} from "../../../redux/slices/discuss/discussSlice";
import responseAPI from "../../../redux/api/practice/responseAPI";
import accountAPI from "../../../redux/api/account/accountAPI";

import { useCookies } from "react-cookie";

import moment from "moment";

import { renderUsername } from "../../../utils/user";
import capitalize from "lodash/capitalize";

import AVATAR from "../../../asset/img/avatar.png";

const { getUserInfoById } = accountAPI;
const { deleteUserResponse } = responseAPI;

const AnswerComment = ({
  id,
  createDate,
  valueResponses,
  userId,
  // score,
  // maxScore,
  isBoard,
  discussionSubs,
  // media,
  componentScores,
  description,
  questionResponseUsers,
  resultOverview
}) => {
  const [isScoreVisible, setIsScoreVisible] = useState(false);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [userIn4, setUserIn4] = useState();
  const [status, setStatus] = useState("idle");
  const [scoreStatus, setScoreStatus] = useState();
  // const questionResponseUser = questionResponseUsers[0]
  // const resultScore = questionResponseUser.resultScores[0]
  const score = resultOverview?.score
  const maxScore = resultOverview?.maxScore
  const media = resultOverview?.media

  const [cookies] = useCookies(["authKey"]);

  const { isSpeaking } = useSelector((state) => state.answer);

  const userAuthId = useSelector((state) => state.user.detail.id);
  const userDetail = useSelector((state) => state.user.detail);
  const lessonDetail = useSelector((state) => state.lesson.lessonDetail.content)

  const dispatch = useDispatch();

  const dispatchGetAnswer = () => {
    if (userAuthId) {
      dispatch(
        getListMeTabThunk({
          lessonId: lessonDetail.id,
          page: 0,
          userId
        })
      );
    }
  };

  const dispatchGetBoard = () => {
    dispatch(getListBoardTabThunk({ lessonId: lessonDetail.id, pageNumber: 0, userId }));
  };

  const showModal = () => {
    setIsModalVisible(true);
  };

  const showScore = () => {
    if (isBoard && (score === null || score === undefined)) {
      setIsScoreVisible(false);
    } else {
      if (score === null || score === undefined) {
        setScoreStatus("loading");
      }
      setIsScoreVisible(true);
    }
  };

  const handleDelete = async () => {
    await deleteUserResponse(createDate);
    dispatchGetAnswer();
    dispatchGetBoard();
  };

  const renderWFDDescription = (des) => {
    const parsedDes = JSON.parse(des);
    const newArray = [];
    parsedDes.forEach((des) => {
      // Each word has detail to describe status of that word
      des.details.forEach((detail) => {
        newArray[detail.index] = (
          <span
            className={`wfd-${detail.textResultStatus?.toLowerCase()}`}
            key={detail.index}
          >
            {detail.index === 0 ? capitalize(des.value) : des.value}
          </span>
        );
      });
    });

    return (
      <p className="wfd">
        {newArray.reduce((prev, curr) => [prev, " ", curr])}.
      </p>
    );
  };

  useEffect(() => {
    if (userId) {
      setStatus("loading");
      if (isBoard) {
        getUserInfoById({
          id: userId,
          authKey: cookies.authKey,
        })
          .then((res) => {
            setUserIn4(res.data.item);
            setStatus("idle");
          })
          .catch((err) => {
            setStatus("idle");
            setUserIn4();
          });
      } else {
        setUserIn4(userDetail);
        setStatus("idle");
      }
    }
  }, [cookies.authKey, isBoard, userDetail, userId]);

  return (
    <>
      {status === "loading" ? (
        <Skeleton
          avatar
          paragraph={{
            rows: 2,
          }}
        />
      ) : (
        <Comment
          className="audio-comment"
          actions={[
            <span key="comment-nested-reply-to" className="audio-comment__date">
              <Tag
                color="processing"
                className="audio-comment__tag"
                onClick={showScore}
              >
                {score !== undefined && score !== null ? (
                  <>
                    Score Info{" "}
                    <span>
                      {score}/{maxScore}
                    </span>
                  </>
                ) : (
                  <span>
                    {scoreStatus === "loading"
                      ? "Click to score"
                      : "Click to score"}
                  </span>
                )}
              </Tag>
            </span>,
            <AnswerAction
              isBoard={isBoard}
              handleDelete={handleDelete}
              showModal={showModal}
            />,
          ]}
          author={
            <h4 className="audio-comment__author">
              {!isBoard
                ? "Me"
                : userIn4?.username
                ? renderUsername(userIn4)
                : "User " + userId}{" "}
              -{" "}
              <span className="audio-comment__date">
                {moment(createDate).format("DD/MM/YYYY")}
              </span>
            </h4>
          }
          avatar={
            <Avatar
              className="audio-comment__avatar"
              src={userIn4?.avatar || AVATAR}
              alt={"Avatar"}
            />
          }
          content={
            isSpeaking ? (
              <audio
                controls
                src={media}
                title="record.mp3"
                type="audio/mpeg"
              />
            ) : (
              <div className="custom-comment__content">
                {/* Description for WFD */}
                {description
                  ? renderWFDDescription(description)
                  : valueResponses?.map((res, index) => {
                      return (
                        <p key={index} className="custom-comment__content--p">
                          {res}
                        </p>
                      );
                    })}
              </div>
            )
          }
        >
          {discussionSubs?.length > 0 && (
            <SubComment subCommentList={discussionSubs} parentId={id} />
          )}
        </Comment>
      )}
      <ReplyCommentModal
        parentId={id}
        parentName={
          userIn4?.username ? renderUsername(userIn4) : "User " + userId
        }
        where={isBoard ? "board" : "me"}
        isModalVisible={isModalVisible}
        setIsModalVisible={setIsModalVisible}
      />
      <ScoreModal
        isModalVisible={isScoreVisible}
        setIsModalVisible={setIsScoreVisible}
        score={{
          maxScore,
          score,
        }}
        componentScores={componentScores}
        response={isSpeaking ? media : valueResponses?.join("\n")}
        isAI={score === null || score === undefined}
        createDate={createDate}
        setScoreStatus={setScoreStatus}
      />
      <Divider />
    </>
  );
};

export default AnswerComment;
