import React from "react";

import "./styles.scss";

const Empty = () => {
  return (
    <>
      <div className="empty">
        <i className="fa-regular fa-folder-open"></i>
        <p>No Data</p>
      </div>
    </>
  );
};

export default Empty;
