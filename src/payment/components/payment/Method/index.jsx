import React from "react";

import "./styles.scss";
import clsx from "clsx";

const Method = ({ method, isActive, setIsActive }) => {
  return (
    <>
      <div
        className={clsx("method", { active: isActive === method.id })}
        onClick={() => setIsActive(method.id)}
      >
        <img src={method.logo} alt={method.name} />
        <p>{method.name}</p>
      </div>
    </>
  );
};

export default Method;
