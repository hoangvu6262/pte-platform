import React from "react";

import "./styles.scss";
import clsx from "clsx";
import { getListBenefitPremium } from "../../../../utils/payment";
import {
  LIST_BENEFITS_STANDARD,
  STANDARD_ID,
} from "../../../../config/contants/payment";

const PlanCard = ({ plan, planActive, handleActivePlan }) => {
  const listBenefit =
    planActive === STANDARD_ID
      ? LIST_BENEFITS_STANDARD
      : getListBenefitPremium(parseInt(plan.name.split("_")[1]));

  return (
    <>
      <div
        className={clsx("plan-card", {
          "card-active": planActive === plan.id,
        })}
        onClick={() => handleActivePlan(plan.id, plan.amount)}
      >
        <p>{plan.amount.split("$")[1] > 0 ? `USD ${plan.amount}` : "FREE"}</p>
        <h3>
          {planActive === -1
            ? `${plan.name.split("_")[1]}`
            : `VIP ${plan.name.split("_")[1]} Days`}
        </h3>
        {/* <p>{plan.name.split("_")[0]}</p> */}
        {planActive === plan.id && (
          <div className="plan-card__icon">
            <i className="fa-solid fa-check"></i>
          </div>
        )}
        <div className="plan-card__list">
          <ul>
            {listBenefit.map((benefit) => (
              <li key={benefit.id}>
                <i className="fa-solid fa-caret-right" />
                {benefit.name}
              </li>
            ))}
          </ul>
        </div>
      </div>
    </>
  );
};

export default PlanCard;
