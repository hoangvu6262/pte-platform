import React from "react";

import "./styles.scss";
import PlanCard from "../PlanCard";
// import Empty from "../../shared/Empty";
import {
  CODE_LOCALE_PLAN,
  STANDARD_ID,
  STANDARD_AMOUNT,
} from "../../../../config/contants/payment";

import { getListPlanFromlocals } from "../../../../utils/payment";

const listPlanStandard = [
  {
    id: STANDARD_ID,
    code: "FREE",
    name: "STANDARD_FOREVER",
    amount: "$0",
    description: "This is a price for BoostPTE.",
    status: 1,
  },
];

const ListPlan = ({
  listPlan = [],
  planActive,
  setPlanActive,
  setActiveTitle,
}) => {
  const listPlanByLocale = getListPlanFromlocals(
    CODE_LOCALE_PLAN.USA,
    listPlan
  );

  const handleActivePlan = (id, amount) => {
    if (amount === 0) {
      setActiveTitle("Get statred");
      setPlanActive(-1);
    } else {
      setActiveTitle("Payment confirm: USD " + amount);
      setPlanActive(id);
    }
  };

  return (
    <>
      <div className="list-plan">
        <div className="list-plan__container">
          {listPlan.length > 0 && (
            <div className="list-plan__container--title">
              <p>Select a plan:</p>
            </div>
          )}

          <div className="list-plan__container--list">
            {listPlanByLocale && listPlanByLocale.length > 0
              ? listPlanByLocale.map((plan) => (
                  <PlanCard
                    key={plan.id}
                    plan={plan}
                    planActive={planActive}
                    handleActivePlan={handleActivePlan}
                  />
                ))
              : // list plan empty
                // <Empty />
                // this is temp list
                listPlanStandard.map((plan) => (
                  <PlanCard
                    key={plan.id}
                    plan={plan}
                    planActive={planActive}
                    handleActivePlan={() =>
                      handleActivePlan(STANDARD_ID, STANDARD_AMOUNT)
                    }
                  />
                ))}
          </div>
        </div>
      </div>
    </>
  );
};

export default ListPlan;
