import React, { useState } from "react";

import "./styles.scss";
import Method from "../Method";

const ListMethod = ({ listMethod }) => {
  const [isActive, setIsActive] = useState(1);

  return (
    <>
      <div className="list-method">
        <div className="list-method__container">
          <div className="list-method__container--title">
            <p>Select a payment method:</p>
          </div>
          <div className="list-method__container--list">
            {listMethod &&
              listMethod.map((method) => (
                <Method
                  key={method.id}
                  method={method}
                  isActive={isActive}
                  setIsActive={setIsActive}
                />
              ))}
          </div>
        </div>
      </div>
    </>
  );
};

export default ListMethod;
