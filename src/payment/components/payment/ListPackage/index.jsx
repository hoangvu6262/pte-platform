import React from "react";

import "./styles.scss";
import PackageItem from "../PackageItem";

const ListPakage = ({ listPackage = [], packageActive, setPackageActive }) => {
  return (
    <>
      <div className="list-package">
        <div className="list-package__container">
          {listPackage.length > 0 &&
            listPackage?.map((item) => (
              <PackageItem
                key={item.id}
                paymentPackage={item}
                packageActive={packageActive}
                setPackageActive={setPackageActive}
              />
            ))}
        </div>
      </div>
    </>
  );
};

export default ListPakage;
