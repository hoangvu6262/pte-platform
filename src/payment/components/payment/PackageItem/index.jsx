import React from "react";

import "./styles.scss";
import clsx from "clsx";

const PackageItem = ({ paymentPackage, packageActive, setPackageActive }) => {
  const handleChoosePackage = () => {
    setPackageActive(paymentPackage.id);
  };

  return (
    <>
      <div
        className={clsx("package-card", {
          active: packageActive === paymentPackage.id,
        })}
        onClick={handleChoosePackage}
        title={paymentPackage.description}
      >
        <h5>{paymentPackage.name}</h5>
        {/* <p className="small">{paymentPackage.description}</p> */}
      </div>
    </>
  );
};

export default PackageItem;
