import React, { useState, useEffect } from "react";
import clsx from "clsx";
import { useSelector } from "react-redux";
import { useQuery } from "@tanstack/react-query";
import { STANDARD_ID } from "../../../../config/contants/payment";

import "./styles.scss";
import ListPakage from "../../../components/payment/ListPackage";
import ListPlan from "../../../components/payment/ListPlan";
import ListMethod from "../../../components/payment/ListMethod";
import Loading from "../../../../components/shared/Loading/Loading";
import STATUS from "../../../../config/contants/status";

import paymentAPI from "../../../../redux/api/payment/paymentAPI";

const listMethod = [
  // {
  //   id: 1,
  //   name: "Wallet",
  //   logo: "https://img.icons8.com/3d-fluency/94/null/coin-wallet.png",
  // },
  {
    id: 1,
    name: "Visa",
    logo: "https://img.icons8.com/plasticine/100/null/visa.png",
  },
];

const { getPackages, getPlans, getDirectCheckoutLink } = paymentAPI;

const Payment = () => {
  const [submitStatus, setSubmitStatus] = useState(STATUS.IDLE);
  const [planLoading, setPlanLoading] = useState(STATUS.IDLE);
  const [listPlan, setListPlan] = useState([]);
  const [packageActive, setPackageActive] = useState(null);
  const [planActive, setPlanActive] = useState(null);
  const [activeTile, setActiveTitle] = useState("Select plan to play");

  const { detail } = useSelector((state) => state.user);

  const { data: listPackage } = useQuery({
    queryKey: ["packages"],
    queryFn: async () => {
      return await getPackages().then((res) => {
        setPackageActive(res.data.item[0]?.id);
        return res.data.item;
      });
    },
    refetchOnWindowFocus: false,
  });

  useEffect(() => {
    setPlanLoading(STATUS.LOADING);
    setPlanActive(null);
    setActiveTitle("Select plan to play");
    packageActive &&
      getPlans(packageActive).then((plans) => {
        setListPlan(plans.data.item);
        setPlanLoading(STATUS.SUCCESS);
      });
  }, [packageActive]);

  useEffect(() => {
    if (listPlan && listPlan.length === 0) {
      setPlanActive(STANDARD_ID);
      setActiveTitle("Get started");
    }
  }, [listPlan]);

  const handleCheckout = () => {
    setSubmitStatus(STATUS.LOADING);
    getDirectCheckoutLink(planActive, detail.id).then((checkoutLink) => {
      window.location.replace(checkoutLink.data.item);
    });
  };

  return (
    <>
      <div className="payment">
        <ListPakage
          listPackage={listPackage}
          packageActive={packageActive}
          setPackageActive={setPackageActive}
        />
        {planLoading === STATUS.LOADING ? (
          <Loading />
        ) : (
          <ListPlan
            listPlan={listPlan}
            planActive={planActive}
            setPlanActive={setPlanActive}
            setActiveTitle={setActiveTitle}
          />
        )}

        {listPlan.length > 0 && <ListMethod listMethod={listMethod} />}

        <div className="payment__button">
          {submitStatus === STATUS.LOADING ? (
            <Loading />
          ) : (
            <button
              type="button"
              className={clsx({
                active: planActive && planActive > 0,
              })}
              onClick={handleCheckout}
              disabled={planActive && planActive > 0 ? false : true}
            >
              {activeTile}
            </button>
          )}
        </div>
      </div>
    </>
  );
};

export default Payment;
