import React, { useEffect } from "react";
import "antd/dist/antd.min.css";
import "react-toastify/dist/ReactToastify.css";

import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { practiceRouter, accountRouter, userRouter } from "./config/router";
import PrivateRoutes from "./config/PrivateRoutes";

import MainLayout from "./layout/main/MainLayout";
import PracticeLayout from "./layout/practice/PracticeLayout";
import AccountLayout from "./layout/account/AccountLayout";
import UserLayout from "./layout/user/UserLayout";
import Home from "./page/main/Home/Home";
import NotFound from "./page/main/NotFound/NotFound";
import MyVocab from "./page/main/MyVocab/MyVocab";
import Search from "./page/main/Search/Search";
import Payment from "./payment/pages/main/payment/Payment";

import { toast, ToastContainer } from "react-toastify";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { useDispatch, useSelector } from "react-redux";
import userSlice from "./redux/slices/user/userSlice";
import { getCategoriesAndMarkThunk } from "./redux/slices/shared/sharedSlice";

import { useCookies } from "react-cookie";
import accountAPI from "./redux/api/account/accountAPI";
import globalAPI from "./redux/api/practice/globalAPI";

const { getUserInfo } = accountAPI;

const { getToken } = globalAPI;
// Create a client
const queryClient = new QueryClient();

function App() {
  const [cookies, setCookie, removeCookie] = useCookies([
    "authKey",
    "username",
    "token",
  ]);
  const dispatch = useDispatch();
  const userDetail = useSelector((state) => state.user.detail);

  useEffect(() => {
    dispatch(getCategoriesAndMarkThunk());
  }, [dispatch]);

  useEffect(() => {
    if (cookies.authKey) {
      getUserInfo({
        authKey: cookies.authKey,
        username: cookies.username,
      })
        .then((res) => {
          dispatch(
            userSlice.actions.updateUser({
              authKey: cookies.authKey,
              detail: res.data,
            })
          );
        })
        .catch(() => {
          removeCookie("authKey", { path: "/" });
          removeCookie("username", { path: "/" });
          removeCookie("token", { path: "/" });
          toast.error("Authenticate failed");
        });
    } else {
      dispatch(userSlice.actions.resetUser());
    }
  }, [cookies, dispatch, removeCookie]);

  useEffect(() => {
    if (userDetail.id) {
      getToken({
        username: userDetail.username,
        authKey: cookies.authKey,
        userType: userDetail.userType,
      }).then((res) => {
        setCookie("token", res.accessToken);
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userDetail.id]);

  const renderRouter = (routes) => {
    return routes.map(({ path, Component, id }) => {
      return <Route key={id + path} path={path} element={<Component />} />;
    });
  };

  return (
    <QueryClientProvider client={queryClient}>
      <div className="app">
        <Router>
          <Routes>
            <Route
              element={
                <PrivateRoutes
                  redirectLink="/account/sign-in"
                  check={cookies.authKey}
                />
              }
            >
              <Route path="/" element={<MainLayout />} exact>
                <Route index element={<Home />} />
                <Route path="/search" element={<Search />} />
                <Route path="/payment" element={<Payment />} />
                <Route
                  path="/myvocab/:skill/:categoryId"
                  element={<MyVocab />}
                />
              </Route>
              <Route path="/practice" element={<PracticeLayout />}>
                {renderRouter(practiceRouter)}
              </Route>
              <Route path="/user" element={<UserLayout />}>
                {renderRouter(userRouter)}
              </Route>
            </Route>
            <Route
              element={
                <PrivateRoutes redirectLink="/" check={!cookies.authKey} />
              }
            >
              <Route path="/account" element={<AccountLayout />}>
                {renderRouter(accountRouter)}
              </Route>
            </Route>
            <Route path="*" element={<NotFound />} />
          </Routes>
        </Router>
        <ToastContainer autoClose={2000} hideProgressBar={true} />
      </div>
    </QueryClientProvider>
  );
}

export default App;
