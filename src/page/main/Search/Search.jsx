import React, { useState, useEffect } from "react";

import "./styles.scss";
import { Input } from "antd";

import SearchCategories from "../../../components/search/SearchCategories/SearchCategories";
import SearchLessons from "../../../components/search/SearchLessons/SearchLessons";

import { useLocation, useSearchParams } from "react-router-dom";
import { useSelector } from "react-redux";
import queryString from "query-string";
import lessonAPI from "../../../redux/api/practice/lessonAPI";

import { findParentCategory } from "../../../utils/practice";
import CustomPagination from "../../../components/shared/Pagination/CustomPagination";

const { Search } = Input;
const { getLessonList } = lessonAPI;
const LIMIT = 10;

const SearchPage = () => {
  const [lessonList, setLessonList] = useState([]);
  const [page, setPage] = useState(0);
  const [pagination, setPagination] = useState();
  const location = useLocation();
  const parsedSearch = queryString.parse(location.search);
  let [, setSearchParams] = useSearchParams({});

  const handleSearch = (data) => {
    setSearchParams(queryString.stringify({ ...parsedSearch, ...data }));
  };

  const categories = useSelector((state) => state.shared.categories);

  useEffect(() => {
    getLessonList({
      categoryId: parseInt(parsedSearch.category),
      searchKeyword: parsedSearch.keyword,
      pageNumber: page,
      limit: LIMIT,
    }).then((res) => {
      setLessonList(res.contents);
      setPagination({
        totalPages: res.totalPages,
        totalItems: res.totalItems,
        sizeCurrentItems: res.sizeCurrentItems,
        numberOfCurrentPage: res.numberOfCurrentPage,
      });
    });
  }, [parsedSearch, page]);

  return (
    <div className="search-page">
      <div className="search-page__input-search">
        <h4>Search</h4>
        <Search
          enterButton="Search"
          size="medium"
          onSearch={(value) => {
            handleSearch({ ...parsedSearch, keyword: value });
          }}
          defaultValue={parsedSearch.keyword}
        />
      </div>
      <SearchCategories
        searchCategory={parseInt(parsedSearch.category)}
        handleSearch={handleSearch}
      />
      <SearchLessons
        lessonList={lessonList}
        parent={
          categories.length > 0
            ? findParentCategory(categories, parseInt(parsedSearch.category))
            : undefined
        }
        categoryId={parseInt(parsedSearch.category)}
      />
      {pagination && (
        <div className="search-page__pagination">
          <CustomPagination
            totalPages={pagination.totalItems}
            curPage={pagination.numberOfCurrentPage}
            pageSize={pagination.sizeCurrentItems}
            onChange={(page) => setPage(page - 1)}
          />
        </div>
      )}
    </div>
  );
};

export default SearchPage;
