import React, { useState, useEffect, useCallback } from "react";

import { useParams, Link, useNavigate } from "react-router-dom";

import { Tabs, Row, Col, Empty, Drawer } from "antd";

import Loading from "../../../components/shared/Loading/Loading";
import VocabList from "../../../container/VocabList/VocabList";

import globalAPI from "../../../redux/api/practice/globalAPI";
import myVocabAPI from "../../../redux/api/practice/myVocabAPI";

import "./styles.scss";
import { useSelector } from "react-redux";

export const myVocabContext = React.createContext();

const { TabPane } = Tabs;
const { getCategoriesByParent } = globalAPI;
const { getVocabList } = myVocabAPI;

const listPane = {
  speaking: 1,
  writing: 8,
  reading: 11,
  listening: 18,
};

const MyVocab = () => {
  const [listSidebar, setListSidebar] = useState([]);
  const [status, setStatus] = useState("idle");
  const [myVocabData, setMyVocabData] = useState();
  const [openDrawer, setOpenDrawer] = useState(false);
  const [vocabListStatus, setVocabListStatus] = useState(false);
  const userDetail = useSelector((state) => state.user.detail);


  const { skill, categoryId } = useParams();

  const navigate = useNavigate();

  const handleTabChange = (tab) => {
    const skill = Object.keys(listPane).filter(
      (key) => listPane[key].toString() === tab
    )[0];

    if (skill === "speaking") {
      navigate("/myvocab/speaking/3");
    } else {
      navigate(`/myvocab/${skill}/${parseInt(tab) + 1}`);
    }
  };

  const handleGetVocabList = useCallback(
    async () => {
      if (userDetail.id){
        setVocabListStatus(true);
        getVocabList({
          categoryId,
          userId: userDetail.id
        })
          .then((res) => {
            setMyVocabData(res);
            setStatus("idle");
            setVocabListStatus(false);
          })
          .catch((err) => {
            setMyVocabData();
            setStatus("error");
            setVocabListStatus(false);
          });
      }
      
    },
    [categoryId, userDetail.id]
  );

  const onClose = () => {
    setOpenDrawer(false);
  };

  const showDrawer = () => {
    setOpenDrawer(true);
  };

  useEffect(() => {
    const getData = async () => {
      setStatus("loading");
      await getCategoriesByParent(listPane[skill])
        .then((res) => {
          setListSidebar(res);
        })
        .catch((err) => {
          setStatus("error");
        });

      await handleGetVocabList(0);
    };

    getData();
  }, [skill, categoryId, handleGetVocabList]);

  return (
    <div className="my-vocab">
      <h1 className="my-vocab__title">My Vocab List</h1>

      {status === "loading" && <Loading />}
      {status === "error" && <Empty />}
      {status === "idle" && (
        <>
          <Row gutter={[24, 24]} style={{ position: "relative" }}>
            <Col sm={6} xs={0}>
              <div className="my-vocab__sidebar">
                {listSidebar.map((cate) => {
                  return (
                    <Link
                      className={`my-vocab__sidebar--link ${
                        parseInt(categoryId) === cate.id
                          ? "my-vocab__sidebar--link-active"
                          : ""
                      }`}
                      key={cate.id}
                      to={`/myvocab/${skill}/${cate.id}`}
                    >
                      {cate.name}
                    </Link>
                  );
                })}
              </div>
            </Col>
            <Col sm={18} xs={24}>
              <myVocabContext.Provider
                value={{
                  data: myVocabData,
                  status,
                  handleGetVocabList,
                  vocabListStatus,
                }}
              >
                <Tabs
                  activeKey={listPane[skill].toString()}
                  onChange={handleTabChange}
                >
                  <TabPane tab="Speaking" key={listPane["speaking"]}>
                    <VocabList
                      data={myVocabData}
                      status={status}
                      handlePageChange={handleGetVocabList}
                    />
                  </TabPane>
                  <TabPane tab="Writing" key={listPane["writing"]}>
                    <VocabList
                      data={myVocabData}
                      status={status}
                      handlePageChange={handleGetVocabList}
                    />
                  </TabPane>
                  <TabPane tab="Reading" key={listPane["reading"]}>
                    <VocabList
                      data={myVocabData}
                      status={status}
                      handlePageChange={handleGetVocabList}
                    />
                  </TabPane>
                  <TabPane tab="Listening" key={listPane["listening"]}>
                    <VocabList
                      data={myVocabData}
                      status={status}
                      handlePageChange={handleGetVocabList}
                    />
                  </TabPane>
                </Tabs>
              </myVocabContext.Provider>
            </Col>
            <div className="my-vocab__open-sidebar" onClick={showDrawer}>
              <i className="fa-solid fa-angle-right"></i>
            </div>
          </Row>
        </>
      )}

      <Drawer
        onClose={onClose}
        visible={openDrawer}
        closeIcon={false}
        placement="left"
        width={"fit-content"}
      >
        {listSidebar.map((cate) => {
          return (
            <Link
              className={`my-vocab__sidebar--link ${
                parseInt(categoryId) === cate.id
                  ? "my-vocab__sidebar--link-active"
                  : ""
              }`}
              key={cate.id}
              to={`/myvocab/${skill}/${cate.id}`}
            >
              {cate.name}
            </Link>
          );
        })}
      </Drawer>
    </div>
  );
};

export default MyVocab;
