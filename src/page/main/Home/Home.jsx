import React from "react";

import "./styles.scss";
import SkillList from "../../../components/shared/SkillList/SkillList";

const HomePage = () => {
  return (
    <div className="home">
      <div className="home-container">
        <SkillList />
      </div>
    </div>
  );
};

export default HomePage;
