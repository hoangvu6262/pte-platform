import React from "react";
import Header from "../../../components/shared/Header/Header";

import "./styles.scss";
import Button from "../../../components/shared/Button/Button";
import boostpteHome from "../../../asset/img/boostpte_home.png";
import { useNavigate } from "react-router-dom";

const NotFound = () => {
  const navigate = useNavigate();

  return (
    <>
      <Header />
      <div
        className="not-found"
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <h1>Coming Soon Stay Tuned!</h1>
        <p>Our platform is under construction, follow us for update now!</p>
        <img src={boostpteHome} alt="boostpte-home" />
        <div
          onClick={() => {
            navigate("");
          }}
        >
          <Button>Back To Homepage</Button>
        </div>
        <span>
          BoostPTE #1 Online PTE Practice – PTE Test Platform – Copyright @
          2022. All rights reserved.
        </span>
      </div>
    </>
  );
};

export default NotFound;
