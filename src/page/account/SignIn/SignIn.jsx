import React, { useState } from "react";

import "./styles.scss";
import { Checkbox } from "antd";

import CustomInput from "../../../components/shared/CustomInput/CustomInput";
import Button from "../../../components/shared/Button/Button";
import Loading from "../../../components/shared/Loading/Loading";

import { Link, useNavigate } from "react-router-dom";
import accountAPI from "../../../redux/api/account/accountAPI";
import { useCookies } from "react-cookie";

import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { toast } from "react-toastify";

const schema = yup.object({
  username: yup
    .string()
    .required("Username is a required field.")
    .min(5, "Username must be at least 5 characters."),
  password: yup.string().required("Password is a required field."),
});

const { signInAPI } = accountAPI;

const SignIn = () => {
  const [, setCookie] = useCookies(["authKey", "username"]);
  const [status, setStatus] = useState("idle");
  const [isRemember, setIsRemember] = useState(false);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const navigate = useNavigate();

  const onSubmit = async (data) => {
    setStatus("loading");
    signInAPI({
      username: data.username,
      password: data.password,
    })
      .then((res) => {
        if (res.data?.authKey) {
          const expireDate = new Date();
          expireDate.setDate(expireDate.getDate() + 1);

          setCookie("authKey", res.data.authKey, {
            path: "/",
            expires: isRemember ? expireDate : undefined,
          });
          setCookie("username", data.username, {
            path: "/",
            expires: isRemember ? expireDate : undefined,
          });

          toast.success("Sign In successfully");
          setStatus("success");
          navigate("/");
        } else if (res.error) {
          setStatus("error");
          toast.error(res.error.error.message);
        }
      })
      .catch((err) => {
        setStatus("error");
        toast.error(err.response.data.error.message);
      });
  };

  return (
    <div className="sign-in">
      <form className="account-form__main" autoComplete="off">
        <CustomInput
          title="Username"
          name="username"
          type="text"
          register={register}
          errors={errors.username?.message}
          submit={handleSubmit(onSubmit)}
        />
        <CustomInput
          title="Password"
          name="password"
          type="password"
          register={register}
          errors={errors.password?.message}
          submit={handleSubmit(onSubmit)}
        />

        <div className="sign-in__form--wrapper">
          <Checkbox
            checked={isRemember}
            onChange={(e) => {
              setIsRemember(e.target.checked);
            }}
          >
            Remember Me
          </Checkbox>
          <Link to="/account/forgot-password" className="sign-in-forget">
            Forgot password?
          </Link>
        </div>
      </form>

      {status === "loading" ? (
        <Loading />
      ) : (
        <>
          <div className="sign-in__btn">
            <Button onClick={handleSubmit(onSubmit)}>Sign In</Button>
          </div>
        </>
      )}

      <div className="account-form__foot">
        <p>
          Don't have an account? <Link to="/account/sign-up">Sign Up</Link>
        </p>
      </div>
    </div>
  );
};

export default SignIn;
