import React, { useState } from "react";

import CustomInput from "../../../components/shared/CustomInput/CustomInput";
import Button from "../../../components/shared/Button/Button";
import Loading from "../../../components/shared/Loading/Loading";

import { useNavigate, useLocation } from "react-router-dom";

import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { yupHelper } from "../../../helpers/yupHelper";
import { toast } from "react-toastify";

import accountAPI from "../../../redux/api/account/accountAPI";

const { resetPasswordAPI } = accountAPI;

const schema = yup.object({
  password: yupHelper.password,
  confirmPassword: yupHelper.confirmPassword,
});

const ResetPassword = () => {
  const [status, setStatus] = useState("idle");

  const location = useLocation();

  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = async (data) => {
    setStatus("loading");
    const queryString = require("query-string");
    const parsedSearch = queryString.parse(location.search);
    const body = {
      newPassword: data.password,
      email: parsedSearch.email,
      token: parsedSearch.token.split(" ").join("+"),
    };

    await resetPasswordAPI(body)
      .then((res) => {
        if (res.data) {
          toast.success("Reset password successful");
          setStatus("success");
          navigate("/account/sign-in");
        } else if (res.error) {
          setStatus("error");
        }
      })
      .catch((err) => {
        toast.error("Reset password Failed");
        setStatus("error");
      });
  };

  return (
    <div className="reset-password">
      <form
        className="account-form__main"
        autoComplete="off"
        onSubmit={(e) => e.preventDefault()}
      >
        <CustomInput
          title="Reset Password"
          name="password"
          type="password"
          register={register}
          errors={errors.password?.message}
          submit={handleSubmit(onSubmit)}
        />
        <CustomInput
          title="Confirm Reset Password"
          name="confirmPassword"
          type="password"
          register={register}
          errors={errors.confirmPassword?.message}
          submit={handleSubmit(onSubmit)}
        />
      </form>

      {status === "loading" ? (
        <Loading />
      ) : (
        <>
          <div className="sign-in__btn">
            <Button onClick={handleSubmit(onSubmit)}>Reset Password</Button>
          </div>
        </>
      )}
    </div>
  );
};

export default ResetPassword;
