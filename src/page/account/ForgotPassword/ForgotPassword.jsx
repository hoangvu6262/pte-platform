import React, { useState } from "react";

import "./styles.scss";

import CustomInput from "../../../components/shared/CustomInput/CustomInput";
import Button from "../../../components/shared/Button/Button";
import Loading from "../../../components/shared/Loading/Loading";

import { useNavigate, Link } from "react-router-dom";

import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { yupHelper } from "../../../helpers/yupHelper";
import { toast } from "react-toastify";

import accountAPI from "../../../redux/api/account/accountAPI";

const { forgotPasswordAPI } = accountAPI;

const schema = yup.object({
  email: yupHelper.email,
});

const ForgotPassword = () => {
  const [status, setStatus] = useState("idle");

  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = async (data) => {
    setStatus("loading");
    const origin = window.location.origin;

    forgotPasswordAPI({
      email: data.email,
      resetPasswordLink: `${origin}/account/reset-password`,
    })
      .then((res) => {
        setStatus("success");
        navigate("/account/sign-in");
        toast.success("We have sent a reset password link to your email.");
      })
      .catch((err) => {
        setStatus("error");
        toast.error(err.response.data.error.message);
      });
  };

  return (
    <div className="forgot-password">
      <form
        className="account-form__main"
        autoComplete="off"
        onSubmit={(e) => e.preventDefault()}
      >
        <CustomInput
          title="Email"
          name="email"
          type="email"
          register={register}
          errors={errors.email?.message}
          submit={handleSubmit(onSubmit)}
        />
      </form>

      {status === "loading" ? (
        <Loading />
      ) : (
        <>
          <div className="sign-in__btn">
            <Button onClick={handleSubmit(onSubmit)}>
              Send Reset Password Link
            </Button>
          </div>
        </>
      )}

      <div className="account-form__foot">
        <p>
          Back to <Link to="/account/sign-in">Sign In</Link>
        </p>

        {status === "error" && (
          <p>
            Did not receive the email? Check your spam filter, or try another
            email address.
          </p>
        )}
      </div>
    </div>
  );
};

export default ForgotPassword;
