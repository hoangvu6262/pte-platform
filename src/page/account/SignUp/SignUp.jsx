import React, { useState } from "react";

import "./styles.scss";

import CustomInput from "../../../components/shared/CustomInput/CustomInput";
import Button from "../../../components/shared/Button/Button";
import Loading from "../../../components/shared/Loading/Loading";

import { Link, useNavigate } from "react-router-dom";
import accountAPI from "../../../redux/api/account/accountAPI";

import { toast } from "react-toastify";

import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { yupHelper } from "../../../helpers/yupHelper";
const schema = yup.object({
  username: yupHelper.username,
  email: yupHelper.email,
  password: yupHelper.password,
  confirmPassword: yupHelper.confirmPassword,
});

const { signUpAPI } = accountAPI;

const SignUp = () => {
  const [status, setStatus] = useState("idle");
  const [errorFields, setErrorFields] = useState([]);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const navigate = useNavigate();

  const getMessage = (field) => {
    const index = errorFields.findIndex((error) => error.key === field);

    if (index !== -1) {
      return errorFields[index].errorMessage.join("\n");
    } else {
      return "";
    }
  };

  const onSubmit = async (data) => {
    setStatus("loading");
    signUpAPI({
      username: data.username,
      password: data.password,
      email: data.email,
    })
      .then((res) => {
        if (res.error) {
          toast.error(res.error.error.message);
          setStatus("error");
        } else if (res.data) {
          setStatus("success");
          navigate("/account/sign-in");
          toast.success("Create account successfully");
        }
      })
      .catch((err) => {
        if (err.response.data.error.error?.length > 0) {
          setErrorFields(err.response.data.error.error);
        }
        toast.error(err.response.data.error.error.message);
        setStatus("error");
      });
  };

  return (
    <div className="sign-up">
      <form className="account-form__main">
        <CustomInput
          title="Username"
          name="username"
          type="text"
          register={register}
          errors={errors.username?.message || getMessage("Username")}
          submit={handleSubmit(onSubmit)}
        />
        <CustomInput
          title="Email"
          name="email"
          type="email"
          register={register}
          errors={errors.email?.message || getMessage("Email")}
          submit={handleSubmit(onSubmit)}
        />
        <CustomInput
          title="Password"
          name="password"
          type="password"
          register={register}
          errors={errors.password?.message || getMessage("Password")}
          submit={handleSubmit(onSubmit)}
        />
        <CustomInput
          title="Confirm Password"
          name="confirmPassword"
          type="password"
          register={register}
          errors={errors.confirmPassword?.message}
          submit={handleSubmit(onSubmit)}
        />
      </form>

      {status === "loading" ? (
        <Loading />
      ) : (
        <div className="sign-up__btn">
          <Button onClick={handleSubmit(onSubmit)}>Sign Up</Button>
        </div>
      )}

      <div className="account-form__foot">
        <p>
          Already have an account? <Link to="/account/sign-in">Sign In</Link>
        </p>
      </div>
    </div>
  );
};

export default SignUp;
