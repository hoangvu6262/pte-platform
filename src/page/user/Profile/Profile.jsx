import React, { useState } from "react";

import "./styles.scss";

import { useSelector } from "react-redux";

import moment from "moment/moment";

import AVATAR from "../../../asset/img/avatar.png";

import EditProfileModal from "../../../components/modal/EditProfileModal/EditProfileModal";

const ProfileField = ({ title, value, verified }) => {
  return (
    <div className="profile-field">
      <div className="profile-field__header">
        <h1>{title}</h1>
        {verified}
      </div>
      <div className="profile-field__main">
        <>
          {value ? (
            <p>{value}</p>
          ) : (
            <p style={{ fontStyle: "italic" }}>
              Please update {title.toLowerCase()}
            </p>
          )}
        </>
      </div>
    </div>
  );
};

const Profile = () => {
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);

  const { detail } = useSelector((state) => state.user);

  const handleEditClick = () => {
    setIsEditModalVisible(true);
  };

  return (
    <div className="profile">
      <div className="profile-header">
        <div className="profile-avatar">
          <img src={detail.avatar ? detail.avatar : AVATAR} alt="avatar" />
        </div>

        <button className="profile-edit" onClick={handleEditClick}>
          <i className="fa-solid fa-user-pen"></i>
          Edit profile
        </button>
      </div>

      <div className="profile-main">
        <ProfileField
          title="Username"
          value={detail.username}
          verified={
            detail.isUserVerified ? (
              <img
                alt="verified"
                src="https://img.icons8.com/color/48/000000/verified-account.png"
              />
            ) : (
              ""
            )
          }
        />
        <ProfileField title="Nickname" value={detail.nickName} />
        <ProfileField title="First name" value={detail.firstName} />
        <ProfileField title="Last name" value={detail.lastName} />
        <ProfileField
          title="Email"
          value={detail.emailAddress}
          verified={
            detail.isEmailAddressVerified ? (
              <img
                src="https://img.icons8.com/color/48/000000/verified-account.png"
                alt=""
              />
            ) : (
              ""
            )
          }
        />
        <ProfileField title="Phone" value={detail.phoneNumber1} />
        <ProfileField title="Website" value={detail.website} />
        <ProfileField
          title="Created date"
          value={moment(detail.createdDate).format("DD/MM/YYYY")}
        />
      </div>

      <EditProfileModal
        setIsModalVisible={setIsEditModalVisible}
        isModalVisible={isEditModalVisible}
      />
    </div>
  );
};

export default Profile;
