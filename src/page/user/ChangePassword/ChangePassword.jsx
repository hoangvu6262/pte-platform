import React, { useState } from "react";

import "./styles.scss";

import CustomInput from "../../../components/shared/CustomInput/CustomInput";
import Button from "../../../components/shared/Button/Button";
import Loading from "../../../components/shared/Loading/Loading";

import { useCookies } from "react-cookie";

import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { yupHelper } from "../../../helpers/yupHelper";
import { toast } from "react-toastify";

import accountAPI from "../../../redux/api/account/accountAPI";

const { changePasswordAPI } = accountAPI;

const schema = yup.object({
  oldPassword: yupHelper.password,
  newPassword: yupHelper.password,
  confirmPassword: yup
    .string()
    .required("Password confirmation is a required field.")
    .oneOf(
      [yup.ref("newPassword"), null],
      "Password confirmation does not match."
    ),
});

const ChangePassword = () => {
  const [status, setStatus] = useState("idle");
  const [cookies] = useCookies(["authKey"]);

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = async (data) => {
    setStatus("loading");
    changePasswordAPI({
      body: {
        oldPassword: data.oldPassword,
        newPassword: data.newPassword,
      },
      authKey: cookies.authKey,
    })
      .then((res) => {
        toast.success(res.data.message);
        setStatus("success");
        reset();
      })
      .catch((err) => {
        toast.error(err.response.data.error.message);
        setStatus("error");
      });
  };

  return (
    <div className="change-password">
      <form
        className="account-form__main"
        autoComplete="off"
        onSubmit={(e) => e.preventDefault()}
      >
        <CustomInput
          title="Old Password"
          name="oldPassword"
          type="password"
          register={register}
          errors={errors.oldPassword?.message}
        />
        <CustomInput
          title="New Password"
          name="newPassword"
          type="password"
          register={register}
          errors={errors.newPassword?.message}
        />
        <CustomInput
          title="Confirm Password"
          name="confirmPassword"
          type="password"
          register={register}
          errors={errors.confirmPassword?.message}
        />
      </form>

      {status === "loading" ? (
        <Loading />
      ) : (
        <>
          <div className="sign-in__btn">
            <Button onClick={handleSubmit(onSubmit)}>Reset Password</Button>
          </div>
        </>
      )}
    </div>
  );
};

export default ChangePassword;
