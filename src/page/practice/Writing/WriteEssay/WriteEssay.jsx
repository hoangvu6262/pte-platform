/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect, useRef } from "react";

import "./styles.scss";
import { Input, Form, Empty } from "antd";

import { useDispatch, useSelector } from "react-redux";
import {
  getLessonDetailThunk,
} from "../../../../redux/slices/lesson/lessonSlice";
import answerSlices, {
  changeIsSpeakingThunk,
} from "../../../../redux/slices/answer/answerSlice";

import { useParams, useLocation } from "react-router-dom";

import FunctionBar from "../../../../container/FunctionBar/FunctionBar";
import Answer from "../../../../container/Answer/Answer";
import PageHeader from "../../../../components/practice/PageHeader/PageHeader";
import DictModeModal from "../../../../components/modal/DictModeModal/DictModeModal";
import Paragraph from "../../../../components/practice/Paragraph/Paragraph";
import Button from "../../../../components/shared/Button/Button";
import Timer from "../../../../components/practice/Timer/Timer";
import Tested from "../../../../components/practice/LessonTools/LessonTools";
import Loading from "../../../../components/shared/Loading/Loading";
import { getSkillCode } from "../../../../helpers/practiceHelper";

const { TextArea } = Input;

const WriteEssay = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isAnswerVisible, setIsAnswerVisible] = useState(false);
  const [isSubmitted, setIsSubmitted] = useState(false);
  const [wordCount, setWordCount] = useState(0);
  const [text, setText] = useState("");
  const [form] = Form.useForm();
  const formRef = useRef();

  const { zorder } = useParams();
  const {pathname} = useLocation()
  const { status, content } = useSelector((state) => state.lesson.lessonDetail);

  const submitStatus = useSelector((state) => state.answer.status);

  const dispatch = useDispatch();

  const openModal = () => {
    setIsModalVisible(true);
  };

  const handleCountForm = (text) => {
    if (text === "") {
      setWordCount(0);
      setText(text);
    } else {
      const splittedText = text.trim().split(/\s+/);
      setWordCount(splittedText.filter((word) => word !== " ").length);
      setText(text);
    }
  };
  const handleRedo = () => {
    formRef.current?.resetFields();
    setWordCount(0);
  };

  useEffect(() => {
    window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
    dispatch(changeIsSpeakingThunk(false));
    dispatch(answerSlices.actions.redoAnswering());
  }, [dispatch]);

  useEffect(() => {
    handleRedo();
    dispatch(getLessonDetailThunk({skill: getSkillCode(pathname), zorder}));

    dispatch(answerSlices.actions.redoAnswering());
  }, [zorder]);

  useEffect(() => {
    if (submitStatus === 3) {
      setIsSubmitted(true);
    } else if (submitStatus === 1) {
      handleRedo();
      setIsSubmitted(false);
    } else {
      setIsSubmitted(false);
    }
  }, [submitStatus]);

  useEffect(() => {
    let answer = {
      question: {
        id:
          content?.questionGroup?.questions &&
          content?.questionGroup?.questions[0]?.id,
      },
      questionResponseUsers: [
        {
          valueText: text,
        },
      ],
    };

    dispatch(answerSlices.actions.updateAnswering(answer));
  }, [text, dispatch, content]);

  return (
    <div className="practice-body writing">
      <PageHeader
        title="Write Essay"
        chip="Study Guide"
        type="Writing"
        idExercise={content.title && status === "success" ? content.title : ""}
        content="You will have 20 minutes to plan, write and revise an essay about the topic below. Your response will be judged on how well you develop a position, organize your ideas, present supporting details, and control the elements of standard written English. You should write 200-300 words."
      />

      {status === "loading" && <Loading />}

      {status === "success" && (
        <>
          <div className="practice-timer-n-test">
            <Timer
              time={content.duration}
              title="Time: "
              name="Time: "
              overtime={true}
            />
            <Tested
              lessonId={content.id}
              openModal={openModal}
              priority={
                content?.priorities?.length > 0 &&
                content.priorities[0].priority
              }
            />
          </div>

          <div className="practice-body__paragraph writing-paragraph">
            <Paragraph content={content?.questionGroup?.questions[0]?.name} />
          </div>
          <div className="practice-body__body">
            <Form
              form={form}
              autoComplete="off"
              className={`writing-form ${isSubmitted ? "disabled" : ""}`}
              onChange={(values) => {
                handleCountForm(values.target.value);
              }}
              disabled={isSubmitted}
              ref={formRef}
            >
              <Form.Item name="writing">
                <TextArea autoSize={{ minRows: 12 }} />
              </Form.Item>
              {isSubmitted && (
                <div className="writing-form-redo">
                  <Button
                    outline={true}
                    onClick={() => {
                      dispatch(answerSlices.actions.redoAnswering());
                    }}
                  >
                    Re-do
                  </Button>
                </div>
              )}
            </Form>
          </div>
          <div className="writing-word-count">Word Count: {wordCount}</div>

          <FunctionBar
            toggleAnswer={() => setIsAnswerVisible(!isAnswerVisible)}
            openModal={() => {
              openModal();
            }}
          />
          {isAnswerVisible && (
            <Answer explanation={content.explanation}>
              {content?.questionGroup?.questions &&
                content?.questionGroup?.questions.map((ques) => {
                  return (
                    <div key={ques.id}>
                      {ques.questionSolutions && (
                        <>
                          <h5>Answer:</h5>
                          {ques.questionSolutions.map((solution) => (
                            <Paragraph
                              key={solution.valueText}
                              content={
                                (solution.valueText &&
                                  (solution.valueText.charAt(0) === "#"
                                    ? ""
                                    : solution.valueText + ". ")) +
                                (solution.explanation
                                  ? solution.explanation
                                  : "")
                              }
                            ></Paragraph>
                          ))}
                        </>
                      )}
                    </div>
                  );
                })}
            </Answer>
          )}
          <DictModeModal
            isModalVisible={isModalVisible}
            setIsModalVisible={setIsModalVisible}
            content={content.content}
          />
        </>
      )}

      {status === "error" && <Empty />}
    </div>
  );
};

export default WriteEssay;
