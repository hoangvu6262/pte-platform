import React, { useState, useEffect, useRef } from "react";

import "./styles.scss";

import { useDispatch, useSelector } from "react-redux";
import answerSlices, {
  changeIsSpeakingThunk,
} from "../../../../redux/slices/answer/answerSlice";
import {
  getLessonDetailThunk,
} from "../../../../redux/slices/lesson/lessonSlice";
import { useParams, useLocation } from "react-router-dom";

import FunctionBar from "../../../../container/FunctionBar/FunctionBar";
import Answer from "../../../../container/Answer/Answer";
import PageHeader from "../../../../components/practice/PageHeader/PageHeader";
import DictModeModal from "../../../../components/modal/DictModeModal/DictModeModal";
import Paragraph from "../../../../components/practice/Paragraph/Paragraph";
import SpeakingTimer from "../../../../components/practice/SpeakingTimer/SpeakingTimer";
import AudioRecord from "../../../../components/practice/AudioRecord/AudioRecord";
import useRecorder from "../../../../hooks/useRecorder";
import Tested from "../../../../components/practice/LessonTools/LessonTools";
import Loading from "../../../../components/shared/Loading/Loading";
import { getSkillCode } from "../../../../helpers/practiceHelper";

import { Empty } from "antd";

const ReadAloud = () => {
  const [audioURL, resetAudio, isRecording, startRecording, stopRecording] =
    useRecorder();
  const [havePermissions, setHavePermissions] = useState(true);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isAnswerVisible, setIsAnswerVisible] = useState(false);
  const [isPrepare, setIsPrepare] = useState(true);
  const [toggleAudio, setToggleAudio] = useState(false);
  const audioRef = useRef(new Audio());
  const dispatch = useDispatch();

  const location = useLocation();
  const { zorder } = useParams();
  const { status, content } = useSelector((state) => state.lesson.lessonDetail);
  const submitStatus = useSelector((state) => state.answer.status);


  const handlePlay = () => {
    setToggleAudio(!toggleAudio);
  };

  useEffect(() => {
    if (submitStatus === 1) {
      resetAudio();
      setIsPrepare(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [submitStatus]);

  useEffect(() => {
    window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
    setIsPrepare(true);
    dispatch(answerSlices.actions.redoAnswering());
    dispatch(changeIsSpeakingThunk(true));
  }, [dispatch]);

  useEffect(() => {
    resetAudio();
    setIsPrepare(true);
    audioRef.current?.pause();
    setToggleAudio(false);
    dispatch(answerSlices.actions.redoAnswering());
    dispatch(getLessonDetailThunk({skill: getSkillCode(location.pathname), zorder}));
    window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [zorder]);

  useEffect(() => {
    if (content?.medias?.length > 0) {
      audioRef.current = new Audio(
        content?.medias[0]?.cdnLink
      );
    }
  }, [content]);

  useEffect(() => {
    const audioToggle = () => {
      if (toggleAudio) {
        audioRef.current.currentTime = 0;
        audioRef.current.play();
      } else {
        audioRef.current.pause();
      }
    };
    audioToggle();
    return () => {
      audioRef.current.pause();
    };
  }, [toggleAudio]);

  //open Modal Dictionary
  const openModal = () => {
    setIsModalVisible(true);
  };

  return (
    <div className="read-alouds practice-body">
      <PageHeader
        title="Read Aloud"
        chip="Study Guide"
        type="Speaking"
        idExercise={content.title && status !== "loading" ? content.title : ""}
        content="Look at the text below. In 40 seconds, you must read this text aloud as naturally and clearly as possible. You have 40 seconds to read aloud."
      />
      {status === "loading" && <Loading />}

      {status === "success" && (
        <>
          <div className="practice-timer-n-test">
            <div className="read-alouds__count-down">
              {isPrepare ? (
                <SpeakingTimer
                  time={content.preparationTime}
                  title={"Prepare:"}
                  isPrepare={isPrepare}
                  setIsPrepare={setIsPrepare}
                  isRecording={isRecording}
                  startRecording={startRecording}
                  stopRecording={stopRecording}
                  keyTimer="prepare"
                  havePermissions={havePermissions}
                />
              ) : (
                <SpeakingTimer
                  time={content.duration}
                  title={"Time:"}
                  isPrepare={isPrepare}
                  isRecording={isRecording}
                  startRecording={startRecording}
                  stopRecording={stopRecording}
                  setIsPrepare={setIsPrepare}
                  keyTimer="time"
                />
              )}
            </div>
            <Tested
              lessonId={content.id}
              openModal={openModal}
              priority={
                content?.priorities?.length > 0 &&
                content.priorities[0].priority
              }
            />
          </div>

          <div className="read-alouds__paragraph">
            {content?.lessonMediaDTOs?.length > 0 && (
              <div className={`read-alouds__play-audio`} onClick={handlePlay}>
                {toggleAudio ? (
                  <i className="fa-solid fa-circle-play"></i>
                ) : (
                  <i className="fa-regular fa-circle-play"></i>
                )}
              </div>
            )}
            <Paragraph content={content?.content} />
          </div>

          <AudioRecord
            setIsPrepare={setIsPrepare}
            isRecording={isRecording}
            startRecording={startRecording}
            stopRecording={stopRecording}
            havePermissions={havePermissions}
            audioURL={audioURL}
            setHavePermissions={setHavePermissions}
          />
          <FunctionBar
            toggleAnswer={() => setIsAnswerVisible(!isAnswerVisible)}
            openModal={() => {
              openModal();
            }}
          />
          {isAnswerVisible && (
            <Answer explanation={content.explanation}>
              {content?.questionGroup?.questions &&
                content?.questionGroup?.questions.map((ques) => {
                  return (
                    <div key={ques.id}>
                      {ques.questionSolutions && (
                        <>
                          {ques.questionSolutions.map((solution) => (
                            <Paragraph
                              key={solution.valueText}
                              content={
                                (solution.valueText &&
                                  (solution.valueText.charAt(0) === "#"
                                    ? ""
                                    : solution.valueText + ".")) +
                                (solution.explanation
                                  ? solution.explanation
                                  : "")
                              }
                            ></Paragraph>
                          ))}
                        </>
                      )}
                    </div>
                  );
                })}
            </Answer>
          )}
          <DictModeModal
            isModalVisible={isModalVisible}
            setIsModalVisible={setIsModalVisible}
            content={content.content}
          />
        </>
      )}
      {status === "error" && <Empty />}
    </div>
  );
};

export default ReadAloud;
