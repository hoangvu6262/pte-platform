import React, { useState, useEffect } from "react";

import "./styles.scss";

import { useDispatch, useSelector } from "react-redux";
import answerSlices, {
  changeIsSpeakingThunk,
} from "../../../../redux/slices/answer/answerSlice";
import {
  getLessonDetailThunk,
} from "../../../../redux/slices/lesson/lessonSlice";

import { useParams, useLocation } from "react-router-dom";

import FunctionBar from "../../../../container/FunctionBar/FunctionBar";
import Answer from "../../../../container/Answer/Answer";
import PageHeader from "../../../../components/practice/PageHeader/PageHeader";
import DictModeModal from "../../../../components/modal/DictModeModal/DictModeModal";
import SpeakingTimer from "../../../../components/practice/SpeakingTimer/SpeakingTimer";
import AudioRecord from "../../../../components/practice/AudioRecord/AudioRecord";
import useRecorder from "../../../../hooks/useRecorder";
import Tested from "../../../../components/practice/LessonTools/LessonTools";
import Loading from "../../../../components/shared/Loading/Loading";
import { getSkillCode } from "../../../../helpers/practiceHelper";

import { Empty } from "antd";

const DescribeImage = () => {
  const [audioURL, resetAudio, isRecording, startRecording, stopRecording] =
    useRecorder();

  const [havePermissions, setHavePermissions] = useState(true);

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isAnswerVisible, setIsAnswerVisible] = useState(false);
  const [isPrepare, setIsPrepare] = useState(true);

  const {pathname} = useLocation();
  const { zorder } = useParams();

  const submitStatus = useSelector((state) => state.answer.status);

  const dispatch = useDispatch();

  const { status, content } = useSelector((state) => state.lesson.lessonDetail);

  useEffect(() => {
    setIsPrepare(true);
    dispatch(changeIsSpeakingThunk(true));
    dispatch(answerSlices.actions.redoAnswering());
    window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
  }, [dispatch]);

  useEffect(() => {
    if (submitStatus === 1) {
      setIsPrepare(true);
    }
  }, [submitStatus]);

  useEffect(() => {
    resetAudio();
    setIsPrepare(true);
    dispatch(answerSlices.actions.redoAnswering());
    dispatch(getLessonDetailThunk({skill: getSkillCode(pathname), zorder}));

    window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [zorder]);

  //open Modal Dictionary
  const openModal = () => {
    setIsModalVisible(true);
  };

  return (
    <div className="describe-image practice-body">
      <PageHeader
        title="Describe Image"
        chip="Study Guide"
        type="Speaking"
        idExercise={content.title && status === "success" ? content.title : ""}
        content="Look at the graph below. In 25 seconds, please speak into the microphone and describe in detail what the graph is showing. You will have 40 seconds to give your response."
      />

      {status === "loading" && <Loading />}

      {status === "success" && (
        <>
          <div className="practice-timer-n-test">
            <div className="describe-image__count-down">
              {isPrepare ? (
                <SpeakingTimer
                  time={content.preparationTime}
                  title={"Prepare:"}
                  isPrepare={isPrepare}
                  isRecording={isRecording}
                  startRecording={startRecording}
                  stopRecording={stopRecording}
                  setIsPrepare={setIsPrepare}
                  keyTimer="prepare"
                  havePermissions={havePermissions}
                />
              ) : (
                <SpeakingTimer
                  time={content.duration}
                  title={"Time:"}
                  isPrepare={isPrepare}
                  isRecording={isRecording}
                  startRecording={startRecording}
                  stopRecording={stopRecording}
                  setIsPrepare={setIsPrepare}
                  keyTimer="time"
                />
              )}
            </div>
            <Tested
              lessonId={content.id}
              openModal={openModal}
              priority={
                content?.priorities?.length > 0 &&
                content.priorities[0].priority
              }
            />
          </div>
          <div className="describe-image__image">
            <img
              src={content.medias[0]?.cdnLink}
              alt="Describe img"
            />
          </div>
          <AudioRecord
            setIsPrepare={setIsPrepare}
            isRecording={isRecording}
            startRecording={startRecording}
            stopRecording={stopRecording}
            audioURL={audioURL}
            havePermissions={havePermissions}
            setHavePermissions={setHavePermissions}
          />
          <FunctionBar
            toggleAnswer={() => setIsAnswerVisible(!isAnswerVisible)}
            openModal={() => {
              openModal();
            }}
          />
          {isAnswerVisible && (
            <Answer
              answer={
                content?.questionGroup?.questions[0]?.questionSolutions[0]
                  ?.explanation
              }
              explanation={content.explanation}
            ></Answer>
          )}
          <DictModeModal
            isModalVisible={isModalVisible}
            setIsModalVisible={setIsModalVisible}
            content={
              content?.questionGroup?.questions[0]?.questionSolutions[0]
                ?.explanation
            }
          />
        </>
      )}

      {status === "error" && <Empty />}
    </div>
  );
};

export default DescribeImage;
