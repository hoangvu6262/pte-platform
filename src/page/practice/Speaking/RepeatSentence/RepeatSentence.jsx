import React, { useState, useEffect } from "react";

import "./styles.scss";

import { useDispatch, useSelector } from "react-redux";
import answerSlices, {
  changeIsSpeakingThunk,
} from "../../../../redux/slices/answer/answerSlice";
import {
  getLessonDetailThunk,
} from "../../../../redux/slices/lesson/lessonSlice";

import { useParams, useLocation } from "react-router-dom";

import FunctionBar from "../../../../container/FunctionBar/FunctionBar";
import Answer from "../../../../container/Answer/Answer";
import PageHeader from "../../../../components/practice/PageHeader/PageHeader";
import DictModeModal from "../../../../components/modal/DictModeModal/DictModeModal";
import CustomAudio from "../../../../components/practice/CustomAudio/CustomAudio";
import SpeakingTimer from "../../../../components/practice/SpeakingTimer/SpeakingTimer";
import AudioRecord from "../../../../components/practice/AudioRecord/AudioRecord";
import useRecorder from "../../../../hooks/useRecorder";
import Tested from "../../../../components/practice/LessonTools/LessonTools";
import Loading from "../../../../components/shared/Loading/Loading";

import { Empty } from "antd";

const RepeatSentence = () => {
  const [audioURL, resetAudio, isRecording, startRecording, stopRecording] =
    useRecorder();

  const [havePermissions, setHavePermissions] = useState(true);

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isAnswerVisible, setIsAnswerVisible] = useState(false);
  const [isPrepare, setIsPrepare] = useState(true);
  const [isPlaying, setIsPlaying] = useState(false);

  const dispatch = useDispatch();

  const location = useLocation()

  const { zorder} = useParams();
  const { status, content } = useSelector((state) => state.lesson.lessonDetail);

  const submitStatus = useSelector((state) => state.answer.status);

  useEffect(() => {
    if (submitStatus === 1) {
      setIsPrepare(true);
    }
  }, [submitStatus])

  useEffect(() => {
    window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
    setIsPrepare(true);
    dispatch(answerSlices.actions.redoAnswering());
    dispatch(changeIsSpeakingThunk(true));
  }, [dispatch]);

  

  useEffect(() => {
    resetAudio();
    setIsPrepare(true);
    setIsAnswerVisible(false);
    dispatch(answerSlices.actions.redoAnswering());
    dispatch(getLessonDetailThunk({skill: location.pathname.split("/")[3], zorder}));
    window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ zorder, dispatch ]);

  //open Modal Dictionary
  const openModal = () => {
    setIsModalVisible(true);
  };

  return (
    <div className="repeat-sentence practice-body">
      <PageHeader
        title="Repeat Sentence"
        chip="Study Guide"
        type="Speaking"
        idExercise={content.title && status !== "loading" ? content.title : ""}
        content="You will hear a sentence. Please repeat the sentence exactly as you hear it. You will hear the sentence only once."
      />

      {status === "loading" && <Loading />}

      {status === "success" && (
        <>
          <div className="practice-timer-n-test">
            <div className="repeat-sentence__count-down">
              {isPrepare ? (
                <SpeakingTimer
                  time={content.preparationTime}
                  title={"Prepare:"}
                  isPrepare={isPrepare}
                  setIsPrepare={setIsPrepare}
                  isRecording={isRecording}
                  startRecording={startRecording}
                  stopRecording={stopRecording}
                  keyTimer="prepare"
                  havePermissions={havePermissions}
                />
              ) : (
                <SpeakingTimer
                  time={content.duration}
                  title={"Time:"}
                  isPrepare={isPrepare}
                  setIsPrepare={setIsPrepare}
                  isRecording={isRecording}
                  startRecording={startRecording}
                  stopRecording={stopRecording}
                  keyTimer="time"
                />
              )}
            </div>
            <Tested
              lessonId={content.id}
              openModal={openModal}
              priority={
                content?.priorities?.length > 0 &&
                content.priorities[0].priority
              }
            />
          </div>
          <CustomAudio
            isRecording={isRecording}
            audioList={content.medias}
            isPlaying={isPlaying}
            setIsPlaying={setIsPlaying}
          />
          <AudioRecord
            setIsPrepare={setIsPrepare}
            isRecording={isRecording}
            startRecording={startRecording}
            stopRecording={stopRecording}
            havePermissions={havePermissions}
            audioURL={audioURL}
            setHavePermissions={setHavePermissions}
          />
          <FunctionBar
            toggleAnswer={() => setIsAnswerVisible(!isAnswerVisible)}
            openModal={() => {
              openModal();
            }}
          />
          {isAnswerVisible && (
            <Answer
              transcript={content.content}
              explanation={content.explanation}
            >
              {content?.questionGroup?.questions &&
                content?.questionGroup?.questions.map((ques) => {
                  return (
                    <p key={ques.id}>
                      {ques.questionSolutions && (
                        <p>
                          <p style={{ fontWeight: "500" }}>Answer:</p>
                          {ques.questionSolutions.map((solution) => (
                            <p key={solution.valueText}>
                              {solution.valueText &&
                                (solution.valueText.charAt(0) === "#"
                                  ? ""
                                  : solution.valueText + ".")}{" "}
                              {solution.explanation}
                            </p>
                          ))}
                        </p>
                      )}
                    </p>
                  );
                })}
            </Answer>
          )}
          <DictModeModal
            isModalVisible={isModalVisible}
            setIsModalVisible={setIsModalVisible}
            content={content.content}
          />
        </>
      )}

      {status === "error" && <Empty />}
    </div>
  );
};

export default RepeatSentence;
