import ReadAloud from "./ReadAloud/ReadAloud";
import RepeatSentence from "./RepeatSentence/RepeatSentence";
import DescribeImage from "./DescribeImage/DescribeImage";
import ReTellLecture from "./ReTellLecture/ReTellLecture";
import AnswerShortQuestion from "./AnswerShortQuestion/AnswerShortQuestion";

export {
  ReadAloud,
  RepeatSentence,
  DescribeImage,
  ReTellLecture,
  AnswerShortQuestion,
};
