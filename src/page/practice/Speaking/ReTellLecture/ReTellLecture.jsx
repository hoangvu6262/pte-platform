import React, { useState, useEffect } from "react";

import "./styles.scss";

import { useDispatch, useSelector } from "react-redux";
import answerSlices, {
  changeIsSpeakingThunk,
} from "../../../../redux/slices/answer/answerSlice";
import {
  getLessonDetailThunk,
} from "../../../../redux/slices/lesson/lessonSlice";
import { useLocation, useParams } from "react-router-dom";

import FunctionBar from "../../../../container/FunctionBar/FunctionBar";
import Answer from "../../../../container/Answer/Answer";
import PageHeader from "../../../../components/practice/PageHeader/PageHeader";
import DictModeModal from "../../../../components/modal/DictModeModal/DictModeModal";
import CustomAudio from "../../../../components/practice/CustomAudio/CustomAudio";
import SpeakingTimer from "../../../../components/practice/SpeakingTimer/SpeakingTimer";
import AudioRecord from "../../../../components/practice/AudioRecord/AudioRecord";
import useRecorder from "../../../../hooks/useRecorder";
import Tested from "../../../../components/practice/LessonTools/LessonTools";
import Loading from "../../../../components/shared/Loading/Loading";
import { getSkillCode } from "../../../../helpers/practiceHelper";

import { Empty } from "antd";

const ReTellLecture = () => {
  const [audioURL, resetAudio, isRecording, startRecording, stopRecording] =
    useRecorder();

  const [havePermissions, setHavePermissions] = useState(true);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isAnswerVisible, setIsAnswerVisible] = useState(false);
  const [isPrepare, setIsPrepare] = useState(true);
  const [isPlaying, setIsPlaying] = useState(false);

  const {pathname} = useLocation()
  const { zorder} = useParams();
  const { status, content } = useSelector((state) => state.lesson.lessonDetail);
  const submitStatus = useSelector((state) => state.answer.status);

  const dispatch = useDispatch();

  useEffect(() => {
    window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
    setIsPrepare(true);
    dispatch(answerSlices.actions.redoAnswering());
    dispatch(changeIsSpeakingThunk(true));
  }, [dispatch]);

  useEffect(() => {
    resetAudio();
    setIsPrepare(true);
    dispatch(answerSlices.actions.redoAnswering());
    setIsAnswerVisible(false);
    dispatch(getLessonDetailThunk({skill: getSkillCode(pathname), zorder}));
    window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [zorder]);

  useEffect(() => {
    if (submitStatus === 1) {
      setIsPrepare(true);
    }
  }, [submitStatus]);

  //open Modal Dictionary
  const openModal = () => {
    setIsModalVisible(true);
  };

  return (
    <div className="re-tell-lecture practice-body">
      <PageHeader
        title="Re-tell Lecture"
        chip="Study Guide"
        type="Speaking"
        linkGuide="/intro/r1"
        idExercise={content.title && status !== "loading" ? content.title : ""}
        content="You will hear a lecture. After listening to the lecture, in 10 seconds, please speak into the microphone and retell what you have just heard from the lecture in your own words. You will have 40 seconds to give your response."
      />

      {status === "loading" && <Loading />}

      {status === "success" && (
        <>
          <div className="practice-timer-n-test">
            <div className="re-tell-lecture__count-down">
              {isPrepare ? (
                <SpeakingTimer
                  time={content.preparationTime}
                  title={"Prepare:"}
                  isPrepare={isPrepare}
                  setIsPrepare={setIsPrepare}
                  isRecording={isRecording}
                  startRecording={startRecording}
                  stopRecording={stopRecording}
                  keyTimer="prepare"
                  havePermissions={havePermissions}
                />
              ) : (
                <SpeakingTimer
                  time={content.duration}
                  title={"Time:"}
                  isPrepare={isPrepare}
                  setIsPrepare={setIsPrepare}
                  isRecording={isRecording}
                  startRecording={startRecording}
                  stopRecording={stopRecording}
                  keyTimer="time"
                />
              )}
            </div>
            <Tested
              lessonId={content.id}
              openModal={openModal}
              priority={
                content?.priorities?.length > 0 &&
                content.priorities[0].priority
              }
            />
          </div>
          <CustomAudio
            isRecording={isRecording}
            audioList={content.medias}
            isPlaying={isPlaying}
            setIsPlaying={setIsPlaying}
          />
          <AudioRecord
            setIsPrepare={setIsPrepare}
            isRecording={isRecording}
            startRecording={startRecording}
            stopRecording={stopRecording}
            havePermissions={havePermissions}
            audioURL={audioURL}
            setHavePermissions={setHavePermissions}
          />
          <FunctionBar
            toggleAnswer={() => setIsAnswerVisible(!isAnswerVisible)}
            openModal={() => {
              openModal();
            }}
          />
          {isAnswerVisible && (
            <Answer
              transcript={content.content}
              explanation={content.explanation}
            >
              {content?.questionGroup?.questions &&
                content?.questionGroup?.questions.map((ques) => {
                  return (
                    <p key={ques.id}>
                      {ques.questionSolutions && (
                        <p>
                          <p style={{ fontWeight: "500" }}>Answer:</p>
                          {ques.questionSolutions.map((solution) => (
                            <p key={solution.valueText}>
                              {solution.valueText &&
                                (solution.valueText.charAt(0) === "#"
                                  ? ""
                                  : solution.valueText + ".")}{" "}
                              {solution.explanation}
                            </p>
                          ))}
                        </p>
                      )}
                    </p>
                  );
                })}
            </Answer>
          )}
          <DictModeModal
            isModalVisible={isModalVisible}
            setIsModalVisible={setIsModalVisible}
            content={content.content}
          />
        </>
      )}

      {status === "error" && <Empty />}
    </div>
  );
};

export default ReTellLecture;
