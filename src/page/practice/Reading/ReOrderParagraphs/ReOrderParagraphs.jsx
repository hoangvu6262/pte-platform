import React, { useState, useEffect } from "react";

import "./styles.scss";

import {
  closestCenter,
  DndContext,
  PointerSensor,
  useSensor,
} from "@dnd-kit/core";
import {
  arrayMove,
  SortableContext,
  verticalListSortingStrategy,
} from "@dnd-kit/sortable";

import { useDispatch, useSelector } from "react-redux";
import answerSlices, {
  changeIsSpeakingThunk,
} from "../../../../redux/slices/answer/answerSlice";
import {
  getLessonDetailThunk,
} from "../../../../redux/slices/lesson/lessonSlice";
import { useLocation, useParams } from "react-router-dom";

import SortableItem from "../../../../container/SortableItems/SortableItems";
import PageHeader from "../../../../components/practice/PageHeader/PageHeader";
import FunctionBar from "../../../../container/FunctionBar/FunctionBar";
import Answer from "../../../../container/Answer/Answer";
import Timer from "../../../../components/practice/Timer/Timer";
import Tested from "../../../../components/practice/LessonTools/LessonTools";
import Loading from "../../../../components/shared/Loading/Loading";
import DictModeModal from "../../../../components/modal/DictModeModal/DictModeModal";
import Paragraph from "../../../../components/practice/Paragraph/Paragraph";

import { Empty } from "antd";

import { shuffleArr } from "../../../../utils";
import { getSkillCode } from "../../../../helpers/practiceHelper";

const ReOrderParagraphs = () => {
  const [items, setItems] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isAnswerVisible, setIsAnswerVisible] = useState(false);
  const [questions, setQuestions] = useState([]);
  const dispatch = useDispatch();

  const { zorder } = useParams();
  const {pathname} = useLocation()

  const { status, content } = useSelector((state) => state.lesson.lessonDetail);
  const submitStatus = useSelector((state) => state.answer.status);

  const sensors = [useSensor(PointerSensor)];

  useEffect(() => {
    window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
    dispatch(changeIsSpeakingThunk(false));
    setIsAnswerVisible(false);
  }, [dispatch]);

  useEffect(() => {
    dispatch(getLessonDetailThunk({skill: getSkillCode(pathname), zorder}));
    dispatch(answerSlices.actions.redoAnswering());
    window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [zorder]);

  useEffect(() => {
    if (content?.questionGroup?.questions) {
      setQuestions(content?.questionGroup?.questions);
      setItems(mappedReorder(content?.questionGroup?.questions));
    }
  }, [content]);

  useEffect(() => {
    if (submitStatus === 3) {
      setIsAnswerVisible(true);
    } else if (submitStatus === 1) {
      setIsAnswerVisible(false);
      if (content?.questionGroup?.questions) {
        setItems(mappedReorder(content?.questionGroup?.questions));
      }
    }
  }, [submitStatus, content]);

  useEffect(() => {
    const listAnswer = [...items];

    dispatch(
      answerSlices.actions.updateAnswering(
        listAnswer.map((ans, index) => {
          const filteredQues = questions.filter(
            (ques) => ques.name === ans.content
          )[0];
          return {
            question: {
              id: filteredQues.id,
              name: filteredQues.name,
            },
            questionResponseUsers: [{ valueText: `#${index + 1}#` }],
          };
        })
      )
    );
  }, [dispatch, items, questions]);

  const mappedReorder = (questions) => {
    questions = shuffleArr(questions);
    const listItem = questions.map((ques, index) => {
      return {
        id: index + 1,
        content: ques.name,
      };
    });
    return listItem;
  };
  
  const reorderSolutions = (questions) => {
    const listOrder = [];
    questions.forEach((ques) => {
      listOrder[ques.questionSolutions[0].valueText[1] - 1] = ques.name;
    });
    return listOrder;
  };

  const handleDragEnd = ({ active, over }) => {
    if (active.id !== over.id) {
      setItems((items) => {
        const oldIndex = items.findIndex((item) => item.id === active.id);
        const newIndex = items.findIndex((item) => item.id === over.id);

        return arrayMove(items, oldIndex, newIndex);
      });
    }
  };

  const renderItems = () => {
    return items.map((item) => {
      return (
        <SortableItem item={item} key={item.id} delay={1000} distance={1} />
      );
    });
  };

  //open Modal Dictionary
  const openModal = () => {
    setIsModalVisible(true);
  };

  

  

  return (
    <div className="practice-body re-order-paragraph">
      <PageHeader
        title="Re-Order Paragraphs"
        chip="Study Guide"
        type="Reading"
        idExercise={content.title && status === "success" ? content.title : ""}
        content="The text boxes in the left panel have been placed in a random order. Restore the original order by dragging the text boxes from the left panel to the right panel.
        "
      />
      {status === "loading" && <Loading />}
      {status === "success" && (
        <>
          <div className="practice-timer-n-test">
            <Timer
              time={content.duration}
              title="Time: "
              name="Time: "
              overtime={true}
            />
            <Tested
              lessonId={content.id}
              openModal={openModal}
              priority={
                content?.priorities?.length > 0 &&
                content.priorities[0].priority
              }
            />
          </div>
          <DndContext
            sensors={sensors}
            collisionDetection={closestCenter}
            onDragEnd={handleDragEnd}
          >
            <SortableContext
              items={items.map((item) => item.id)}
              strategy={verticalListSortingStrategy}
            >
              {renderItems()}
            </SortableContext>
          </DndContext>
          <FunctionBar
            isAnswerVisible={isAnswerVisible}
            toggleAnswer={() => setIsAnswerVisible(!isAnswerVisible)}
            openModal={() => {
              openModal();
            }}
          />
          {isAnswerVisible && (
            <Answer explanation={content.explanation}>
              {reorderSolutions(content?.questionGroup?.questions).map(
                (sol, index) => {
                  return (
                    <p key={index}>
                      {" "}
                      {index + 1}. {sol}
                    </p>
                  );
                }
              )}
            </Answer>
          )}
          <DictModeModal
            isModalVisible={isModalVisible}
            setIsModalVisible={setIsModalVisible}
          >
            <section className="dict-mode-modal__section">
              <h3>Choices: </h3>
              <div className="dict-mode-modal__section--choices">
                {content?.questionGroup?.questions?.map((item, index) => (
                  <Paragraph
                    content={index + 1 + ". " + item.name}
                    key={item.id}
                  />
                ))}
              </div>
            </section>
          </DictModeModal>
        </>
      )}
      {status === "error" && <Empty />}
    </div>
  );
};

export default ReOrderParagraphs;
