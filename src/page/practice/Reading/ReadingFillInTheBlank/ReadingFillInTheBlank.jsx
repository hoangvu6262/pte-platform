import React, { useState, useEffect } from "react";

import "./styles.scss";

import { useSelector, useDispatch } from "react-redux";

import {
  getLessonDetailThunk,
} from "../../../../redux/slices/lesson/lessonSlice";

import answerSlices, {
  changeIsSpeakingThunk,
} from "../../../../redux/slices/answer/answerSlice";
import { useLocation, useParams } from "react-router-dom";

import FunctionBar from "../../../../container/FunctionBar/FunctionBar";
import Answer from "../../../../container/Answer/Answer";
import Dnd from "../../../../container/DnDKit/DnDKit";
import PageHeader from "../../../../components/practice/PageHeader/PageHeader";
import DictModeModal from "../../../../components/modal/DictModeModal/DictModeModal";
import Timer from "../../../../components/practice/Timer/Timer";
import Tested from "../../../../components/practice/LessonTools/LessonTools";
import Loading from "../../../../components/shared/Loading/Loading";

import { Empty } from "antd";

import { renderParagraph } from "../../../../utils";
import {
  filteredResult,
  dictParagraphEmbedded,
} from "../../../../utils/practice";
import { getSkillCode } from "../../../../helpers/practiceHelper";

/**
 * Blank: $$(<idQues>)//<answer>**
 */

const filteredParagraph = (paragraph, listQues) => {
  listQues.map((ques) => {
    if (ques.code && paragraph) {
      let blankValue =
        `$$$(${ques.code[1]})//` + ques.questionSolutions[0].valueText;
      paragraph = paragraph?.replace(ques.code, blankValue);
      return blankValue;
    }
    return "";
  });
  return paragraph;
};

const filteredWrongAnswer = (questions) => {
  let filteredOption = [];
  let filteredSolution = [];

  questions.forEach((ques) => {
    filteredOption.push(...ques.questionOptions);
    filteredSolution.push(...ques.questionSolutions);
  });

  filteredOption = filteredOption.map((opt) => opt.name);
  filteredSolution = filteredSolution.map((sol) => sol.valueText);

  const wrongAnswer = filteredOption.filter((filter) => {
    return !filteredSolution.includes(filter);
  });
  return wrongAnswer;
};

const ReadingFillInTheBlank = () => {
  const { zorder } = useParams();
  const {pathname} = useLocation()
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isAnswerVisible, setIsAnswerVisible] = useState(false);

  const { status, content } = useSelector((state) => state.lesson.lessonDetail);
  const dispatch = useDispatch();

  const openModal = () => {
    setIsModalVisible(true);
  };

  useEffect(() => {
    dispatch(getLessonDetailThunk({skill: getSkillCode(pathname), zorder}));
    dispatch(answerSlices.actions.redoAnswering());
    window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [zorder]);

  useEffect(() => {
    setIsAnswerVisible(false);
    dispatch(changeIsSpeakingThunk(false));
    dispatch(answerSlices.actions.redoAnswering());
  }, [dispatch]);

  return (
    <div>
      <div className="practice-body reading">
        <PageHeader
          title="Reading: Fill in the Blanks"
          chip="Study Guide"
          type="Reading"
          idExercise={
            content.title && status === "success" ? content.title : ""
          }
          content="In the text below some words are missing. Drag words from the box below to the appropriate place in the text. To undo an answer choice, drag the word back to the box below the text."
        />
        {status === "loading" && <Loading />}
        {status === "success" && (
          <>
            <div className="practice-timer-n-test">
              <Timer
                time={content.duration}
                title="Time: "
                name="Time: "
                overtime={true}
              />
              <Tested
                lessonId={content.id}
                openModal={openModal}
                priority={
                  content?.priorities?.length > 0 &&
                  content.priorities[0].priority
                }
              />
            </div>

            <div className="practice-body__paragraph reading-paragraph">
              <Dnd
                taskId="dnd-1"
                wrongAnswers={filteredWrongAnswer(
                  content?.questionGroup?.questions
                )}
                isAnswerVisible={isAnswerVisible}
                setIsAnswerVisible={setIsAnswerVisible}
                questions={content?.questionGroup?.questions}
              >
                {renderParagraph(
                  filteredParagraph(
                    content.content,
                    content?.questionGroup?.questions
                  )
                )}
              </Dnd>
            </div>

            <FunctionBar
              isAnswerVisible={isAnswerVisible}
              toggleAnswer={() => setIsAnswerVisible(!isAnswerVisible)}
              openModal={() => {
                openModal();
              }}
            />
            {isAnswerVisible && (
              <Answer
                answer={filteredResult(content?.questionGroup?.questions)}
                explanation={content.explanation}
              ></Answer>
            )}
            <DictModeModal
              isModalVisible={isModalVisible}
              setIsModalVisible={setIsModalVisible}
              content={dictParagraphEmbedded(
                content.content,
                content?.questionGroup?.questions
              )}
            />
          </>
        )}
        {status === "error" && <Empty />}
      </div>
    </div>
  );
};

export default ReadingFillInTheBlank;
