import MultipleChoice from "./MultipleChoice/MultipleChoice";
import SingleChoice from "./SingleChoice/SingleChoice";
import ReadingAndWriting from "./ReadingAndWriting/ReadingAndWriting";
import ReOrderParagraphs from "./ReOrderParagraphs/ReOrderParagraphs";
import ReadingFillInTheBlank from "./ReadingFillInTheBlank/ReadingFillInTheBlank";

export {
  MultipleChoice,
  SingleChoice,
  ReadingAndWriting,
  ReOrderParagraphs,
  ReadingFillInTheBlank,
};
