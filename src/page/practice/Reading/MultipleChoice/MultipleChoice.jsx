import React, { useState, useEffect } from "react";

import "./styles.scss";
import { Checkbox, Empty } from "antd";

import { useSelector, useDispatch } from "react-redux";
import {
  getLessonDetailThunk,
} from "../../../../redux/slices/lesson/lessonSlice";

import answerSlices, {
  changeIsSpeakingThunk,
} from "../../../../redux/slices/answer/answerSlice";

import { useParams, useLocation } from "react-router-dom";

import FunctionBar from "../../../../container/FunctionBar/FunctionBar";
import Answer from "../../../../container/Answer/Answer";
import PageHeader from "../../../../components/practice/PageHeader/PageHeader";
import DictModeModal from "../../../../components/modal/DictModeModal/DictModeModal";
import Paragraph from "../../../../components/practice/Paragraph/Paragraph";
import Timer from "../../../../components/practice/Timer/Timer";
import Tested from "../../../../components/practice/LessonTools/LessonTools";
import Loading from "../../../../components/shared/Loading/Loading";

import { shuffleArr } from "../../../../utils";
import { getSkillCode } from "../../../../helpers/practiceHelper";

const MultipleChoice = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isAnswerVisible, setIsAnswerVisible] = useState(false);
  const [checkList, setCheckList] = useState([]);
  const [options, setOptions] = useState([]);
  const [result, setResult] = useState([]);
  const [questions, setQuestions] = useState([]);

  const dispatch = useDispatch();

  const { zorder } = useParams();
  const {pathname} = useLocation();

  const { status, content } = useSelector((state) => state.lesson.lessonDetail);

  const submitStatus = useSelector((state) => state.answer.status);

  const openModal = () => {
    setIsModalVisible(true);
  };

  const handleCheckbox = (value) => {
    setCheckList(value);
  };

  const handleRedo = () => {
    dispatch(answerSlices.actions.redoAnswering());
    setCheckList([]);
  };

  const checkRightAnswer = (value) => {
    let status = "";
    if (result.includes(value) && checkList.includes(value)) {
      status = "right";
    } else if (checkList.includes(value) && !result.includes(value)) {
      status = "wrong";
    } else if (result.includes(value)) {
      status = "true";
    }
    return status;
  };

  useEffect(() => {
    dispatch(answerSlices.actions.redoAnswering());
    dispatch(changeIsSpeakingThunk(false));
  }, [dispatch]);

  useEffect(() => {
    dispatch(getLessonDetailThunk({skill: getSkillCode(pathname), zorder}));
    dispatch(answerSlices.actions.redoAnswering());
    window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [zorder]);

  useEffect(() => {
    if (submitStatus === 1) {
      handleRedo();
      setOptions(shuffleArr(options));
      setIsAnswerVisible(false);
    } else if (submitStatus === 2) {
      setIsAnswerVisible(false);
    } else if (submitStatus === 3) {
      setIsAnswerVisible(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [submitStatus]);

  useEffect(() => {
    const listOption = content?.questionGroup?.questions[0]?.questionOptions;
    const result = content?.questionGroup?.questions[0]?.questionSolutions.map(
      (sol) => sol.valueText
    );
    if (content?.questionGroup?.isShuffle) {
      setOptions(shuffleArr(listOption));
    } else {
      setOptions(listOption);
    }
    setResult(result);
    setQuestions(content?.questionGroup?.questions);
  }, [content]);

  useEffect(() => {
    const mappedChecklist = checkList.map((check) => {
      return {
        valueText: check,
      };
    });
    let answer = [
      {
        // component: "test",
        question: {
          id: questions && questions[0]?.id,
        },
        questionResponseUsers: mappedChecklist,
      },
    ];

    dispatch(answerSlices.actions.updateAnswering(answer));
  }, [checkList, dispatch, questions]);

  return (
    <div className="practice-body reading">
      <PageHeader
        title="Multiple Choice (Multiple)"
        chip="Study Guide"
        type="Reading"
        idExercise={content.title && status === "success" ? content.title : ""}
        content="Read the text and answer the question by selecting all the correct responses. More than one response is correct."
      />

      {status === "loading" && <Loading />}

      {status === "success" && (
        <>
          <div className="practice-timer-n-test">
            <Timer
              time={content.duration}
              title="Time: "
              name="Time: "
              overtime={true}
            />
            <Tested
              lessonId={content.id}
              openModal={openModal}
              priority={
                content?.priorities?.length > 0 &&
                content.priorities[0].priority
              }
            />
          </div>

          <div className="practice-body__paragraph reading-paragraph">
            <Paragraph content={content.content} />
          </div>
          <Checkbox.Group
            value={checkList}
            disabled={submitStatus === 3}
            onChange={handleCheckbox}
          >
            <p>{content?.questionGroup?.questions[0]?.name}</p>
            {options?.map((option) => (
              <Checkbox
                className={
                  submitStatus === 3 || isAnswerVisible
                    ? checkRightAnswer(option.name)
                    : ""
                }
                value={option.name}
                key={option.id}
              >
                {option.name}
              </Checkbox>
            ))}
          </Checkbox.Group>

          <FunctionBar
            isAnswerVisible={isAnswerVisible}
            toggleAnswer={() => setIsAnswerVisible(!isAnswerVisible)}
            openModal={() => {
              openModal();
            }}
          />
          {isAnswerVisible && (
            <Answer
              answer={result.join(", ")}
              explanation={content.explanation}
            ></Answer>
          )}
          <DictModeModal
            isModalVisible={isModalVisible}
            setIsModalVisible={setIsModalVisible}
            content={content.content}
          >
            <section className="dict-mode-modal__section">
              <h3>Choices: </h3>
              <div className="dict-mode-modal__section--choices">
                {options?.map((option, index) => (
                  <Paragraph
                    content={index + 1 + ". " + option.name}
                    key={option.id}
                  />
                ))}
              </div>
            </section>
          </DictModeModal>
        </>
      )}

      {status === "error" && <Empty />}
    </div>
  );
};

export default MultipleChoice;
