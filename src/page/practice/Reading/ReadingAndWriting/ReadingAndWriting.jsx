/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";

import "./styles.scss";

import { useSelector, useDispatch } from "react-redux";

import {
  getLessonDetailThunk,
} from "../../../../redux/slices/lesson/lessonSlice";
import answerSlices, {
  changeIsSpeakingThunk,
} from "../../../../redux/slices/answer/answerSlice";
import { useParams, useLocation } from "react-router-dom";

import FunctionBar from "../../../../container/FunctionBar/FunctionBar";
import Answer from "../../../../container/Answer/Answer";
import PageHeader from "../../../../components/practice/PageHeader/PageHeader";
import DictModeModal from "../../../../components/modal/DictModeModal/DictModeModal";
import ParagraphBlank from "../../../../components/practice/ParagraphBlank/ParagraphBlank";
import Timer from "../../../../components/practice/Timer/Timer";
import Tested from "../../../../components/practice/LessonTools/LessonTools";
import Loading from "../../../../components/shared/Loading/Loading";

import { Empty } from "antd";
import {
  filteredResult,
  dictParagraphEmbedded,
} from "../../../../utils/practice";
import { getSkillCode, filteredParagraph } from "../../../../helpers/practiceHelper";



const ReadingAndWriting = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isAnswerVisible, setIsAnswerVisible] = useState(false);

  const dispatch = useDispatch();
  const { zorder } = useParams();
  const {pathname} = useLocation();

  const { status, content } = useSelector((state) => state.lesson.lessonDetail);
  const submitStatus = useSelector((state) => state.answer.status);

  useEffect(() => {
    dispatch(changeIsSpeakingThunk(false));
  }, [dispatch]);

  useEffect(() => {
    if (submitStatus === 3) {
      setIsAnswerVisible(true);
    } else if (submitStatus === 1) {
      setIsAnswerVisible(false);
    }
  }, [submitStatus]);

  const openModal = () => {
    setIsModalVisible(true);
  };

  useEffect(() => {
    dispatch(getLessonDetailThunk({skill: getSkillCode(pathname), zorder}));
    dispatch(answerSlices.actions.redoAnswering());
    window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
  }, [zorder]);

  return (
    <div>
      <div className="practice-body listening">
        <PageHeader
          title="Reading & Writing：Fill in the blanks"
          chip="Study Guide"
          type="Reading"
          idExercise={
            content.title && status === "success" ? content.title : ""
          }
          content="There are some words missing in the following text. Please select the correct word in the drop-down box."
        />
        {status === "loading" && <Loading />}
        {status === "success" && (
          <>
            <div className="practice-timer-n-test">
              <Timer
                time={content.duration}
                title="Time: "
                name="Time: "
                overtime={true}
              />
              <Tested
                lessonId={content.id}
                openModal={openModal}
                priority={
                  content?.priorities?.length > 0 &&
                  content.priorities[0].priority
                }
              />
            </div>

            <div className="practice-body__paragraph reading-paragraph">
              <ParagraphBlank
                content={filteredParagraph(
                  content.content,
                  content?.questionGroup?.questions
                )}
                questions={content?.questionGroup?.questions}
                isAnswerVisible={isAnswerVisible}
              />
            </div>

            <FunctionBar
              isAnswerVisible={isAnswerVisible}
              toggleAnswer={() => setIsAnswerVisible(!isAnswerVisible)}
              openModal={() => {
                openModal();
              }}
            />
            {isAnswerVisible && (
              <Answer
                answer={filteredResult(content?.questionGroup?.questions)}
                explanation={content.explanation}
              ></Answer>
            )}
            <DictModeModal
              isModalVisible={isModalVisible}
              setIsModalVisible={setIsModalVisible}
              content={dictParagraphEmbedded(
                content.content,
                content?.questionGroup?.questions
              )}
            />
          </>
        )}
        {status === "error" && <Empty />}
      </div>
    </div>
  );
};

export default ReadingAndWriting;
