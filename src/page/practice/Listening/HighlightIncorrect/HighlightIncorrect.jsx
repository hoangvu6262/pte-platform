import React, { useState, useEffect } from "react";

import "./styles.scss";

import { useSelector, useDispatch } from "react-redux";
import {
  getLessonDetailThunk,
} from "../../../../redux/slices/lesson/lessonSlice";
import answerSlices, {
  changeIsSpeakingThunk,
} from "../../../../redux/slices/answer/answerSlice";

import { useLocation, useParams } from "react-router-dom";

import { Empty } from "antd";

import FunctionBar from "../../../../container/FunctionBar/FunctionBar";
import Answer from "../../../../container/Answer/Answer";
import PageHeader from "../../../../components/practice/PageHeader/PageHeader";
import DictModeModal from "../../../../components/modal/DictModeModal/DictModeModal";
import Timer from "../../../../components/practice/Timer/Timer";
import Tested from "../../../../components/practice/LessonTools/LessonTools";
import CustomAudio from "../../../../components/practice/CustomAudio/CustomAudio";
import ParagraphHighlight from "../../../../components/practice/ParagraphHighlight/ParagraphHighlight";
import Loading from "../../../../components/shared/Loading/Loading";

import { dictParagraphEmbedded } from "../../../../utils/practice";
import { getSkillCode } from "../../../../helpers/practiceHelper";

const filteredParagraph = (paragraph, listQues) => {
  listQues.map((ques) => {
    if (ques.code && ques.questionOptions[0]) {
      let blankValue =
        ques.questionOptions[0].name +
        `//${ques.id}//` +
        ques.questionSolutions[0].valueText;
      paragraph = paragraph.replace(ques.code, blankValue);
      return blankValue;
    }
    return "";
  });
  return paragraph;
};

const SummarizeSpokenText = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isAnswerVisible, setIsAnswerVisible] = useState(false);
  const [isPlaying, setIsPlaying] = useState(true);

  const dispatch = useDispatch();
  const { zorder, code } = useParams();
  const {pathname} = useLocation()

  const { status, content } = useSelector((state) => state.lesson.lessonDetail);

  const submitStatus = useSelector((state) => state.answer.status);

  const openModal = () => {
    setIsModalVisible(true);
  };

  useEffect(() => {
    dispatch(changeIsSpeakingThunk(false));
    dispatch(answerSlices.actions.redoAnswering());
  }, [dispatch]);

  useEffect(() => {
    dispatch(getLessonDetailThunk({skill: getSkillCode(pathname), zorder}));

    dispatch(answerSlices.actions.redoAnswering());
    window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [zorder]);

  useEffect(() => {
    if (submitStatus === 3) {
      setIsAnswerVisible(true);
    } else {
      setIsAnswerVisible(false);
    }
  }, [submitStatus]);

  return (
    <div>
      <div className="practice-body listening">
        <PageHeader
          title="Highlight Incorrect Words"
          chip="Study Guide"
          type="Listening"
          idExercise={
            content.title && status === "success" ? content.title : ""
          }
          content="You will hear a recording. Below is a transcription of the recording. Some words in the transcription differ from what the speaker said. Please click on the words that are different."
        />
        {status === "loading" && <Loading />}
        {status === "success" && (
          <>
            <div className="practice-timer-n-test">
              <Timer
                time={content.duration}
                title="Time: "
                name="Time: "
                overtime={true}
              />
              <Tested
                lessonId={content.id}
                openModal={openModal}
                priority={
                  content?.priorities?.length > 0 &&
                  content.priorities[0].priority
                }
              />
            </div>
            <div className="practice-body__audio">
              <CustomAudio
                audioList={content.medias}
                isPlaying={isPlaying}
                setIsPlaying={setIsPlaying}
              />
            </div>
            <div className="practice-body__body">
              <ParagraphHighlight
                content={filteredParagraph(
                  content.content,
                  content?.questionGroup?.questions
                )}
                questions={content?.questionGroup?.questions}
                isAnswerVisible={isAnswerVisible}
              />
            </div>

            <FunctionBar
              isAnswerVisible={isAnswerVisible}
              toggleAnswer={() => setIsAnswerVisible(!isAnswerVisible)}
              openModal={() => {
                openModal();
              }}
              code={code}
            />
            {isAnswerVisible && (
              <Answer
                transcript={dictParagraphEmbedded(
                  content.content,
                  content?.questionGroup?.questions
                )}
                explanation={content.explanation}
              ></Answer>
            )}
            <DictModeModal
              isModalVisible={isModalVisible}
              setIsModalVisible={setIsModalVisible}
              content={dictParagraphEmbedded(
                content.content,
                content?.questionGroup?.questions
              )}
            />
          </>
        )}
        {status === "error" && <Empty />}
      </div>
    </div>
  );
};

export default SummarizeSpokenText;
