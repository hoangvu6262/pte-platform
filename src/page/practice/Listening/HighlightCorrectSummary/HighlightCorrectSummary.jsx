/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";

import "./styles.scss";
import { Radio } from "antd";

import { useSelector, useDispatch } from "react-redux";
import {
  getLessonDetailThunk,
} from "../../../../redux/slices/lesson/lessonSlice";
import answerSlices, {
  changeIsSpeakingThunk,
} from "../../../../redux/slices/answer/answerSlice";
import { useLocation, useParams } from "react-router-dom";

import FunctionBar from "../../../../container/FunctionBar/FunctionBar";
import Answer from "../../../../container/Answer/Answer";
import PageHeader from "../../../../components/practice/PageHeader/PageHeader";
import DictModeModal from "../../../../components/modal/DictModeModal/DictModeModal";
import Paragraph from "../../../../components/practice/Paragraph/Paragraph";
import Timer from "../../../../components/practice/Timer/Timer";
import Tested from "../../../../components/practice/LessonTools/LessonTools";
import CustomAudio from "../../../../components/practice/CustomAudio/CustomAudio";
import Loading from "../../../../components/shared/Loading/Loading";

import { Empty } from "antd";

import { shuffleArr } from "../../../../utils";
import { getSkillCode } from "../../../../helpers/practiceHelper";

const HighlightCorrectSummary = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isAnswerVisible, setIsAnswerVisible] = useState(false);
  const [radioValue, setRadioValue] = useState();
  const [isPlaying, setIsPlaying] = useState(true);
  const [options, setOptions] = useState([]);
  const [result, setResult] = useState("");
  const [questions, setQuestions] = useState([]);

  const dispatch = useDispatch();

  const { zorder } = useParams();
  const {pathname} = useLocation()

  const submitStatus = useSelector((state) => state.answer.status);
  const { status, content } = useSelector((state) => state.lesson.lessonDetail);

  const openModal = () => {
    setIsModalVisible(true);
  };

  const handleRedo = () => {
    setIsAnswerVisible(false);
    setRadioValue();
    dispatch(answerSlices.actions.redoAnswering());
  };

  const handleCheckbox = (e) => {
    setRadioValue(e.target.value);
  };

  const checkRightAnswer = (value) => {
    let status = "";

    if (result === radioValue && value === radioValue) {
      status = "right";
    } else if (value === radioValue && result !== radioValue) {
      status = "wrong";
    } else if (value === result) {
      status = "true";
    }
    return status;
  };

  useEffect(() => {
    dispatch(changeIsSpeakingThunk(false));
  }, [dispatch]);

  useEffect(() => {
    dispatch(getLessonDetailThunk({skill: getSkillCode(pathname), zorder}));

    dispatch(answerSlices.actions.redoAnswering());
    window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
  }, [zorder]);

  useEffect(() => {
    if (submitStatus === 3) {
      setIsAnswerVisible(true);
    } else if (submitStatus === 1) {
      handleRedo();
      setOptions(shuffleArr(options));
    }
  }, [submitStatus]);

  useEffect(() => {
    const listOption = content?.questionGroup?.questions[0]?.questionOptions;
    const result = content?.questionGroup?.questions[0]?.questionSolutions[0];
    if (content?.questionGroup?.isShuffle) {
      setOptions(shuffleArr(listOption));
    } else {
      setOptions(listOption);
    }
    setResult(result?.valueText);
    setQuestions(content?.questionGroup?.questions);
  }, [content]);

  useEffect(() => {
    if (radioValue) {
      let answer = [
        {
          question: {
            id: questions && questions[0]?.id,
          },
          questionResponseUsers: [{ valueText: radioValue }],
        },
      ];

      dispatch(answerSlices.actions.updateAnswering(answer));
    }
  }, [dispatch, questions, radioValue]);

  return (
    <div>
      <div className="practice-body listening">
        <PageHeader
          title="Highlight Correct Summary"
          chip="Study Guide"
          type="Listening"
          idExercise={
            content.title && status === "success" ? content.title : ""
          }
          content="You will hear a recording. Click on the paragraph that best relates to the recording."
        />

        {status === "loading" && <Loading />}
        {status === "success" && (
          <>
            <div className="practice-timer-n-test">
              <Timer
                time={content.duration}
                title="Time: "
                name="Time: "
                overtime={true}
              />
              <Tested
                lessonId={content.id}
                openModal={openModal}
                priority={
                  content?.priorities?.length > 0 &&
                  content.priorities[0].priority
                }
              />
            </div>
            <div className="practice-body__audio">
              <CustomAudio
                audioList={content.medias}
                isPlaying={isPlaying}
                setIsPlaying={setIsPlaying}
              />
            </div>
            <div className="practice-body__paragraph listening-paragraph">
              <Paragraph content={content?.questionGroup?.questions[0]?.name} />
            </div>
            <Radio.Group
              disabled={submitStatus === 3}
              value={radioValue}
              onChange={handleCheckbox}
            >
              {options?.map((option, index) => (
                <Radio
                  className={
                    submitStatus === 3 || isAnswerVisible
                      ? checkRightAnswer(option.name)
                      : ""
                  }
                  value={option.name}
                  key={option.id}
                >
                  {option.name}
                </Radio>
              ))}
            </Radio.Group>
            <FunctionBar
              isAnswerVisible={isAnswerVisible}
              toggleAnswer={() => setIsAnswerVisible(!isAnswerVisible)}
              openModal={() => {
                openModal();
              }}
            />
            {isAnswerVisible && (
              <Answer
                answer={result}
                transcript={content.content}
                explanation={content.explanation}
              ></Answer>
            )}
            <DictModeModal
              isModalVisible={isModalVisible}
              setIsModalVisible={setIsModalVisible}
              content={content.content}
            >
              <section className="dict-mode-modal__section">
                <h3>Choices: </h3>
                <div className="dict-mode-modal__section--choices">
                  {options?.map((option, index) => (
                    <Paragraph
                      content={index + 1 + ". " + option.name}
                      key={option.id}
                    />
                  ))}
                </div>
              </section>
            </DictModeModal>
          </>
        )}
        {status === "error" && <Empty />}
      </div>
    </div>
  );
};

export default HighlightCorrectSummary;
