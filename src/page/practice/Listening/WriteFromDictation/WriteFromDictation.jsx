import React, { useState, useEffect, useRef } from "react";

import "./styles.scss";
import { Input, Form, Empty } from "antd";

import { useSelector, useDispatch } from "react-redux";
import {
  getLessonDetailThunk,
} from "../../../../redux/slices/lesson/lessonSlice";

import answerSlices, {
  changeIsSpeakingThunk,
} from "../../../../redux/slices/answer/answerSlice";
import { useLocation, useParams } from "react-router-dom";

import FunctionBar from "../../../../container/FunctionBar/FunctionBar";
import Answer from "../../../../container/Answer/Answer";
import PageHeader from "../../../../components/practice/PageHeader/PageHeader";
import DictModeModal from "../../../../components/modal/DictModeModal/DictModeModal";
import Timer from "../../../../components/practice/Timer/Timer";
import Tested from "../../../../components/practice/LessonTools/LessonTools";
import CustomAudio from "../../../../components/practice/CustomAudio/CustomAudio";
import Loading from "../../../../components/shared/Loading/Loading";
import Button from "../../../../components/shared/Button/Button";
import { getSkillCode } from "../../../../helpers/practiceHelper";

const { TextArea } = Input;

const WriteFromDictation = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isAnswerVisible, setIsAnswerVisible] = useState(false);
  const [wordCount, setWordCount] = useState(0);
  const [text, setText] = useState("");
  const [form] = Form.useForm();
  const [isPlaying, setIsPlaying] = useState(true);

  const [questions, setQuestions] = useState([]);

  const dispatch = useDispatch();
  const { zorder } = useParams();
  const {pathname} = useLocation()

  const { status, content } = useSelector((state) => state.lesson.lessonDetail);

  const [isSubmitted, setIsSubmitted] = useState(false);

  const submitStatus = useSelector((state) => state.answer.status);

  const formRef = useRef();

  const openModal = () => {
    setIsModalVisible(true);
  };

  const handleCountForm = (text) => {
    if (text === "") {
      setWordCount(0);
    } else {
      const splittedText = text.trim().split(/\s+/);
      setWordCount(splittedText.filter((word) => word !== " ").length);
      setText(text);
    }
  };
  const handleRedo = () => {
    formRef.current?.resetFields();
    setWordCount(0);
    setIsAnswerVisible(false);
  };

  useEffect(() => {
    handleRedo();
    dispatch(getLessonDetailThunk({skill: getSkillCode(pathname), zorder}));

    setIsAnswerVisible(false);
    dispatch(answerSlices.actions.redoAnswering());
    window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [zorder]);

  useEffect(() => {
    dispatch(changeIsSpeakingThunk(false));
    dispatch(answerSlices.actions.redoAnswering());
  }, [dispatch]);

  useEffect(() => {
    if (submitStatus === 3) {
      setIsSubmitted(true);
    } else if (submitStatus === 1) {
      handleRedo();
      setIsSubmitted(false);
      dispatch(answerSlices.actions.redoAnswering());
    } else {
      setIsSubmitted(false);
    }
  }, [dispatch, submitStatus]);

  useEffect(() => {
    setQuestions(content?.questionGroup?.questions);
  }, [content]);

  useEffect(() => {
    let answer = [
      {
        question: {
          id: questions && questions[0]?.id,
        },
        questionResponseUsers: [
          {
            valueText: text,
          },
        ],
      },
    ];

    dispatch(answerSlices.actions.updateAnswering(answer));
  }, [text, dispatch, questions]);

  return (
    <div>
      <div className="practice-body writing">
        <PageHeader
          title="Write From Dictation"
          chip="Study Guide"
          type="Listening"
          idExercise={
            content.title && status === "success" ? content.title : ""
          }
          content="You will hear a sentence. Type the sentence in the box below exactly as you hear it. Write as much of the sentence as you can. You will hear the sentence only once."
        />
        {status === "loading" && <Loading />}
        {status === "success" && (
          <>
            <div className="practice-timer-n-test">
              <Timer
                time={content.duration}
                title="Time: "
                name="Time: "
                overtime={true}
              />
              <Tested
                lessonId={content.id}
                openModal={openModal}
                priority={
                  content?.priorities?.length > 0 &&
                  content.priorities[0].priority
                }
              />
            </div>
            <div className="practice-body__audio">
              <CustomAudio
                audioList={content.medias}
                isPlaying={isPlaying}
                setIsPlaying={setIsPlaying}
              />
            </div>
            <div className="practice-body__body">
              <Form
                form={form}
                autoComplete="off"
                className={`writing-form ${isSubmitted ? "disabled" : ""}`}
                onChange={(values) => {
                  handleCountForm(values.target.value);
                }}
                disabled={isSubmitted}
                ref={formRef}
              >
                <Form.Item name="writing">
                  <TextArea autoSize={{ minRows: 3 }} />
                </Form.Item>
                {isSubmitted && (
                  <div className="writing-form-redo">
                    <Button
                      outline={true}
                      onClick={() => {
                        dispatch(answerSlices.actions.redoAnswering());
                      }}
                    >
                      Re-do
                    </Button>
                  </div>
                )}
              </Form>
            </div>
            <div className="writing-word-count">Word Count: {wordCount}</div>

            <FunctionBar
              toggleAnswer={() => setIsAnswerVisible(!isAnswerVisible)}
              openModal={() => {
                openModal();
              }}
            />
            {isAnswerVisible && (
              <Answer
                transcript={content.content}
                explanation={content.explanation}
              ></Answer>
            )}
            <DictModeModal
              isModalVisible={isModalVisible}
              setIsModalVisible={setIsModalVisible}
              content={content.content}
            />
          </>
        )}
        {status === "error" && <Empty />}
      </div>
    </div>
  );
};

export default WriteFromDictation;
