/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";

import "./styles.scss";

import { useSelector, useDispatch } from "react-redux";

import {
  getLessonDetailThunk,
} from "../../../../redux/slices/lesson/lessonSlice";

import answerSlices, {
  changeIsSpeakingThunk,
} from "../../../../redux/slices/answer/answerSlice";
import { useLocation, useParams } from "react-router-dom";

import FunctionBar from "../../../../container/FunctionBar/FunctionBar";
import Answer from "../../../../container/Answer/Answer";
import PageHeader from "../../../../components/practice/PageHeader/PageHeader";
import DictModeModal from "../../../../components/modal/DictModeModal/DictModeModal";
import ParagraphBlank from "../../../../components/practice/ParagraphBlank/ParagraphBlank";
import Timer from "../../../../components/practice/Timer/Timer";
import LessonTools from "../../../../components/practice/LessonTools/LessonTools";
import CustomAudio from "../../../../components/practice/CustomAudio/CustomAudio";
import Loading from "../../../../components/shared/Loading/Loading";

import { Empty } from "antd";

import {
  dictParagraphEmbedded,
  filteredResult,
} from "../../../../utils/practice";
import { getSkillCode } from "../../../../helpers/practiceHelper";
/**
 * Blank: **(<idQues>)//<answer>**
 */

const filteredParagraph = (paragraph, listQues) => {
  listQues.map((ques) => {
    if (ques.code && paragraph) {
      let blankValue =
        `**(${ques.code[1]})//` + ques.questionSolutions[0].valueText;
      paragraph = paragraph.replace(ques.code, blankValue);
      return blankValue;
    }
    return undefined;
  });
  return paragraph;
};

const FillBlanks = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isAnswerVisible, setIsAnswerVisible] = useState(false);
  const [isPlaying, setIsPlaying] = useState(true);

  const dispatch = useDispatch();

  const { zorder } = useParams();
  const {pathname} = useLocation()

  const { status, content } = useSelector((state) => state.lesson.lessonDetail);

  const submitStatus = useSelector((state) => state.answer.status);

  const openModal = () => {
    setIsModalVisible(true);
  };

  useEffect(() => {
    setIsPlaying(true);
    dispatch(changeIsSpeakingThunk(false));
    dispatch(answerSlices.actions.redoAnswering());
  }, [dispatch]);

  useEffect(() => {
    setIsPlaying(true);
    dispatch(getLessonDetailThunk({skill: getSkillCode(pathname), zorder}));

    dispatch(answerSlices.actions.redoAnswering());
    window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
  }, [zorder]);

  useEffect(() => {
    if (submitStatus === 3) {
      setIsAnswerVisible(true);
    } else if (submitStatus === 1) {
      setIsAnswerVisible(false);
    }
  }, [submitStatus]);

  return (
    <div>
      <div className="practice-body listening">
        <PageHeader
          title="Fill in the Blanks"
          chip="Study Guide"
          type="Listening"
          idExercise={
            content.title && status === "success" ? content.title : ""
          }
          content="You will hear a recording. Type the missing words in each blank."
        />
        {status === "loading" && <Loading />}
        {status === "success" && (
          <>
            <div className="practice-timer-n-test">
              <Timer
                time={content.duration}
                title="Time: "
                name="Time: "
                overtime={true}
              />
              <LessonTools
                lessonId={content.id}
                openModal={openModal}
                priority={
                  content?.priorities?.length > 0 &&
                  content.priorities[0].priority
                }
              />
            </div>

            <div className="practice-body__audio">
              <CustomAudio
                audioList={content.medias}
                isPlaying={isPlaying}
                setIsPlaying={setIsPlaying}
              />
            </div>

            <div className="practice-body__paragraph listening-paragraph">
              <ParagraphBlank
                content={filteredParagraph(
                  content.content,
                  content?.questionGroup?.questions
                )}
                isAnswerVisible={isAnswerVisible}
                questions={content?.questionGroup?.questions}
              />
            </div>

            <FunctionBar
              isAnswerVisible={isAnswerVisible}
              toggleAnswer={() => setIsAnswerVisible(!isAnswerVisible)}
              openModal={() => {
                openModal();
              }}
            />
            {isAnswerVisible && (
              <Answer
                answer={filteredResult(content?.questionGroup?.questions)}
                transcript={dictParagraphEmbedded(
                  content.content,
                  content?.questionGroup?.questions
                )}
                explanation={content.explanation}
              ></Answer>
            )}
            <DictModeModal
              isModalVisible={isModalVisible}
              setIsModalVisible={setIsModalVisible}
              content={dictParagraphEmbedded(
                content.content,
                content?.questionGroup?.questions
              )}
            />
          </>
        )}
        {status === "error" && <Empty />}
      </div>
    </div>
  );
};

export default FillBlanks;
