export const renderUsername = (userIn4) => {
  if (userIn4.nickName) {
    return userIn4.nickName;
  } else if (userIn4.firstName && userIn4.lastName) {
    return userIn4.firstName + " " + userIn4.lastName;
  } else if (userIn4.firstName) {
    return userIn4.firstName;
  } else if (userIn4.username) {
    return userIn4.username;
  }
};
