import { Blank } from "../container/DnDKit/DnDKit";

import readingLogo from "../asset/img/reading-icon.png";
import writingLogo from "../asset/img/writing-icon.png";
import listeningLogo from "../asset/img/listening-icon.png";
import speakingLogo from "../asset/img/speaking-icon.png";

import { regex } from "../config/contants/regex";

export const defaultContainerStyle = ({ isOverContainer, isCorrect }) => ({
  backgroundColor: isOverContainer
    ? "rgb(235,235,235,1)"
    : typeof isCorrect === "boolean"
    ? isCorrect
      ? "rgba(154, 230, 180, 0.36)" // 0.16 opacity for odyssey dark mode
      : "rgba(254, 178, 178, 0.36)" // 0.16 opacity for odyssey dark mode
    : "rgb(220, 220, 220)",
});

// default sortable transition duration -- use in the cypress integration tests
export const SORTABLE_TRANSITION_DURATION = 200;

export const WORD_BANK = "WORD_BANK";

export class SolutionGetter {
  constructor(defaultUsed = []) {
    // all the answers that have already been used in existing blanks and that have been marked as correct
    this.used = defaultUsed;
  }

  get(solutions) {
    // if answer is NOT correct, or it has no answer, check the solutions for the blank
    // if the blank only has 1 solution, then that's already the correct answer we want
    if (solutions.length === 1) {
      return solutions;
    }

    // if the blank has multiple solutions, we need to pick the one that has NOT been selected as a correct answer already
    const solution = solutions.find(
      (solution) => !this.used.includes(solution)
    );
    this.used.push(solution);
    return [solution];
  }
}

/**
 * Returns a object with blank IDs as keys, and an object as values
 *  Each object has:
 *  - id (the blank's ID)
 *  - isCorrect (boolean indicating if the blank has the correct answer)
 *  - items (an array of strings indicating what is currently in the blank)
 *  - solutions (an array of strings indicating what the correct answers could be for this blank. can have multiple.)
 */
export const getCorrectAnswers = (items) => {
  const entries = Object.entries(items);

  // get all the answers that have already been used in existing blanks and that have been marked as correct
  const alreadyUsedSolutions = entries.reduce(
    (acc, [key, value]) =>
      key !== WORD_BANK && value.isCorrect && value.questionSolutions.length > 1
        ? [...acc, ...value.items]
        : acc,
    []
  );

  const solutionGetter = new SolutionGetter(alreadyUsedSolutions);

  return entries.reduce(
    (acc, [key, value]) =>
      key === WORD_BANK
        ? acc
        : {
            ...acc,
            [key]: {
              ...value,
              isCorrect: true,
              // if answer is already correct, then we don't need to do anything, just keep it as-is
              items: value.isCorrect
                ? value.items
                : solutionGetter.get(value.questionSolutions),
            },
          },
    items
  );
};

export const handleWord = (word) => {
  let leftIndex = 0;
  let rightIndex = word.length - 1;
  let splittedWord = [];

  while (
    (word[leftIndex].match(regex.SPECIAL_CHARACTER) ||
      word[rightIndex].match(regex.SPECIAL_CHARACTER)) &&
    leftIndex < rightIndex
  ) {
    if (word[leftIndex].match(regex.SPECIAL_CHARACTER)) {
      leftIndex++;
    }

    if (word[rightIndex].match(regex.SPECIAL_CHARACTER)) {
      rightIndex--;
    }
  }

  if (rightIndex > leftIndex) {
    splittedWord = [
      word.slice(0, leftIndex),
      word.slice(leftIndex, rightIndex + 1),
      word.slice(rightIndex + 1, word.length),
    ];
  } else {
    splittedWord = [word, "", ""];
  }

  return splittedWord;
};

// render paragraph
export const renderParagraph = (sentence) => {
  return sentence
    ?.split(" ")
    .map((word, index) => {
      if (word) {
        // Check
        if (word.includes("$$")) {
          return (
            <Blank
              // id={word[3]}
              key={index}
              solution={word.split("//")[1] ? word.split("//")[1] : ""}
            />
          );
        } else {
          return word;
        }
      }
      return "";
    })
    .reduce((prev, curr) => [prev, "\xa0", curr]);
};

// render Lesson logo
export const renderLessonLogo = (title) => {
  switch (title) {
    case "Speaking":
      return speakingLogo;
    case "Writing":
      return writingLogo;
    case "Listening":
      return listeningLogo;
    case "Reading":
      return readingLogo;
    default:
      return;
  }
};

// filtered List Category
export const filteredCate = (listCate) => {
  const findChildCate = (code) => {
    return listCate.filter((cate) => cate.parentCode === code);
  };
  let newList = listCate.filter((cate) => cate.parentCode === undefined);
  newList = newList.map((cate) => {
    let listFilteredChild = findChildCate(cate.code);

    listFilteredChild = listFilteredChild.map((fil) => {
      return {
        ...fil,
        listChildCate: findChildCate(fil.code),
      };
    });

    return {
      ...cate,
      listChildCate: listFilteredChild,
    };
  });
  //Check to get COMPONENT category
  return newList;
};

export const shuffleArr = (array) => {
  const newArray = [];
  const usedIndex = [];
  while (newArray.length < array.length) {
    let index = Math.floor(Math.random() * array.length);
    while (usedIndex.includes(index)) {
      index = Math.floor(Math.random() * array.length);
    }
    newArray.push(array[index]);
    usedIndex.push(index);
  }
  return newArray;
};

export const capitalizeLetter = (str) => {
  let newStr = str.toLowerCase();

  return newStr.charAt(0).toUpperCase() + newStr.slice(1);
};

export const getTokenFromCookie = () => {
  if (document.cookie.includes("token=")) {
    const splittedCookie = document.cookie.split("; ");
    const authKeyIndex = splittedCookie.findIndex((cookie) =>
      cookie.includes("token=")
    );

    const token = splittedCookie[authKeyIndex].replace("token=", "");
    return token;
  }
  return "";
};

export const mapPriority = (code, listPriority) => {
  const colorIndex = listPriority.findIndex(
    (mark) => parseInt(mark.code) === parseInt(code)
  );

  if (colorIndex >= 0) {
    return listPriority[colorIndex].categoryValues;
  } else {
    return "rgb(204, 204, 204)";
  }
};
