export const filteredResult = (questions) => {
  const filteredResult = questions.map((ques) => {
    return `${ques.code[1]}. ${ques.questionSolutions[0].valueText}`;
  });
  return filteredResult.join(", ");
};

export const dictParagraphEmbedded = (paragraph, listQues) => {
  listQues.forEach((ques) => {
    if (ques.code && paragraph) {
      paragraph = paragraph.replace(
        ques.code,
        ques.questionSolutions[0]?.valueText
      );
    }
  });
  return paragraph;
};

export const filteredEmbeddedAnswer = (questions, userId) => {
  return questions.map((question) => {
    return {
      question: {
        id: question.id,
      },
      questionResponseUsers: [{ valueText: "" }],
    };
  });
};

export const sortQuestions = (questions) => {
  const listQues = [...questions];
  return listQues?.sort((ques1, ques2) => {
    if (ques1.code > ques2.code) {
      return 1;
    } else {
      return -1;
    }
  });
};

export const handleBlankSpace = (text) => {
  return text.split(" ").join("_");
};

export const breakBlankSpace = (text) => {
  return text.split("_").join(" ");
};

export const checkSubmitDisable = (content) => {
  let isDisabled = true;

  if (content?.length > 0) {
    const listAns = content.map((ques) => ques.questionResponseUsers);
    let isEmpty = true;
    for (let i = 0; i < listAns.length; i++) {
      if (listAns[i].length > 0) {
        for (let j = 0; j < listAns[i].length; j++) {
          if (listAns[i][j].valueText) {
            isEmpty = false;
          }
        }
      }
    }
    isDisabled = isEmpty;
  } else {
    if (content?.questionResponseUsers) {
      if (content.questionResponseUsers[0].valueText) {
        isDisabled = false;
      }
    } else if (content?.speech) {
      isDisabled = false;
    }
  }

  return isDisabled;
};

export const findParentCategory = (parent, child) => {
  const mapped = parent.map((cate) => {
    if (cate.id !== child) {
      const index = cate.listChildCate.find((cate) => cate.id === child);
      if (index) {
        return cate.name.toLowerCase();
      } else {
        return null;
      }
    } else {
      return cate.name.toLowerCase();
    }
  });
  return mapped.find((item) => item !== null);
};
