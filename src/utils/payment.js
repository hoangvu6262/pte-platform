export const getListPlanFromlocals = (locale, listPlan) => {
  const ListFilterPlan = listPlan
    .filter(
      //get by locale code
      (plan) => plan.code.at(-1) === locale
    )
    .sort((a, b) => a.id - b.id);

  return ListFilterPlan;
};

export const getListBenefitPremium = (days) => {
  return [
    {
      id: 1,
      name: "Unlimited lesson",
    },
    {
      id: 2,
      name: "Unlimited scoring with AI & suggestion",
    },
    {
      id: 3,
      name: "Unlimit do mock tests",
    },
    {
      id: 4,
      name: "Scoring mock tests with AI",
    },
    {
      id: 5,
      name: "Contribute content",
    },
    {
      id: 6,
      name: "Discusstion and community",
    },
    {
      id: 7,
      name: `Devices using in ${days / 30} month`,
    },
    {
      id: 8,
      name: "Join all class",
    },
    {
      id: 9,
      name: "Raise hand",
    },
    {
      id: 10,
      name: "Download any lesson",
    },
  ];
};
