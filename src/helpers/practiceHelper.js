import { shuffleArr } from "../utils";
import { handleBlankSpace } from "../utils/practice";

export const getSkillCode = (path) => {
  return path.split("/")[3];
};

/**
 * Blank: **(<idQues>)//<answer>
 *
 */

export const filteredParagraph = (paragraph, listQues) => {
  listQues.map((ques) => {
    let filteredOption = ques.questionOptions.map((item) => {
      return item.name;
    });
    filteredOption = shuffleArr(filteredOption);

    if (ques.code && paragraph) {
      let blankValue =
        "**" +
        `(${ques.code[1]})` +
        filteredOption
          .map((option) => {
            return handleBlankSpace(option);
          })
          .join(",") +
        "//" +
        ques.questionSolutions[0].valueText;
      paragraph = paragraph.replace(ques.code, blankValue);
      return blankValue;
    }
    return "";
  });
  return paragraph;
};
