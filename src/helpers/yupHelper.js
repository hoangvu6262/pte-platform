import * as yup from "yup";

export const yupHelper = {
  username: yup
    .string()
    .required("Username is a required field.")
    .matches(
      /^(?=[a-zA-Z0-9._-]{5,30}$)(?!.*[_.-]{2})[^_.].*[^_.]$/,
      "Username must consist of alphanumeric characters (a-zA-Z0-9), lowercase, or uppercase.\n- Username allowed of the dot (.), underscore (_), and hyphen (-).\n- The dot (.), underscore (_), or hyphen (-) must not be the first or last character.\n- The dot (.), underscore (_), or hyphen (-) does not appear consecutively, e.g., java..regex\n- The number of characters must be between 5 to 30.\n- Username is case-insensitive."
    ),
  email: yup
    .string()
    .required("Email is a required field.")
    .matches(
      /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
      "Email must be a valid email."
    ),
  password: yup
    .string()
    .required("Password is a required field.")
    .min(6, "Password must be at least 6 characters long."),
  confirmPassword: yup
    .string()
    .required("Password confirmation is a required field.")
    .oneOf(
      [yup.ref("password"), null],
      "Password confirmation does not match."
    ),
};
