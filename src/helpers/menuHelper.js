const getItem = (label, key, icon, children, type) => {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
};

const generateMenuItem = (group, list) => {
  const { Link } = require("react-router-dom");
  return list?.map((item) =>
    getItem(
      <Link
        to={
          group.code !== "COMP"
            ? "/practice/" +
              group.name.toLowerCase() +
              "/" +
              item.code.toLowerCase() +
              "/" +
              item.id +
              "/*"
            : item.code !== "VOCABULARY"
            ? `/${item.code.toLowerCase()}`
            : "/myvocab/speaking/3"
        }
        className="mega-menu__link--item"
      >
        <span>
          {item.name}{" "}
          {item.tags === "AI" && <sup style={{ color: "#df1f1f" }}>(AI)</sup>}
        </span>
      </Link>,
      item.id + item.code
    )
  );
};

export const menuHelper = {
  getItem,
  generateMenuItem,
};
