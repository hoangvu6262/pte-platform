import React from "react";
import Header from "../../components/shared/Header/Header";
import Footer from "../../components/shared/Footer/Footer";

import "./styles.scss";
import { Outlet } from "react-router-dom";

const MainLayout = () => {
  return (
    <div className="main-layout">
      <Header />
      <Outlet />
      <Footer />
    </div>
  );
};

export default MainLayout;
