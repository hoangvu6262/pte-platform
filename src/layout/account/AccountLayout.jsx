import React from "react";

import "./styles.scss";

import AccountForm from "../../container/AccountForm/AccountForm";
import { Outlet } from "react-router-dom";

const AccountLayout = () => {
  return (
    <div className="account-layout">
      <AccountForm>
        <Outlet />
      </AccountForm>
    </div>
  );
};

export default AccountLayout;
