import React from "react";

import "./styles.scss";

import Header from "../../components/shared/Header/Header";
import Footer from "../../components/shared/Footer/Footer";

import { Outlet, useLocation, Link } from "react-router-dom";

import { useCookies } from "react-cookie";

const listLink = [
  {
    id: "profile",
    icon: <i className="fa-regular fa-user"></i>,
    name: "Profile",
  },
  {
    id: "change-password",
    icon: <i className="fa-solid fa-key"></i>,
    name: "Change Password",
  },
];

const UserLayout = () => {
  const { pathname } = useLocation();

  const [, , removeCookie] = useCookies(["authKey"]);

  const handleSignOut = () => {
    removeCookie("authKey", { path: "/" });
    removeCookie("username", { path: "/" });
    removeCookie("token", { path: "/" });
  };

  const renderList = () => {
    return listLink.map((link) => {
      return (
        <Link
          to={link.id}
          key={link.id}
          className={`user-layout-sidebar__link ${
            pathname.split("/")[pathname.split("/").length - 1] === link.id
              ? "user-layout-sidebar__link--active"
              : ""
          }`}
        >
          <div className="user-layout-sidebar__link--icon">{link.icon}</div>
          <span>{link.name}</span>
        </Link>
      );
    });
  };

  return (
    <>
      <Header />
      <div className="user-layout">
        <div className="user-layout-sidebar">
          <div className="user-layout-sidebar__list">{renderList()}</div>

          <div
            className="user-layout-sidebar__sign-out"
            onClick={handleSignOut}
          >
            <span>Sign out</span>
            <div className="user-layout-sidebar__sign-out--icon">
              <i className="fa-solid fa-arrow-right-from-bracket"></i>
            </div>
          </div>
        </div>
        <div className="user-layout-main">
          <Outlet />
        </div>
      </div>
      <Footer />
    </>
  );
};

export default UserLayout;
