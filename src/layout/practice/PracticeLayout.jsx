/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";

import Header from "../../components/shared/Header/Header";
import Footer from "../../components/shared/Footer/Footer";
import CustomDrawer from "../../components/practice/Drawer/CustomDrawer";
import PracticeSidebar from "../../container/PracticeSidebar/PracticeSidebar";
import Discuss from "../../container/Discuss/Discuss";
import { Tooltip, BackTop } from "antd";
import "./styles.scss";

import {  useParams, Outlet } from "react-router-dom";

import { useSelector, useDispatch } from "react-redux";
import {
  getListLessonThunk,
  resetFilterThunk,
} from "../../redux/slices/lesson/lessonSlice";
import { getCategoriesAndMarkThunk } from "../../redux/slices/shared/sharedSlice";

const PracticeLayout = () => {
  const [visible, setVisible] = useState(true);
  const [sidebarVisible, setSidebarVisible] = useState(false);
  const { id } = useParams();
  // const { pathname } = useLocation();

  // const navigate = useNavigate();
  const dispatch = useDispatch();

  // const { content } = useSelector((state) => state.lesson.lessonDetail);
  const listCategories = useSelector((state) => state.shared.categories);

  //Navigate Id
  // const navigateLink = (path, id) => {
  //   const splittedPath = path.split("/");
  //   splittedPath[splittedPath.length - 1] = id;

  //   return splittedPath.join("/");
  // };

  const openSidebar = () => {
    setSidebarVisible(!sidebarVisible);
  };

  useEffect(() => {
    setVisible(false);
    setSidebarVisible(true);
    dispatch(resetFilterThunk({ categoryIds: id - 0 }));
    dispatch(getListLessonThunk({ categoryIds: id - 0 }));
  }, [id]);

  // useEffect(() => {
  //   if (content.zorder) {
  //     navigate(navigateLink(pathname, content.zorder));
  //   }
  //   // console.clear();
  // }, [content]);

  useEffect(() => {
    if (!listCategories) {
      dispatch(getCategoriesAndMarkThunk());
    }
  }, [listCategories]);

  return (
    <div style={{ backgroundColor: "#fafafa", width: "100%" }}>
      <Header />

      <div
        className={`practice-layout ${
          sidebarVisible && "practice-layout-w-sidebar"
        }`}
      >
        <div className="practice-layout-main">
          <div className="practice-layout-content">
            <Outlet />
          </div>
          <Discuss />
        </div>

        {sidebarVisible && (
          <div className="practice-layout-sidebar">
            <PracticeSidebar setVisible={setVisible} />
            <Tooltip
              title={sidebarVisible ? "Hide Sidebar" : "Show sidebar"}
              placement="bottom"
            >
              <button
                className="practice-layout-sidebar-close"
                onClick={openSidebar}
              >
                <i className="fa-solid fa-angle-right"></i>
              </button>
            </Tooltip>
          </div>
        )}
      </div>
      <BackTop>
        <div className="back-top">
          <i className="fa-solid fa-chevron-up"></i>
        </div>
      </BackTop>
      {!sidebarVisible && (
        <Tooltip
          title={sidebarVisible ? "Hide Sidebar" : "Show sidebar"}
          placement="bottomRight"
        >
          <button className="practice-layout-bar" onClick={openSidebar}>
            <i className="fa-solid fa-angle-left"></i>
          </button>
        </Tooltip>
      )}
      <CustomDrawer visible={visible} setVisible={setVisible}>
        <PracticeSidebar setVisible={setVisible} />
      </CustomDrawer>
      <Footer />
    </div>
  );
};

export default PracticeLayout;
