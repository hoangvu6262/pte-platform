import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { toast } from "react-toastify";

import responseAPI from "../../../redux/api/practice/responseAPI";

const {
  addUserResponse,
  addUserResponseHIW,
  addUserResponseAISpeech,
  addUserResponseAIText,
} = responseAPI;
/**
 * status:
 * 1: not submit.
 * 2: answering.
 * 3: submitted.
 */

const answerSlices = createSlice({
  name: "answer",
  initialState: {
    status: 1,
    content: "",
    isSpeaking: false,
  },
  reducers: {
    updateAnswering: (state, action) => {
      state.status = 2;
      state.content = action.payload;
    },
    getAnswerContent: (state, action) => {
      return state.content;
    },
    redoAnswering: (state, action) => {
      state.status = 1;
      state.content = "";
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(submitAnswerThunk.pending, (state) => {
        toast.loading("Submitting...", {
          position: "top-center",
        });
      })
      .addCase(submitAnswerThunk.fulfilled, (state, { payload }) => {
        state.status = 3;
        toast.dismiss();
        toast.success(
          `Submit successfully! ${
            payload.score !== null && payload.score !== undefined
              ? `Your score: ${payload.score}/${payload.maxScore}`
              : ""
          }`
        );
      })
      .addCase(submitAnswerThunk.rejected, (state, action) => {
        toast.dismiss();
        toast.error("Sorry, we failed to submit your answer");
      })
      .addCase(changeIsSpeakingThunk.fulfilled, (state, { payload }) => {
        state.isSpeaking = payload;
      })
      .addCase(changeStatusThunk.fulfilled, (state, action) => {
        state.status = action.payload;
      });
  },
});

export default answerSlices;

export const submitAnswerThunk = createAsyncThunk(
  "answer/submit",
  async (data, thunkAPI) => {
    const { id, code, category } = data;
    const content = thunkAPI.getState().answer.content;
    if (id !== "*") {
      if (content.speech) {
        const res = await addUserResponseAISpeech(content);
        return res;
      }
      if (category === "swt" || category === "essay" || category === "sst") {
        const res = await addUserResponseAIText(content);
        return res;
      }
      if (id === 26) {
        const res = await addUserResponseHIW(content, parseInt(code));
        return res;
      } else {
        const res = await addUserResponse(content);
        return res;
      }
    }
  }
);

export const changeIsSpeakingThunk = createAsyncThunk(
  "answer/changeIsSpeakingThunk",
  (data) => {
    return data;
  }
);

export const changeStatusThunk = createAsyncThunk(
  "answer/changeStatusThunk",
  (data) => {
    return data;
  }
);
