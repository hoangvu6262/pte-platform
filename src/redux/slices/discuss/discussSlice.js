import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'

import responseAPI from '../../../redux/api/practice/responseAPI'
import discussionAPI from '../../../redux/api/practice/discussionAPI'

const { deleteUserResponse } = responseAPI
const { getDiscussionList, getBoardList, getMeList } = discussionAPI

const initialPagination = {
  totalPages: 0,
  totalItems: 0,
  sizeCurrentItems: 0,
  numberOfCurrentPage: 0,
}

const discussSlices = createSlice({
  name: 'discuss',
  initialState: {
    listDiscussion: [],
    listBoard: [],
    listAnswer: [],
    tabKey: 1,
    discussPagination: initialPagination,
    boardPagination: initialPagination,
    mePagination: initialPagination,
  },
  reducers: {
    updateAnswer: (state, action) => {
      state.listAnswer = action.payload
    },
  },
  extraReducers: (builder) => {
    builder.addCase(addDiscussionThunk.fulfilled, (state, { payload }) => {
      switch (state.tabKey) {
        case 2: {
          state.listBoard.push(payload)
          break
        }
        case 3: {
          state.listAnswer.push(payload)
          break
        }
        default: {
          state.listDiscussion.push(payload)
        }
      }
    })
    builder.addCase(
      getListDiscussionTabThunk.fulfilled,
      (state, { payload }) => {
        const { contents, ...rest } = payload
        state.discussPagination = rest
        state.listDiscussion = contents
      }
    )
    builder.addCase(
      getListDiscussionTabThunk.rejected,
      (state, { payload }) => {
        state.discussPagination = initialPagination
        state.listDiscussion = []
      }
    )
    builder.addCase(getListBoardTabThunk.fulfilled, (state, { payload }) => {
      const { contents, ...rest } = payload
      state.boardPagination = rest
      state.listBoard = contents
    })
    builder.addCase(getListBoardTabThunk.rejected, (state, { payload }) => {
      state.listBoard = []
      state.boardPagination = initialPagination
    })
    builder.addCase(getListMeTabThunk.fulfilled, (state, { payload }) => {
      const { contents, ...rest } = payload
      state.mePagination = rest
      state.listAnswer = contents
    })
    builder.addCase(getListMeTabThunk.rejected, (state, { payload }) => {
      state.listAnswer = []
      state.mePagination = initialPagination
    })
    builder.addCase(deleteAnswerThunk.fulfilled, (state, { payload }) => {
      state.listAnswer = state.listAnswer.filter((ans) => ans.id !== payload.id)
    })
    builder.addCase(changeTabKeyThunk.fulfilled, (state, { payload }) => {
      state.tabKey = parseInt(payload)
    })
  },
})

export default discussSlices

export const addDiscussionThunk = createAsyncThunk(
  'discuss/addDiscussion',
  async (data) => {
    return data
  }
)

export const getListDiscussionTabThunk = createAsyncThunk(
  'discuss/getListDiscussionTabThunk',
  async (data) => {
    const res = await getDiscussionList(data)
    return res
  }
)

export const getListBoardTabThunk = createAsyncThunk(
  'discuss/getListBoardTabThunk',
  async (data) => {
    const res = await getBoardList(data)
    return res
  }
)

export const getListMeTabThunk = createAsyncThunk(
  'discuss/getListMeTabThunk',
  async (data) => {
    const res = await getMeList(data)
    return res
  }
)

export const deleteAnswerThunk = createAsyncThunk(
  'discuss/deleteAnswer',
  async ({ id, createdBy }) => {
    const res = await deleteUserResponse(id, createdBy)
    return {
      response: res,
      id: id,
    }
  }
)

export const changeTabKeyThunk = createAsyncThunk(
  'discuss/changeTabKeyThunk',
  async (key) => {
    return key
  }
)
