import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import lessonAPI from '../../../redux/api/practice/lessonAPI'

const { getLessonDetail, getLessonList } = lessonAPI

// const limitAmount = 10;

export const initialFilters = {
  categoryIds: null,
  sort: 1,
  priority: null,
  practiceStatus: null,
  // shadowing: null,
  // explanation: null,
  // month: null,
  // weekly: null,
  status: 1,
  searchKeyWord: null,
  // shared: null,
  pageNumber: 0,
  pageSize: 20,
  sortDataField: 'ZORDER',
  sortOrder: 'ASC',
}

const initialPagination = {
  totalPages: 0,
  totalItems: 0,
  sizeCurrentItems: 0,
  numberOfCurrentPage: 0,
}

const lessonSlices = createSlice({
  name: 'lessonSlices',
  initialState: {
    lessonDetail: {
      status: 'idle',
      content: {},
    },
    listLesson: {
      status: 'idle',
      listLesson: [],
      filters: initialFilters,
      pagination: initialPagination,
    },
  },
  reducers: {
    updateFilter: (state, action) => {
      state.listLesson.filters = {
        ...state.listLesson.filters,
        ...action.payload,
      }
    },
  },
  extraReducers: (builder) => {
    builder
      //Get List Lesson
      .addCase(getListLessonThunk.pending, (state) => {
        state.listLesson.status = 'loading'
      })
      .addCase(getListLessonThunk.fulfilled, (state, { payload }) => {
        state.listLesson.status = 'success'
        if (payload && payload.contents) {
          state.listLesson.listLesson = payload.contents
        } else {
          state.listLesson.listLesson = []
        }
        state.listLesson.pagination = {
          totalPages: payload.totalPages,
          totalItems: payload.totalItems,
          sizeCurrentItems: payload.sizeCurrentItems,
          numberOfCurrentPage: payload.numberOfCurrentPage,
        }
      })
      .addCase(getListLessonThunk.rejected, (state, action) => {
        state.listLesson.status = 'error'
        state.listLesson.listLesson = []
        state.listLesson.pagination = initialPagination
      })
      // Get Detail
      .addCase(getLessonDetailThunk.pending, (state) => {
        state.lessonDetail.status = 'loading'
      })
      .addCase(getLessonDetailThunk.fulfilled, (state, { payload }) => {
        state.lessonDetail.status = 'success'
        state.lessonDetail.content = payload
      })
      .addCase(getLessonDetailThunk.rejected, (state, action) => {
        state.lessonDetail.status = 'error'
      })
    // Get Next List
  },
})

export default lessonSlices

export const getListLessonThunk = createAsyncThunk(
  'lessonSlices/getListLessonThunk',
  async (data, thunkAPI) => {
    const filtersState = thunkAPI.getState().lesson.listLesson.filters
    const body = { ...filtersState, ...data }
    const res = await getLessonList(body)
    return res
  }
)

export const getLessonDetailThunk = createAsyncThunk(
  'lessonSlices/getLessonDetailThunk',
  async (data) => {
    const res = await getLessonDetail(data.skill, data.zorder)
    return res
  }
)

export const resetFilterThunk = createAsyncThunk(
  'lessonSlices/reset-filter',
  async (data, thunkAPI) => {
    thunkAPI.dispatch(
      lessonSlices.actions.updateFilter({ ...initialFilters, ...data })
    )
    thunkAPI.dispatch(getListLessonThunk(data))
  }
)

export const getNextPageLessonThunk = createAsyncThunk(
  'lessonSlices/next-lesson',
  async (data, thunkAPI) => {
    thunkAPI.dispatch(lessonSlices.actions.updateFilter(data))
    const res = await thunkAPI.dispatch(getListLessonThunk(data))
    const nextId = res.payload.contents[0].id
    thunkAPI.dispatch(getLessonDetailThunk(nextId))
  }
)

export const getPrevPageLessonThunk = createAsyncThunk(
  'lessonSlices/next-lesson',
  async (data, thunkAPI) => {
    thunkAPI.dispatch(lessonSlices.actions.updateFilter(data))
    const res = await thunkAPI.dispatch(getListLessonThunk(data))
    const prevId = res.payload.contents[res.payload.contents.length - 1].id
    thunkAPI.dispatch(getLessonDetailThunk(prevId))
  }
)
