import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

import globalAPI from "../../api/practice/globalAPI";
import { filteredCate } from "../../../utils";

const { getCategories } = globalAPI;

const sharedSlice = createSlice({
  name: "shared",
  initialState: {
    categories: [],
    marks: [],
  },
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(
      getCategoriesAndMarkThunk.fulfilled,
      (state, { payload }) => {
        state.categories = payload.categories;
        state.marks = payload.marks;
      }
    );
  },
});

export default sharedSlice;

export const getCategoriesAndMarkThunk = createAsyncThunk(
  "shared/getCategoriesAndMark",
  async () => {
    const categories = await getCategories(null, "PTE").then((res) => {
      return filteredCate(res);
    });
    const components = await getCategories(null, "SCORE").then((res) => {
      return filteredCate(res);
    });
    const marks = await getCategories(null, "MARK");

    return {
      categories: [...categories, ...components],
      marks: marks.filter((item) => item.parentCode !== undefined),
    };
  }
);
