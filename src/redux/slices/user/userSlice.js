import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

import accountAPI from "../../../redux/api/account/accountAPI";

const { getUserInfo } = accountAPI;

const userSlice = createSlice({
  name: "user",
  initialState: {
    status: "",
    authKey: "",
    detail: {},
  },
  reducers: {
    updateUser: (state, { payload }) => {
      state.detail = payload.detail;
      state.authKey = payload.authKey;
    },

    resetUser: (state, action) => {
      state.detail = {};
      state.authKey = "";
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getUserIn4Thunk.fulfilled, (state, { payload }) => {
      state.detail = payload.data.item;
    });
  },
});

export default userSlice;

export const getUserIn4Thunk = createAsyncThunk(
  "user/getUserIn4",
  async ({ username, authKey }) => {
    const res = getUserInfo({ authKey, username });

    return res;
  }
);
