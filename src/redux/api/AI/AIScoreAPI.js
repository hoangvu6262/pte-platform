import api from "./apiConfig";

const AIScoreAPI = ({ rest, body }) => {
  const url = `${rest}`;

  return api.post(url, body);
};

export default AIScoreAPI;
