import { userTransport } from "../../../config/http/transport";

const paymentAPI = {
  getPackages: () => {
    const url = "packages";

    return userTransport.get(url);
  },
  getPlans: (id) => {
    const url = `packages/${id}/prices`;

    return userTransport.get(url);
  },
  getDirectCheckoutLink: (id, userId) => {
    const url = `prices/${id}/direct-checkout-link?userId=${userId}`;

    return userTransport.post(url);
  },
};

export default paymentAPI;
