import axios from "axios";

import { getTokenFromCookie } from "../../../utils/index";

const api = axios.create({
  baseURL: process.env.REACT_APP_PRACTICE_API,
  headers: {
    "Content-Type": "application/json",
  },
});

api.interceptors.request.use(
  (config) => {
    config.headers["Brandurl"] = "boostpte.com";
    config.headers["Authorization"] = "Bearer " + getTokenFromCookie();
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

api.interceptors.response.use(
  (response) => {
    if (response && response.data) {
      return response.data.data;
    }

    return response;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default api;
