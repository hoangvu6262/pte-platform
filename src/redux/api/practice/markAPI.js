import api from "./apiConfig";

const markAPI = {
  setPriority: (priority, id) => {
    const url = "/lesson-priority";

    return api.post(url, {
      priority,
      lesson: {
        id,
      },
    });
  },
  deletePriority: (id) => {
    const url = `/lesson-priority/${id}`;

    return api.delete(url);
  },
};

export default markAPI;
