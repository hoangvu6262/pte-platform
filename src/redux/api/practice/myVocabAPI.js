import api from "./apiConfig";

const REST = "vocab-user";

const LIMIT = 12;

const myVocabAPI = {
  getDetail: ({ vocab }) => {
    const url = "vocabs";

    return api.get(url, {
      params: {
        vocab,
        pageSize: 1,
      },
    });
  },

  getDictionary: ({ vocab, category }) => {
    const url = `${REST}/dictionary`;

    return api.post(url, { vocab, category });
  },

  addVocab: (data) => {
    // data:
    /**
     * {
    "userId": 0,
    "definitionUser": "tieu de",
    "priority": 1,
    "exampleUser": " this is title",
    "revisedCount": 1,
    "vocab": {
        "vocab": "title"
    },
    "category": {
        "id": 1
    }
     */

    const url = `${REST}`;

    return api.post(url, data);
  },

  getVocabList: ({ userId, categoryId }) => {
    const url = `${REST}`;

    return api.get(url, {
      params: {
        categoryId,
        userId,
      },
    });
  },

  updateVocab: (data, id) => {
    /**
     * data
     * {
      "definitionUser": "neww",
      "exampleUser": "new",
      "status": 1
      }
     */
    const url = `${REST}/${id}`;

    return api.put(url, data);
  },

  deleteVocab: (id) => {
    const url = `${REST}/${id}`;

    return api.delete(url);
  },
};

export default myVocabAPI;
