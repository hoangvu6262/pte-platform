import api from "./apiConfig";

const REST = "lessons";

const lessonAPI = {
  getLessonList: (data) => {
    const url = `${REST}/filter`;

    return api.get(url, {
      params: data,
    });
  },

  getLessonDetail: (categoryCode, zorder) => {
    const url = "lesson";

    return api.get(url, {
      params: {
        categoryCode,
        zOrder: zorder,
      },
    });
  },

  getTypeList: (id) => {
    const url = `category/${id}`;

    return api.get(url);
  },
};

export default lessonAPI;
