import api from './apiConfig'

const REST = 'discussion'
const LIMIT = 10

const discussionAPI = {
  getDiscussionList: ({ lessonId, page }) => {
    const url = `${REST}/lesson/${lessonId}`

    return api.get(url, {
      params: {
        page,
        limit: LIMIT,
      },
    })
  },

  getBoardList: ({ lessonId, pageNumber, userId }) => {
    const url = `${REST}s/v2/response-user`

    return api.get(url, {
      params: {
        lessonId,
        pageNumber: pageNumber,
        pageSize: LIMIT,
        typeQuery: 'BOARD',
        userId,
      },
    })
  },

  getMeList: ({ lessonId, pageNumber, userId }) => {
    const url = `${REST}s/v2/response-user`

    return api.get(url, {
      params: {
        lessonId,
        pageNumber: pageNumber,
        pageSize: LIMIT,
        typeQuery: 'ME',
        userId,
      },
    })
  },

  addDiscussion: (data) => {
    const url = `${REST}`

    return api.post(url, data)
  },

  deleteDiscussion: (id, userid) => {
    const url = `${REST}/${id}`

    return api.delete(url, {
      params: {
        userid,
      },
    })
  },

  updateDiscussion: ({ userId, discussionContent, id }) => {
    const url = `${REST}/${id}`

    const body = {
      userId,
      discussionContent,
    }

    return api.put(url, body)
  },

  addDiscussionReaction: (data) => {
    const url = `${REST}-reactions`

    return api.post(url, data)
  },

  deleteDiscussionReaction: (discussionId, userid) => {
    const url = `${REST}-reactions`

    return api.delete(url, {
      params: {
        discussion_id: discussionId,
        userid,
      },
    })
  },
}

export default discussionAPI
