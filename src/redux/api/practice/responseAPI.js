import api from "./apiConfig";

const REST = "user-score";

const responseAPI = {
  addUserResponse: (data) => {
    const url = `${REST}`;

    return api.post(url, data);
  },
  addUserResponseHIW: (data, id) => {
    const url = `${REST}/smw/${id}`;

    return api.post(url, data);
  },

  addUserResponseAISpeech: (data) => {
    const url = `${REST}/ai-speech`;

    return api.post(url, data, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  },

  addUserResponseAIText: (data) => {
    const url = `${REST}/ai-text`;

    return api.post(url, data);
  },

  doAIScore: (data) => {
    const url = `${REST}/ai/do-score`;

    return api.post(url, data);
  },

  getListUserResponse: (lessonId, userId) => {
    const url = `${REST}/${lessonId}/${userId}`;

    return api.get(url);
  },

  deleteUserResponse: (createdTime) => {
    const url = `${REST}`;

    return api.delete(url, {
      params: {
        dateTime: createdTime,
      },
    });
  },

  getListUserResponseV2: ({ userId, questionIds, codeCategory }) => {
    const url = `${REST}/param`;

    let body = {};

    if (userId) {
      body = {
        userId,
        questionIds,
        codeCategory,
      };
    } else {
      body = {
        questionIds,
        codeCategory,
      };
    }

    return api.post(url, body);
  },
};

export default responseAPI;
