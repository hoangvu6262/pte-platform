import api from "./apiConfig";

const REST = "leaner/tested";

const testedAPI = {
  getCount: (id) => {
    const url = `${REST}/lesson/${id}`;

    return api.get(url);
  },

  saveTested: (data) => {
    const url = `${REST}`;

    const body = {
      userId: 1,
      ...data,
    };

    return api.post(url, body);
  },

  viewAllTested: (lessonId) => {
    const url = `${REST}/view-all/${lessonId}`;

    return api.get(url);
  },

  getByUserId: (lessonId, userId) => {
    const url = `${REST}/${lessonId}/${userId ? userId : 1}`;

    return api.get(url);
  },

  deleteTested: (id) => {
    const url = `${REST}/${id}`;

    api.delete(url);
  },
};

export default testedAPI;
