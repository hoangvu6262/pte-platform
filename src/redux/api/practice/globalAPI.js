import api from "./apiConfig";

// const HOSTNAME = window.location.hostname;
const BRANDURL = "boostpte.com";
// const BRANDURL = HOSTNAME;

const globalAPI = {
  getCategories: (id, group) => {
    const url = `/category`;
    if (id) {
      return api.get(url + `/${id}`);
    }
    return api.get(url, {
      params: {
        group,
      },
    });
  },
  getCategoriesByParent: (id) => {
    const url = `/category/${id}`;

    return api.get(url);
  },
  getToken: ({ username, authKey }) => {
    const url = `/token`;

    const body = {
      username,
      authKey,
      brandUrl: BRANDURL,
    };

    return api.post(url, body);
  },
};

export default globalAPI;
