import api from "./apiConfig";

const globalAPI = {
  cdnSaveFile: (file) => {
    const url = "cdn/save-file";

    return api.post(
      url,
      {
        file: file,
      },
      {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      }
    );
  },
};

export default globalAPI;
