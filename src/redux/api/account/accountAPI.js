import api from './apiConfig'

const REST = 'v1/users'

// const HOSTNAME = window.location.hostname;
const BRANDURL = 'my.boostpte.com'
// const BRANDURL = HOSTNAME;

const accountAPI = {
  signUpAPI: (data) => {
    const url = `${REST}/register`

    const body = {
      brandUrl: BRANDURL,
      ...data,
    }

    return api.post(url, body)
  },

  signInAPI: (data) => {
    const url = `${REST}/auth`

    const body = {
      brandUrl: BRANDURL,
      ...data,
    }

    return api.post(url, body)
  },

  forgotPasswordAPI: ({ email, resetPasswordLink }) => {
    const url = `${REST}/forgot-password`

    return api.post(url, null, {
      params: {
        brandUrl: BRANDURL,
        email: email,
        resetPasswordLink: resetPasswordLink,
        patternCode: 'FORGOT_PASSWORD',
      },
    })
  },

  resetPasswordAPI: (data) => {
    const url = `${REST}/reset-password`

    const body = {
      brandUrl: BRANDURL,
      ...data,
    }

    return api.post(url, body)
  },

  changePasswordAPI: ({ body, authKey }) => {
    const url = `${REST}/change-password`

    return api.put(url, body, {
      params: {
        authKey,
      },
    })
  },

  getUserInfo: ({ username, authKey }) => {
    const url = `${REST}/${username}`

    return api.get(url, {
      params: {
        brandUrl: BRANDURL,
        authKey,
      },
    })
  },

  getUserInfoById: ({ id, authKey }) => {
    const url = `${REST}/${id}`

    return api.get(url, {
      params: {
        brandUrl: BRANDURL,
        authKey,
      },
    })
  },

  updateUserInfo: ({ username, authKey, body }) => {
    const url = `${REST}/${username}`

    return api.put(url, body, {
      params: {
        authKey,
        brandUrl: BRANDURL,
      },
    })
  },
  getBrandInfo: (authKey) => {
    const url = 'brands/info'

    return api.get(url, {
      params: {
        authKey,
        brandUrl: BRANDURL,
      },
    })
  },
}

export default accountAPI
