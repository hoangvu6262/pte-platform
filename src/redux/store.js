import { configureStore } from "@reduxjs/toolkit";
import answerSlices from "./slices/answer/answerSlice";
import discussSlices from "./slices/discuss/discussSlice";
import lessonSlices from "./slices/lesson/lessonSlice";
import userSlice from "./slices/user/userSlice";
import sharedSlice from "./slices/shared/sharedSlice";

const store = configureStore({
  reducer: {
    answer: answerSlices.reducer,
    discuss: discussSlices.reducer,
    lesson: lessonSlices.reducer,
    user: userSlice.reducer,
    shared: sharedSlice.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

export default store;
