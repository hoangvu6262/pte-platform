import { useState } from 'react'

const useModal = () => {
  const [open, setOpen] = useState(false)
  const [data, setData] = useState(null)

  const _handleToggle = (data) => {
    !!data && setData(data)
    setOpen(!open)
  }

  return { open, data, _handleToggle }
}

export default useModal
