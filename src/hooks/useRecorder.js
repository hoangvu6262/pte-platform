import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import answerSlices from "../redux/slices/answer/answerSlice";

const useRecorder = () => {
  const [audioURL, setAudioURL] = useState("");
  const [isRecording, setIsRecording] = useState(false);
  const [recorder, setRecorder] = useState(null);

  const dispatch = useDispatch();

  const { content } = useSelector((state) => state.lesson.lessonDetail);

  useEffect(() => {
    // Lazily obtain recorder first time we're recording.
    if (recorder === null) {
      if (isRecording) {
        requestRecorder().then(setRecorder, console.error);
      }
      return;
    }

    // Manage recorder state.
    if (isRecording) {
      recorder.start();
    } else {
      recorder.stop();
    }

    // Obtain the audio when ready.
    const handleData = async (e) => {
      const fileAudio = new File([e.data], "record.mp3", {
        type: e.data.type,
      });

      setAudioURL(URL.createObjectURL(fileAudio));

      const body = {
        speech: fileAudio,
        question: content?.questionGroup?.questions[0]?.id,
      };
      dispatch(answerSlices.actions.updateAnswering(body));
    };

    recorder.addEventListener("dataavailable", handleData);
    return () => recorder.removeEventListener("dataavailable", handleData);
  }, [recorder, isRecording, content, dispatch]);

  const startRecording = () => {
    setIsRecording(true);
  };

  const stopRecording = () => {
    setIsRecording(false);
  };

  const resetAudio = () => {
    setAudioURL("");
    setIsRecording(false);
    setRecorder(null);
    dispatch(answerSlices.actions.redoAnswering());
  };

  return [audioURL, resetAudio, isRecording, startRecording, stopRecording];
};

async function requestRecorder() {
  const stream = await navigator.mediaDevices.getUserMedia({ audio: true });
  return new MediaRecorder(stream);
}
export default useRecorder;
